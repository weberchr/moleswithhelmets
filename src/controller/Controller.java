package controller;

import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.shape.Rectangle;
import javafx.util.Duration;
import model.Model;
import model.Token;
import view.LevelEditor;
import view.View;

import static javafx.scene.input.KeyCode.*;

/**
 * Erstellen des Controllers für das Spiel
 *
 * created by Vadim
 */
public class Controller {
	
	private View		view;
	private Model		model;
	private LevelEditor	levelEditor;
	private boolean		musikIsRunning;
	private Media		media;
	private MediaPlayer	mediaPlayer;
	
	public Controller(Model model, View view) {
		this.view = view;
		this.model = model;
		this.levelEditor = this.view.getLevelEditor();
		
		// Erstellen des MP3-Players
		this.media = new Media(getClass().getResource("../music/newAge.mp3").toString());
		this.mediaPlayer = new MediaPlayer(this.media);
		this.mediaPlayer.setStartTime(Duration.millis(5000));
		this.mediaPlayer.setVolume(0.07);
		this.mediaPlayer.setCycleCount(MediaPlayer.INDEFINITE);
		this.musikIsRunning = false;
		
		/**
		 * Funktionalität zuweisen sämtlichen benötigten Buttons
		 */
		// Back-Button in Game
		this.view.getBackButton().setOnAction(event -> {
			levelAuswahlChoice();
			this.model.startStopTimer();
		});
		
		// Musik - Button in Game
		this.view.getMusikButton().setOnAction(event -> this.startStopMusik());
		
		// Craft-Mode- Button im LevelAuswahlPopUp
		this.view.getCraftButton().setOnAction(event -> {
			popUpHide();
			this.model.setCrafmode(true);
			this.view.fromLevelAuswahlPopUpToGame();
			this.model.startStopTimer();
		});
		
		// Back- Button im LevelAuswahlPopUp
		this.view.getBackToLevelAuswahlButton().setOnAction(event -> {
			levelAuswahlChoice();
			this.model.setRunning(false);
			popUpHide();
			this.model.setInPopup(false);
		});
		
		// Go-Button im LevelAuswahlPopUp
		this.view.getGameButton().setOnAction(event -> {
			popUpHide();
			this.view.fromLevelAuswahlPopUpToGame();
			this.model.setIsOver();
			this.model.startStopTimer();
		});
		
		// Exit- Button in Startmenu
		this.view.getExitButton().setOnAction(event -> this.view.exit());
		
		// Start-Button in Startmenu
		this.view.getStartButton().setOnAction(event -> this.view.fromStartToLevelAuswahl());
		
		// LevelEditor- Button in Startmenu
		this.view.getLevelEditorButton().setOnAction(event -> {
			this.view.fromStartToLevelEditor();
			this.model.setLevelEditorRunning(true);
		});
		
		// Next-Button im LevelAuswahl
		this.view.getNextLevelauswahlButton().setOnAction(event -> {
			if (this.model.calculateUnlocked().get(6)) this.view.fromStartToLevelAuswahl2();
			else this.view.showAlert();
		});
		
		// MainMenu- Button in Levelauswahl
		this.view.getBackToStartButton().setOnAction(event -> this.view.backToMainMenu());
		
		// Previous-Button im LevelAuswahl2
		this.view.getPreviousLevelauswahlButton().setOnAction(event -> this.view.fromStartToLevelAuswahl());
		
		// MainMenu- Button in Levelauswahl2
		this.view.getBackToStartButton2().setOnAction(event -> this.view.backToMainMenu());
		
		// Buttons (1-6) im LA und der Buttons (7-12) in LA2
		for (int i = 0; i < this.view.getLevelButtonList().size(); i++) {
			final int x = i;
			this.view.getLevelButtonList().get(i).setOnAction(event -> this.view.toLevelAuswahlPopUp(x));
		}
		
		for (int i = 6; i < this.view.getLevelButtonList2().size(); i++) {
			final int x = i;
			this.view.getLevelButtonList2().get(i).setOnAction(event -> this.view.toLevelAuswahlPopUp(x));
		}
		
		// Okay-Button in AlertPopUp
		this.view.getAlertPopUp().okayButton.setOnAction(event -> {
			popUpHide();
			this.view.fromStartToLevelAuswahl();
		});
		
		// PlayAgain-Button im GewonnenPopUp
		this.view.getPlayAgainButton().setOnAction(event -> {
			this.model.setLevel(this.model.getCurrentlevel().getFilename(), this.model.getLevelindex());
			this.view.fromLevelAuswahlPopUpToGame();
			this.model.setIsOver();
			popUpHide();
		});
		
		// Back-Button im GewonnenPopUp
		this.view.getBackToLevAuswahlButton().setOnAction(event -> {
			levelAuswahlChoice();
			this.model.startStopTimer();
			popUpHide();
		});
		
		// TryAgain-Button im VerlorenPopUp
		this.view.getTryAgainButton().setOnAction(event -> {
			this.model.setLevel(this.model.getCurrentlevel().getFilename(), this.model.getLevelindex());
			this.view.fromLevelAuswahlPopUpToGame();
			this.model.setIsOver();
			popUpHide();
		});
		
		// Back-Button im VerlorenPopUp
		this.view.getBackToLAuswahlButton().setOnAction(event -> {
			levelAuswahlChoice();
			this.model.startStopTimer();
			popUpHide();
		});
		
		// Close-Button im TutorialPopUp
		this.view.getCloseButton().setOnAction(event -> {
			levelAuswahlChoice();
			popUpHide();
		});
		
		// TutorialButton in der LevelAuswahl 1 und 2
		this.view.getTutorialButton().setOnAction(event -> this.view.showTutorialPopUp());
		this.view.getTutorialButton2().setOnAction(event -> this.view.showTutorialPopUp());
		
		// Back-Button im LevelEditor
		this.view.getLevelEditorBackButton().setOnAction(event -> {
			this.view.backToMainMenu();
			this.model.setLevelEditorRunning(false);
		});
		
		// SaveAndExit-Button in Game
		this.view.getSaveAndExitButton().setOnAction(event -> {
			this.model.saveLevel_craft("data/levels/customlevel.json");
			levelAuswahlChoice();
			this.model.startStopTimer();
			this.model.setCrafmode(false);
			popUpHide();
		});
		
		// Save-Button im LevelEditor
		this.view.getLESaveButton().setOnAction(event -> {
			this.model.saveLevel_editor("data/levels/customlevel.json");
			this.view.backToMainMenu();
			this.model.setLevelEditorRunning(false);
		});
		
		// SmallButton im LevelEditor
		this.view.getSmallButton().setOnAction(event -> {
			sizeSetter(13, 13, 40);
		});
		
		// MediumButton im LevelEditor
		this.view.getMediumButton().setOnAction(event -> {
			sizeSetter(15, 15, 35);
		});
		
		// BigButton im LevelEditor
		this.view.getBigButton().setOnAction(event -> {
			sizeSetter(18, 18, 30);
		});
		
		// Token-Buttons im LevelEditor
		for (int i = 0; i < 13; i++) {
			switch (i) {
				case 0:
					this.view.getTokenButtons().get(0).setOnAction(event -> {
						this.model.setSelectedToken(Token.ME);
					});
					break;
				case 1:
					this.view.getTokenButtons().get(1).setOnAction(event -> {
						this.model.setSelectedToken(Token.MUD);
					});
					break;
				case 2:
					this.view.getTokenButtons().get(2).setOnAction(event -> {
						this.model.setSelectedToken(Token.PATH);
					});
					break;
				case 3:
					this.view.getTokenButtons().get(3).setOnAction(event -> {
						this.model.setSelectedToken(Token.GEM);
					});
					break;
				case 4:
					this.view.getTokenButtons().get(4).setOnAction(event -> {
						this.model.setSelectedToken(Token.STONE);
					});
					break;
				case 5:
					this.view.getTokenButtons().get(5).setOnAction(event -> {
						this.model.setSelectedToken(Token.WALL);
					});
					break;
				case 6:
					this.view.getTokenButtons().get(6).setOnAction(event -> {
						this.model.setSelectedToken(Token.BLOCKLING);
					});
					break;
				case 7:
					this.view.getTokenButtons().get(7).setOnAction(event -> {
						this.model.setSelectedToken(Token.SWAPLING);
					});
					break;
				case 8:
					this.view.getTokenButtons().get(8).setOnAction(event -> {
						this.model.setSelectedToken(Token.XLING);
					});
					break;
				case 9:
					this.view.getTokenButtons().get(9).setOnAction(event -> {
						this.model.setSelectedToken(Token.SLIME);
					});
					break;
				case 10:
					this.view.getTokenButtons().get(10).setOnAction(event -> {
						this.model.setSelectedToken(Token.TURBOTIME);
					});
					break;
				case 11:
					this.view.getTokenButtons().get(11).setOnAction(event -> {
						this.model.setSelectedToken(Token.HOURGLASS);
					});
					break;
				case 12:
					this.view.getTokenButtons().get(12).setOnAction(event -> {
						this.model.setSelectedToken(Token.EXIT);
					});
					break;
			}
		}
		
		keyEventHandler();
		this.view.getStage().show();
	}
	
	private void sizeSetter(int width, int height, int rect) {
		this.model.setWidthLE(width);
		this.model.setHeightLE(height);
		this.model.setRectWidth(rect);
		this.model.levelEditorArray();
		rectanglesClickbar();
	}
	
	private void rectanglesClickbar() {
		Rectangle[][] rects = this.levelEditor.getRectangleGrid();
		
		for (int x = 0; x < rects.length; x++) {
			for (int y = 0; y < rects[x].length; y++) {
				final int fx = x;
				final int fy = y;
				rects[x][y].setOnMouseClicked(event -> this.model.setLevelEditorToken(fx, fy));
			}
		}
	}
	
	// Wechseln zwischen Level-Auswahl
	// und Level-Auswahl2
	private void levelAuswahlChoice() {
		if (this.model.getLevelindex() <= 5) {
			this.view.fromStartToLevelAuswahl();
		} else {
			this.view.fromStartToLevelAuswahl2();
		}
	}
	
	// Methode um nicht benötigte PopUps zu verstecken
	private void popUpHide() {
		this.view.getVerlorenPopUp().popup.hide();
		this.view.getGewonnenPopUp().popup.hide();
		this.view.getLevelAuswahlPopUp().popup.hide();
		this.view.getAlertPopUp().popup.hide();
		this.view.getTutorialPopUp().popup.hide();
	}
	
	// Methode um Musik-Player auszuführen
	private void startStopMusik() {
		if (musikIsRunning) {
			musikIsRunning = false;
			this.mediaPlayer.pause();
			view.getMusikButton().setGraphic(view.getImgLoader().getMusicOnView());
		} else {
			musikIsRunning = true;
			this.mediaPlayer.play();
			view.getMusikButton().setGraphic(view.getImgLoader().getMusicOffView());
		}
	}
	
	// Eventhandler für ArrowKeys Press und Release
	private void keyEventHandler() {
		this.view.getStage().addEventHandler(KeyEvent.KEY_PRESSED, event -> {
			KeyCode key = event.getCode();
			if (key == UP || key == DOWN || key == LEFT || key == RIGHT) {
				model.setPressedArrowKey(key);
				if (event.isShiftDown()) model.setPressedShift(true);
			}
		});
		
		this.view.getStage().addEventHandler(KeyEvent.KEY_RELEASED, event -> {
			KeyCode key = event.getCode();
			if (key == UP || key == DOWN || key == LEFT || key == RIGHT || key == SHIFT) {
				if (key != SHIFT) model.addReleasedArrowKeys(key);
				else model.setShiftReleased(true);
			}
		});
		
	}
	
}