package model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

/**
 * Objektklasse für alle levelspezifischen Daten
 */
public class Level {
	
	/**
	 * An den Level übergebenes Model
	 */
	private Model			model;
	/**
	 * Dateiname des Levels
	 */
	private String			filename;
	/**
	 * Name des Levels zum Anzeigen in der Levelauswahl
	 */
	private String			name;
	/**
	 * Breite des Levels (Anzahl der Spielfelder)
	 */
	private int				width;
	/**
	 * Höhe des Levels (Anzahl der Spielfelder)
	 */
	private int				height;
	/**
	 * Maximale Anzahl an Feldern, die Schleim beinhalten dürfen
	 */
	private int				maxslime;
	/**
	 * Edelsteine, die gesammelt werden müssen, um den Level mit 1, 2, oder 3 Punkten zu bestehen
	 */
	private List<Integer>	gems;
	/**
	 * Anzahl der Ticks, die maximal vergehen dürfen, um den Level mit 1, 2 oder 3 Punkten zu bestehen
	 */
	private List<Integer>	ticks;
	/**
	 * Liste der Levelregeln, die vor den Hauptregeln ausgeführt werden
	 */
	private List<LevelRule>	pre;
	/**
	 * Liste der Levelregeln, die nach den Hauptregeln ausgeführt werden
	 */
	private List<LevelRule>	post;
	/**
	 * Zweidimensionales Array der Spielfelder
	 */
	private Spielfeld[][]	map;
	
	/**
	 * @param model
	 * @param dateiname
	 *            einer JSON-Datei mit Daten des zu erstellenden Levels
	 * @throws JSONException
	 * @throws FileNotFoundException
	 */
	public Level(Model model, String dateiname) throws JSONException, FileNotFoundException {
		this.model = model;
		this.filename = dateiname;
		JSONObject JSONLevel = new JSONObject(new JSONTokener(new FileInputStream(dateiname)));
		
		// get name
		this.name = JSONLevel.getString("name");
		
		// get width
		this.width = JSONLevel.getInt("width");
		
		// get height
		this.height = JSONLevel.getInt("height");
		
		// get maxslime, is size of level if not specified
		if (JSONLevel.has("maxslime")) this.maxslime = JSONLevel.getInt("maxslime");
		else this.maxslime = this.width * this.height;
		
		// get gems
		this.gems = new ArrayList<Integer>();
		JSONArray JSONGems = JSONLevel.getJSONArray("gems");
		for (int i = 0; i < 3; i++) {
			this.gems.add(JSONGems.getInt(i));
		}
		
		// get ticks
		this.ticks = new ArrayList<Integer>();
		JSONArray JSONTicks = JSONLevel.getJSONArray("ticks");
		for (int i = 0; i < 3; i++) {
			this.ticks.add(JSONTicks.getInt(i));
		}
		
		// get pre, is empty ArrayList if not specified
		this.pre = new ArrayList<LevelRule>();
		if (JSONLevel.has("pre")) {
			JSONArray JSONPre = JSONLevel.getJSONArray("pre");
			for (int i = 0; i < JSONPre.length(); i++) {
				this.pre.add(new LevelRule(this.model, JSONPre.getJSONObject(i)));
			}
		}
		
		// get post, is empty ArrayList if not specified
		this.post = new ArrayList<LevelRule>();
		if (JSONLevel.has("post")) {
			JSONArray JSONPost = JSONLevel.getJSONArray("post");
			for (int i = 0; i < JSONPost.length(); i++) {
				this.post.add(new LevelRule(this.model, JSONPost.getJSONObject(i)));
			}
		}
		
		// get map
		this.map = new Spielfeld[this.width][this.height];
		JSONArray JSONMap = JSONLevel.getJSONArray("map");
		for (int i = 0; i < JSONMap.length(); i++) {
			for (int j = 0; j < JSONMap.getJSONArray(i).length(); j++) {
				if (JSONMap.getJSONArray(i).get(j) instanceof JSONObject) {
					this.map[j][i] = new Spielfeld(this.model, JSONMap.getJSONArray(i).getJSONObject(j));
				} else {
					this.map[j][i] = new Spielfeld(this.model, JSONMap.getJSONArray(i).getString(j));
				}
			}
		}
	}
	
	// Getter
	/**
	 * @return Name des Levels zum Anzeigen in der Levelauswahl
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * @return Dateiname des Levels
	 */
	public String getFilename() {
		return filename;
	}
	
	/**
	 * @return Breite des Levels (Anzahl der Spielfelder)
	 */
	public int getWidth() {
		return width;
	}
	
	/**
	 * @return Höhe des Levels (Anzahl der Spielfelder)
	 */
	public int getHeight() {
		return height;
	}
	
	/**
	 * @return Maximale Anzahl an Feldern, die Schleim beinhalten dürfen
	 */
	public int getMaxslime() {
		return maxslime;
	}
	
	/**
	 * @return Edelsteine, die gesammelt werden müssen, um den Level mit 1, 2, oder 3 Punkten zu bestehen
	 */
	public List<Integer> getGems() {
		return gems;
	}
	
	/**
	 * @return Anzahl der Ticks, die maximal vergehen dürfen, um den Level mit 1, 2 oder 3 Punkten zu bestehen
	 */
	public List<Integer> getTicks() {
		return ticks;
	}
	
	/**
	 * @return Liste der Levelregeln, die vor den Hauptregeln ausgeführt werden
	 */
	public List<LevelRule> getPre() {
		return pre;
	}
	
	/**
	 * @return Liste der Levelregeln, die nach den Hauptregeln ausgeführt werden
	 */
	public List<LevelRule> getPost() {
		return post;
	}
	
	/**
	 * @return Zweidimensionales Array der Spielfelder
	 */
	public Spielfeld[][] getMap() {
		return map;
	}
	
	/**
	 * @return Spielfeld an gegebener Koordinate
	 */
	public Spielfeld getField(int x, int y) {
		return map[x][y];
	}
	
}