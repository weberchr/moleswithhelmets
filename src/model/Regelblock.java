package model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;

import static model.Value.*;

/**
 * Objektklasse für alle Daten eines Regelblocks
 */
public class Regelblock extends FeldBlock {
	
	/**
	 * Liste an Dingern, die sich auf dem Feld befinden dürfen
	 */
	private List<Token> tokenlist;
	
	public Regelblock(Model model, JSONObject JSONBlock) {
		this.model = model;
		if (JSONBlock.has("values")) this.values = createValues(JSONBlock.getJSONObject("values"));
		else this.values = createValues(new JSONObject());
		this.number = -1;
		
		this.tokenlist = new ArrayList<Token>();
		if (JSONBlock.get("token") instanceof String) {
			this.tokenlist.add(convertToken(JSONBlock.getString("token")));
		} else {
			JSONArray JSONTokenlist = JSONBlock.getJSONArray("token");
			for (int i = 0; i < JSONTokenlist.length(); i++) {
				this.tokenlist.add(convertToken(JSONTokenlist.getString(i)));
			}
		}
	}
	
	/**
	 * @return Liste an Dingern, die sich auf dem Feld befinden dürfen.
	 */
	public List<Token> getTokenlist() {
		return tokenlist;
	}
	
	/**
	 * @param tokens
	 *            List der Namen der neu gesetzten Feldtokens
	 */
	public void setTokenlist(List<Token> tokens) {
		this.tokenlist = tokens;
	}
	
	/**
	 * @param JSONValues
	 *            ein von org.json erstelltes JSONObject mit Feldeigenschaften
	 * @return Hashmap aller möglichen Feldeigenschaften
	 */
	public Map<Value, Integer> createValues(JSONObject JSONValues) {
		Map<Value, Integer> values = new HashMap<Value, Integer>();
		if (JSONValues.has("moved")) values.put(MOVED, JSONValues.getInt("moved"));
		if (JSONValues.has("falling")) values.put(FALLING, JSONValues.getInt("falling"));
		if (JSONValues.has("loose")) values.put(LOOSE, JSONValues.getInt("loose"));
		if (JSONValues.has("slippery")) values.put(SLIPPERY, JSONValues.getInt("slippery"));
		if (JSONValues.has("pushable")) values.put(PUSHABLE, JSONValues.getInt("pushable"));
		if (JSONValues.has("bam")) values.put(BAM, JSONValues.getInt("bam"));
		if (JSONValues.has("bamrich")) values.put(BAMRICH, JSONValues.getInt("bamrich"));
		if (JSONValues.has("direction")) values.put(DIRECTION, JSONValues.getInt("direction"));
		if (JSONValues.has("A")) values.put(A, JSONValues.getInt("A"));
		if (JSONValues.has("B")) values.put(B, JSONValues.getInt("B"));
		if (JSONValues.has("C")) values.put(C, JSONValues.getInt("C"));
		if (JSONValues.has("D")) values.put(D, JSONValues.getInt("D"));
		return values;
	}
	
}