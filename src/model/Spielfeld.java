package model;

import static model.Value.A;
import static model.Value.B;
import static model.Value.BAM;
import static model.Value.BAMRICH;
import static model.Value.C;
import static model.Value.D;
import static model.Value.DIRECTION;
import static model.Value.FALLING;
import static model.Value.LOOSE;
import static model.Value.MOVED;
import static model.Value.PUSHABLE;
import static model.Value.SLIPPERY;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONObject;

/**
 * Objektklasse für alle Daten eines Spielfeldes
 */
public class Spielfeld extends FeldBlock {
	
	/**
	 * Ding, das sich auf dem Feld befindet
	 */
	private Token	token;
	/**
	 * Boolean, ob das Feld im Schleimalgorithmus schon beachtet wurde
	 */
	private boolean	visited;
	
	/**
	 * Bei Verwendung dieses Konstrukturs werden alle Werte in values auf 0 gesetzt.
	 * 
	 * @param model
	 * @param token
	 *            der Name des Feldtokens
	 */
	public Spielfeld(Model model, String token) {
		this.model = model;
		this.values = createValues(new JSONObject());
		this.number = -1;
		
		this.token = convertToken(token);
		this.visited = false;
	}
	
	/**
	 * @param model
	 * @param JSONSpielfeld
	 *            ein von org.json erstelltes JSONObject mit den Daten des zu erstellenden Spielfelds
	 */
	public Spielfeld(Model model, JSONObject JSONSpielfeld) {
		this.model = model;
		if (JSONSpielfeld.has("values")) this.values = createValues(JSONSpielfeld.getJSONObject("values"));
		else this.values = createValues(new JSONObject());
		this.number = -1;
		
		this.token = convertToken(JSONSpielfeld.getString("token"));
		this.visited = false;
	}
	
	/**
	 * @param JSONValues
	 *            ein von org.json erstelltes JSONObject mit Feldeigenschaften
	 * @return Hashmap aller möglichen Feldeigenschaften
	 */
	public Map<Value, Integer> createValues(JSONObject JSONValues) {
		Map<Value, Integer> values = new HashMap<Value, Integer>();
		
		if (JSONValues.has("moved")) {
			values.put(MOVED, JSONValues.getInt("moved"));
		} else {
			values.put(MOVED, 0);
		}
		
		if (JSONValues.has("falling")) {
			values.put(FALLING, JSONValues.getInt("falling"));
		} else {
			values.put(FALLING, 0);
		}
		
		if (JSONValues.has("loose")) {
			values.put(LOOSE, JSONValues.getInt("loose"));
		} else {
			values.put(LOOSE, 0);
		}
		
		if (JSONValues.has("slippery")) {
			values.put(SLIPPERY, JSONValues.getInt("slippery"));
		} else {
			values.put(SLIPPERY, 0);
		}
		
		if (JSONValues.has("pushable")) {
			values.put(PUSHABLE, JSONValues.getInt("pushable"));
		} else {
			values.put(PUSHABLE, 0);
		}
		
		if (JSONValues.has("bam")) {
			values.put(BAM, JSONValues.getInt("bam"));
		} else {
			values.put(BAM, 0);
		}
		
		if (JSONValues.has("bamrich")) {
			values.put(BAMRICH, JSONValues.getInt("bamrich"));
		} else {
			values.put(BAMRICH, 0);
		}
		
		if (JSONValues.has("direction")) {
			values.put(DIRECTION, JSONValues.getInt("direction"));
		} else {
			values.put(DIRECTION, 0);
		}
		
		if (JSONValues.has("A")) {
			values.put(A, JSONValues.getInt("A"));
		} else {
			values.put(A, 0);
		}
		
		if (JSONValues.has("B")) {
			values.put(B, JSONValues.getInt("B"));
		} else {
			values.put(B, 0);
		}
		
		if (JSONValues.has("C")) {
			values.put(C, JSONValues.getInt("C"));
		} else {
			values.put(C, 0);
		}
		
		if (JSONValues.has("D")) {
			values.put(D, JSONValues.getInt("D"));
		} else {
			values.put(D, 0);
		}
		
		return values;
	}
	
	/**
	 * @return Ding, das sich auf dem Feld befindet
	 */
	public Token getToken() {
		return token;
	}
	
	/**
	 * @return Boolean, ob das Feld im Schleimalgorithmus schon beachtet wurde
	 */
	public boolean getVisited() {
		return visited;
	}
	
	/**
	 * @param token
	 *            Name des neu gesetzten Feldtokens
	 */
	public void setToken(Token token) {
		this.token = token;
	}
	
	/**
	 * Setter für Wert, ob das Feld im Schleimgalgorithmus schon beachtet wurde
	 */
	public void setVisited(boolean bool) {
		this.visited = bool;
	}
	
}