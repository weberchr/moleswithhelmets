package model;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.scene.input.KeyCode;
import javafx.util.Duration;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;

import static model.SoundEvent.*;
import static model.Token.*;
import static model.Value.*;

public class Model extends Observable {
	
	/**
	 * Anzahl der spielbaren Levels
	 */
	public static final int		NUMBEROFLEVELS	= 12;
	/**
	 * Anzahl der gesammelten Edelsteine
	 */
	private int					gems;
	/**
	 * Anzahl der vergangenen Ticks
	 */
	private int					ticks;
	/**
	 * Minutenteil der noch verbleibenden Zeit
	 */
	private int					minutes;
	/**
	 * Sekundenteil der noch verbleibenden Zeit
	 */
	private int					seconds;
	/**
	 * Wert der globalen Bedingung X
	 */
	private int					x;
	/**
	 * Wert der globalen Bedingung Y
	 */
	private int					y;
	/**
	 * Wert der globalen Bedingung Z
	 */
	private int					z;
	/**
	 * Aktuell geladener Level
	 */
	private Level				currentLevel;
	/**
	 * Index des aktuell geladenen Levels
	 */
	private int					levelindex;
	/**
	 * Punkte pro Level (0 = noch nicht bestanden)
	 */
	private List<Integer>		points;
	/**
	 * Wert ob jeder Level schon freigeschaltet ist
	 */
	private List<Boolean>		unlocked;
	/**
	 * Boolean, ob Spiel läuft
	 */
	private boolean				isRunning;
	/**
	 * Boolean ob man im Popup ist
	 */
	private boolean				inPopup;
	/**
	 * Boolean, ob Spiel abgeschlossen ist
	 */
	private boolean				isOver;
	/**
	 * Aktuell gedrückte Taste
	 */
	private KeyCode				pressedArrowKey;
	/**
	 * Boolean, ob SHIFT gedrückt ist
	 */
	private boolean				pressedShift;
	/**
	 * Taste die gerade losgelassen wurde
	 */
	private ArrayList<KeyCode>	releasedArrowKeys;
	/**
	 * Boolean, ob SHIFT gedrückt ist
	 */
	private boolean				shiftReleased;
	/**
	 * Richtung in die die Spielfigur zeigt
	 */
	private String				playerDirection;
	/**
	 * Wert, mit wie vielen Punkten der Level im aktuellen Durchgang beendet wurde
	 */
	private int					finishedPositive;
	/**
	 * Boolean, ob der Level mit 0 Punkten oder durch Tod beendet wurde
	 */
	private boolean				finishedNegative;
	/**
	 * Liste an Soundevents die im aktuellen Tick vorkommen
	 */
	private List<SoundEvent>	soundList;
	/**
	 * Anzahl an im Spielverlauf getöteten Gegner
	 */
	private int					enemiesKilled;
	/**
	 * Anzahl an im Spielverlauf aufgetretenen Explosionen
	 */
	private int					explosionsOccurred;
	/**
	 * Anzahl an im Spielverlauf verschobenen Steinen
	 */
	private int					stonesPushed;
	/**
	 * Boolean, ob turbomodus aktiviert ist
	 */
	private boolean				turbo;
	/**
	 * Zeitwert für den Turbomodus
	 */
	private int					turbotime;
	/**
	 * Boolean, ob der aktuelle Tick gezählt werden soll
	 */
	private boolean				tickcount;
	/**
	 * boolean für Craft-Mode
	 */
	private boolean				craftmode;
	/**
	 * Wert, ob der Leveleditor gerade läuft
	 */
	private boolean				levelEditorRunning;
	/**
	 * Eingestellte Breite im Leveleditor
	 */
	private int					widthLE;
	/**
	 * Eingestellte Höhe im Leveleditor
	 */
	private int					heightLE;
	/**
	 * Größe der Spielfelder im Leveleditor
	 */
	private int					rectWidth;
	/**
	 * Doppeltes Array für gerade geladene Spielfelder im Leveleditor
	 */
	private Spielfeld[][]		levelEditorCells;
	/**
	 * Gerade ausgewähltes Token im Leveleditor
	 */
	private Token				selectedToken;
	/**
	 * Timeline für den Spielverlauf
	 */
	private Timeline			timeline;
	
	public Model() {
		this.gems = 0;
		this.ticks = 0;
		this.minutes = 0;
		this.seconds = 0;
		this.x = 0;
		this.y = 0;
		this.z = 0;
		this.currentLevel = null;
		this.levelindex = 0;
		this.points = readScore("data/score.json");
		this.unlocked = calculateUnlocked();
		this.isRunning = false;
		this.inPopup = false;
		this.isOver = false;
		this.pressedArrowKey = null;
		this.pressedShift = false;
		this.releasedArrowKeys = new ArrayList<>();
		this.shiftReleased = false;
		this.playerDirection = "left";
		this.finishedPositive = 0;
		this.finishedNegative = false;
		this.soundList = new ArrayList<>();
		this.enemiesKilled = 0;
		this.explosionsOccurred = 0;
		this.stonesPushed = 0;
		this.turbo = false;
		this.turbotime = 0;
		this.tickcount = true;
		this.craftmode = false;
		
		this.levelEditorRunning = false;
		this.widthLE = 0;
		this.heightLE = 0;
		this.rectWidth = 0;
		this.levelEditorCells = null;
		this.selectedToken = MUD;
		
		this.timeline = new Timeline(new KeyFrame(Duration.millis(200), event -> tick()));
		this.timeline.setCycleCount(Timeline.INDEFINITE);
		this.timeline.play();
	}
	
	/**
	 * Liest den gespeicherten Spielstand ein. Bei Fehler wird der Spielstand für alle Level auf 0 gesetzt und eine
	 * Fehlermeldung gezeigt.
	 *
	 * @param dateiname
	 *            der JSON-Datei mit dem Spielstand
	 * @return eine Liste mit den erreichten Punkten pro Level (0 = noch nicht bestanden)
	 */
	private List<Integer> readScore(String dateiname) {
		List<Integer> score = new ArrayList<Integer>();
		try {
			JSONArray JSONScore = new JSONObject(new JSONTokener(new FileInputStream(dateiname))).getJSONArray("score");
			for (int i = 0; i < NUMBEROFLEVELS; i++) {
				score.add(JSONScore.getInt(i));
			}
			return score;
		} catch (FileNotFoundException | JSONException e) {
			if (e instanceof FileNotFoundException) System.out.println("Keine Spielstanddatei gefunden!");
			if (e instanceof JSONException) System.out.println("Fehler beim Einlesen des Spielstandes!");
			score.clear();
			for (int i = 0; i < NUMBEROFLEVELS; i++) {
				score.add(0);
			}
			return score;
		}
	}
	
	/**
	 * Speichert den erreichten Spielstand im JSON-Format ab.
	 *
	 * @param dateiname
	 *            der zu schreibenden Spielstanddatei
	 */
	public void saveScore(String dateiname) {
		JSONObject JSONScore = new JSONObject();
		JSONArray scoreList = new JSONArray();
		for (int p : this.points) {
			scoreList.put(p);
		}
		JSONScore.put("score", scoreList);
		
		try {
			FileWriter file = new FileWriter(dateiname);
			file.write(JSONScore.toString());
			file.close();
		} catch (IOException e) {
			System.out.println("Fehler beim Speichern des Spielstandes!");
		}
	}
	
	/**
	 * Speichert aktuell im Craftmodus geladenen Levelzustand in JSON-Datei ab
	 * 
	 * @param dateiname
	 *            der zu schreibenden Datei
	 */
	public void saveLevel_craft(String dateiname) {
		String JSONName = "Im Craft-Modus erzeugtes Level";
		int JSONWidth = currentLevel.getWidth();
		int JSONHeight = currentLevel.getHeight();
		
		JSONArray JSONGems = new JSONArray();
		for (int i = 0; i < 3; i++) {
			JSONGems.put(i + 1);
		}
		JSONArray JSONTicks = new JSONArray();
		for (int i = 3; i > 0; i--) {
			JSONTicks.put(250 * i);
		}
		JSONArray JSONMap = new JSONArray();
		for (int x = 0; x < JSONWidth; x++) {
			JSONArray currentJSONRow = new JSONArray();
			for (int y = 0; y < JSONHeight; y++) {
				Spielfeld currentRealField = currentLevel.getField(y, x);
				JSONObject currentJSONField = new JSONObject();
				
				JSONObject currentValues = new JSONObject();
				for (Value v : currentRealField.getValueMap().keySet()) {
					int value = currentRealField.getValue(v);
					if (value > 0) currentValues.put(v.toString(), value);
				}
				
				currentJSONField.put("token", currentRealField.getToken().toString());
				currentJSONField.put("values", currentValues);
				currentJSONRow.put(currentJSONField);
			}
			JSONMap.put(currentJSONRow);
		}
		
		saveLevel(dateiname, JSONName, JSONWidth, JSONHeight, JSONGems, JSONTicks, JSONMap);
	}
	
	/**
	 * Speichert aktuell im Leveleditor geladenen Levelzustand in JSON-Datei ab
	 * 
	 * @param dateiname
	 *            der zu schreibenden Datei
	 */
	public void saveLevel_editor(String dateiname) {
		String JSONName = "Im Levelditor erzeugtes Level";
		int JSONWidth = widthLE;
		int JSONHeight = heightLE;
		
		JSONArray JSONGems = new JSONArray();
		for (int i = 0; i < 3; i++) {
			JSONGems.put(i + 1);
		}
		JSONArray JSONTicks = new JSONArray();
		for (int i = 3; i > 0; i--) {
			JSONTicks.put(250 * i);
		}
		JSONArray JSONMap = new JSONArray();
		for (int x = 0; x < widthLE; x++) {
			JSONArray currentJSONRow = new JSONArray();
			for (int y = 0; y < heightLE; y++) {
				currentJSONRow.put(levelEditorCells[y][x].getToken().toString());
			}
			JSONMap.put(currentJSONRow);
		}
		
		saveLevel(dateiname, JSONName, JSONWidth, JSONHeight, JSONGems, JSONTicks, JSONMap);
	}
	
	/**
	 * Speichert Level im JSON-Format ab
	 */
	private void saveLevel(String dateiname, String JSONName, int JSONWidth, int JSONHeight, JSONArray JSONGems,
			JSONArray JSONTicks, JSONArray JSONMap) {
		JSONObject JSONLevel = new JSONObject();
		JSONLevel.put("name", JSONName);
		JSONLevel.put("width", JSONWidth);
		JSONLevel.put("height", JSONHeight);
		JSONLevel.put("gems", JSONGems);
		JSONLevel.put("ticks", JSONTicks);
		JSONLevel.put("map", JSONMap);
		
		try {
			FileWriter file = new FileWriter(dateiname);
			file.write(JSONLevel.toString());
			file.close();
		} catch (IOException e) {
			System.out.println("Fehler beim Speichern des Levels!");
		}
	}
	
	/**
	 * Berechnet für jeden Level, ob er schon freigeschaltet wurde
	 */
	public List<Boolean> calculateUnlocked() {
		int sum = 0;
		for (int i : this.points) {
			sum += i;
		}
		
		List<Boolean> unlocked = new ArrayList<Boolean>();
		for (int i = 0; i < NUMBEROFLEVELS; i++) {
			if (sum >= 2 * i) unlocked.add(true);
			else unlocked.add(false);
		}
		return unlocked;
	}
	
	/**
	 * Initalisieren des Leveleditors
	 */
	public void levelEditorArray() {
		this.levelEditorCells = new Spielfeld[this.widthLE][this.heightLE];
		for (int x = 0; x < this.widthLE; x++) {
			for (int y = 0; y < this.heightLE; y++) {
				Spielfeld lec = new Spielfeld(this, selectedToken.toString());
				levelEditorCells[x][y] = lec;
				lec.setToken(getSelectedToken());
			}
		}
		setChanged();
		notifyObservers();
	}
	
	/**
	 * Setzt den Level, der gespielt werden soll
	 *
	 * @param dateiname
	 *            der JSON-Datei mit den Daten des zu spielenden Levels
	 */
	public void setLevel(String dateiname, int index) {
		try {
			this.currentLevel = new Level(this, dateiname);
			this.levelindex = index;
			
			this.gems = 0;
			this.ticks = 0;
			this.minutes = (currentLevel.getTicks().get(0) / 5) / 60;
			this.seconds = (currentLevel.getTicks().get(0) / 5) % 60;
			this.x = 0;
			this.y = 0;
			this.z = 0;
			this.playerDirection = "left";
			this.soundList = new ArrayList<SoundEvent>();
			this.enemiesKilled = 0;
			this.explosionsOccurred = 0;
			this.stonesPushed = 0;
			this.turbo = false;
			this.turbotime = 0;
			this.tickcount = true;
			this.craftmode = false;
			
			this.timeline.stop();
			this.timeline = new Timeline(new KeyFrame(Duration.millis(200), event -> tick()));
			this.timeline.setCycleCount(Timeline.INDEFINITE);
			this.timeline.play();
		} catch (JSONException e) {
			System.out.println("Fehler beim Lesen der Leveldaten!");
		} catch (FileNotFoundException e) {
			System.out.println("Die Daten des gewählten Levels wurden nicht gefunden!");
		}
	}
	
	/**
	 * Ereignisse, die sich in jedem Tick des aktiven Spiels wiederholen
	 */
	private void tick() {
		if (isRunning) {
			
			this.soundList.clear();
			if (turbo) {
				turbotime++;
				if (turbotime == 100) deactivateTurboMode();
			}
			
			if (!craftmode && !isOver) {
				if (turbo) tickcount = !tickcount;
				if (tickcount) {
					ticks++;
					if (ticks % 5 == 0) {
						seconds--;
						if (seconds < 0) {
							minutes--;
							seconds = 59;
						}
					}
				}
				MainRules.gameOver(this);
				
				// 1. Zurücksetzen der Zusatzwerte aller Felder
				for (Spielfeld[] currentRow : currentLevel.getMap()) {
					for (Spielfeld currentField : currentRow) {
						Token currentToken = currentField.getToken();
						
						currentField.setVisited(false);
						currentField.setValue(MOVED, 0);
						currentField.setValue(FALLING, 0);
						currentField.setValue(BAM, 0);
						currentField.setValue(BAMRICH, 0);
						
						if (currentToken == STONE || currentToken == GEM || currentToken == TURBOTIME
								|| currentToken == HOURGLASS) {
							currentField.setValue(LOOSE, 1);
						} else {
							currentField.setValue(LOOSE, 0);
						}
						
						if (currentToken == STONE || currentToken == GEM || currentToken == TURBOTIME
								|| currentToken == HOURGLASS || currentToken == BRICKS) {
							currentField.setValue(SLIPPERY, 1);
						} else {
							currentField.setValue(SLIPPERY, 0);
						}
						
						if (currentToken == STONE) {
							currentField.setValue(PUSHABLE, 1);
						} else {
							currentField.setValue(PUSHABLE, 0);
						}
					}
				}
				
				// 2. Levelregeln pre
				for (LevelRule preRule : currentLevel.getPre()) {
					PreAndPostRules.checkSituation(this, preRule);
				}
				
				// 3. Hauptregeln
				if (pressedArrowKey != null) MainRules.move(this);
				MainRules.gravitation(this);
				MainRules.moveEnemy(this);
				MainRules.slime(this);
				MainRules.kamehameha(this);
				MainRules.explosion(this);
				
				// 4. Levelregeln post
				for (LevelRule postRule : currentLevel.getPost()) {
					PreAndPostRules.checkSituation(this, postRule);
				}
			}
			
			else if (pressedArrowKey != null) MainRules.craftmove(this);
			
			// 5. Spielzustand wird angezeigt und gedrückte Tasten werden zurückgesetzt
			clearKeys();
			setChanged();
			notifyObservers();
		}
	}
	
	/**
	 * Setzen eines Tokens im Leveleditor
	 */
	public void setLevelEditorToken(int x, int y) {
		levelEditorCells[x][y].setToken(this.getSelectedToken());
		setChanged();
		notifyObservers();
	}
	
	/**
	 * Aktiviert Turbomode, setzt Turbotime auf 0 zurück
	 */
	public void activateTurboMode() {
		this.turbo = true;
		this.turbotime = 0;
		this.tickcount = false;
		
		this.timeline.stop();
		this.timeline = new Timeline(new KeyFrame(Duration.millis(100), event -> tick()));
		this.timeline.setCycleCount(Timeline.INDEFINITE);
		this.timeline.play();
	}
	
	/**
	 * Deaktiviert Turbomode, setzt turbotime auf 0 zurück
	 */
	private void deactivateTurboMode() {
		this.turbo = false;
		this.turbotime = 0;
		this.tickcount = true;
		
		this.timeline.stop();
		this.timeline = new Timeline(new KeyFrame(Duration.millis(200), event -> tick()));
		this.timeline.setCycleCount(Timeline.INDEFINITE);
		this.timeline.play();
	}
	
	/**
	 * Gibt dem Spieler 10 Sekunden mehr Zeit
	 */
	public void getMoreTime() {
		this.ticks -= 50;
		this.seconds += 10;
		if (this.seconds >= 60) {
			this.minutes += 1;
			this.seconds -= 60;
		}
	}
	
	/**
	 * Toggelt die Timeline
	 */
	public void startStopTimer() {
		if (getRunning()) {
			timeline.stop();
			setRunning(false);
		} else {
			timeline.play();
			setRunning(true);
		}
	}
	
	/**
	 * Stoppt die Timeline
	 */
	public void stopTimer() {
		timeline.stop();
		setRunning(false);
	}
	
	/**
	 * Zurücksetzen aller gespeicherten Tasten
	 */
	private void clearKeys() {
		if (this.releasedArrowKeys != null) {
			for (KeyCode key : this.releasedArrowKeys) {
				if (this.pressedArrowKey != null && this.pressedArrowKey.equals(key)) {
					this.pressedArrowKey = null;
				} else if (this.pressedArrowKey != null) {
					key = null;
				}
			}
		}
		
		if (this.shiftReleased) {
			setPressedShift(false);
			this.shiftReleased = false;
		}
	}
	
	/**
	 * Hinzufügen von Keys die Released wurden
	 */
	public void addReleasedArrowKeys(KeyCode key) {
		this.releasedArrowKeys.add(key);
	}
	
	/**
	 * @return Anzahl der gesammelten Edelsteine
	 */
	public int getGems() {
		return gems;
	}
	
	/**
	 * @return Anzahl der vergangenen Ticks
	 */
	public int getTicks() {
		return ticks;
	}
	
	/**
	 * @return Minutenteil der noch verbleibenden Zeit
	 */
	public int getMinutes() {
		return minutes;
	}
	
	/**
	 * @return Sekundenteil der noch verbleibenden Zeit
	 */
	public int getSeconds() {
		return seconds;
	}
	
	/**
	 * @return Wert der globalen Bedingung X
	 */
	public int getX() {
		return x;
	}
	
	/**
	 * @return Wert der globalen Bedingung Y
	 */
	public int getY() {
		return y;
	}
	
	/**
	 * @return Wert der globalen Bedingung Z
	 */
	public int getZ() {
		return z;
	}
	
	/**
	 * @return Aktuell geladener Level
	 */
	public Level getCurrentlevel() {
		return currentLevel;
	}
	
	/**
	 * @return Index des aktuell geladenen Levels
	 */
	public int getLevelindex() {
		return levelindex;
	}
	
	/**
	 * @return Punkte pro Level (0 = noch nicht bestanden)
	 */
	public List<Integer> getPoints() {
		return points;
	}
	
	/**
	 * @return Wert ob jeder Level schon freigeschaltet ist
	 */
	public List<Boolean> getUnlocked() {
		return unlocked;
	}
	
	/**
	 * @return Boolean, ob Spiel läuft
	 */
	public boolean getRunning() {
		return isRunning;
	}
	
	/**
	 * @return Boolean, ob in Popup
	 */
	public boolean getInPopup() {
		return inPopup;
	}
	
	/**
	 * @return Aktuell gedrückte Taste
	 */
	public KeyCode getPressedKey() {
		return pressedArrowKey;
	}
	
	/**
	 * @return Boolean, ob SHIFT gedrückt ist
	 */
	public boolean getPressedShift() {
		return pressedShift;
	}
	
	/**
	 * @return Richtung in die die Spielfigur zeigt
	 */
	public String getPlayerDirection() {
		return playerDirection;
	}
	
	/**
	 * @return Wert, mit wie vielen Punkten der Level im aktuellen Durchgang beendet wurde
	 */
	public int getFinishedPos() {
		return finishedPositive;
	}
	
	/**
	 * @return Boolean, ob der Level mit 0 Punkten oder durch Tod beendet wurde
	 */
	public boolean getFinishedNeg() {
		return finishedNegative;
	}
	
	/**
	 * @return Sound, der am Ende des Ticks abgespielt werden soll
	 */
	public List<SoundEvent> getSoundList() {
		return soundList;
	}
	
	/**
	 * @return Anzahl an im Spielverlauf getöteten Gegner
	 */
	public int getEnemiesKilled() {
		return enemiesKilled;
	}
	
	/**
	 * @return Anzahl an im Spielverlauf aufgetretenen Explosionen
	 */
	public int getExplosionsOccurred() {
		return explosionsOccurred;
	}
	
	/**
	 * @return Anzahl an im Spielverlauf verschobenen Steinen
	 */
	public int getStonesPushed() {
		return stonesPushed;
	}
	
	/**
	 * @return boolean für turbomode
	 */
	public boolean getTurboActive() {
		return turbo;
	}
	
	/**
	 * getter für Craft-Mode
	 */
	public boolean getCrafmode() {
		return craftmode;
	}
	
	/**
	 * @return Wert, ob der Leveleditor gerade läuft
	 */
	public boolean getLevelEditorRunning() {
		return this.levelEditorRunning;
	}
	
	/**
	 * @return Eingestellte Breite im Leveleditor
	 */
	public int getWidthLE() {
		return widthLE;
	}
	
	/**
	 * @return Eingestellte Höhe im Leveleditor
	 */
	public int getHeightLE() {
		return heightLE;
	}
	
	/**
	 * @return Größe der Spielfelder im Leveleditor
	 */
	public int getRectWidth() {
		return rectWidth;
	}
	
	/**
	 * @return Aktuell geladenes Spielfeld im Leveleditor
	 */
	public Spielfeld getLevelEditorGrid(int x, int y) {
		return this.levelEditorCells[x][y];
	}
	
	/**
	 * @return Gerade ausgewählte Token im Leveleditor
	 */
	public Token getSelectedToken() {
		return selectedToken;
	}
	
	/**
	 * Überschreibt die globalen Variablen X, Y, Z, Gems & Ticks
	 */
	public void setXYZ(Integer newX, Integer newY, Integer newZ, Integer newGems, Integer newTicks) {
		if (newX != null) {
			if (newX != 0) {
				this.x += newX;
			} else this.x = 0;
			if (this.x < 0) this.x = 0;
		}
		
		if (newY != null) {
			if (newY != 0) {
				this.y += newY;
			} else this.y = 0;
			if (this.y < 0) this.y = 0;
		}
		
		if (newZ != null) {
			if (newZ != 0) {
				this.z += newZ;
			} else this.z = 0;
			if (this.z < 0) this.z = 0;
		}
		
		if (newGems != null) {
			if (newGems != 0) {
				this.gems += newGems;
			} else this.gems = 0;
			if (this.gems < 0) this.gems = 0;
		}
		
		if (newTicks != null) {
			if (newTicks != 0) {
				this.ticks += newTicks;
				this.seconds -= newTicks / 5;
				if (this.seconds < 0) {
					this.minutes--;
					this.seconds += 60;
				}
			} else {
				this.ticks = 0;
				this.minutes = (currentLevel.getTicks().get(0) / 5) / 60;
				this.seconds = (currentLevel.getTicks().get(0) / 5) % 60;
			}
			if (this.ticks < 0) {
				this.ticks = 0;
				this.minutes = (currentLevel.getTicks().get(0) / 5) / 60;
				this.seconds = (currentLevel.getTicks().get(0) / 5) % 60;
			}
		}
	}
	
	/**
	 * Setter für Punkte eines Levels
	 */
	public void setPoints(int level, int points) {
		this.points.set(level, points);
	}
	
	/**
	 * Setter für Wert, ob Spiel läuft
	 */
	public void setRunning(boolean bool) {
		isRunning = bool;
	}
	
	/**
	 * Setter für Wert, in Popup
	 */
	public void setInPopup(boolean bool) {
		this.inPopup = bool;
	}
	
	/**
	 * Deaktivieren von isOver
	 */
	public void setIsOver() {
		this.isOver = false;
	}
	
	/**
	 * Setter für aktuell gedrückte Taste
	 */
	public void setPressedArrowKey(KeyCode pressedKey) {
		pressedArrowKey = pressedKey;
	}
	
	/**
	 * Setter für Wert, ob SHIFT gedrückt ist
	 */
	public void setPressedShift(boolean bool) {
		this.pressedShift = bool;
	}
	
	/**
	 * Setzt bool ob Shift losgelassen wurde
	 */
	public void setShiftReleased(boolean bool) {
		this.shiftReleased = true;
	}
	
	/**
	 * Setter für Richtung in die Spielfigur zeigt
	 */
	public void setPlayerDirection(String direction) {
		this.playerDirection = direction;
	}
	
	/**
	 * Setter für Wert, mit wie vielen Punkten der aktuelle Level positiv beendet wurde
	 */
	public void setFinishedPos(int points) {
		this.unlocked = calculateUnlocked();
		this.finishedPositive = points;
		if (points > 0) this.soundList.add(APPLAUSE);
		this.isOver = true;
	}
	
	/**
	 * Setter für Wert, ob der aktuelle Level negativ beendet wurde
	 */
	public void setFinishedNeg(boolean bool) {
		this.unlocked = calculateUnlocked();
		this.finishedNegative = bool;
		if (!bool) this.soundList.add(GAMEOVER);
		this.isOver = true;
	}
	
	/**
	 * Setter für Craft-Mode
	 */
	public void setCrafmode(boolean bool) {
		this.craftmode = bool;
	}
	
	/**
	 * Setter für Wert, ob der Leveleditor gerade läuft
	 */
	public void setLevelEditorRunning(boolean bool) {
		this.levelEditorRunning = bool;
	}
	
	/**
	 * Setter für Breite des Leveleditors
	 */
	public void setWidthLE(int w) {
		this.widthLE = w;
	}
	
	/**
	 * Setter für Höhe des Leveleditors
	 */
	public void setHeightLE(int h) {
		this.heightLE = h;
	}
	
	/**
	 * Setter für Breite der Rectangles des Leveleditors
	 */
	public void setRectWidth(int w) {
		this.rectWidth = w;
	}
	
	/**
	 * Setter für ausgewählten Token im Leveleditor
	 */
	public void setSelectedToken(Token token) {
		this.selectedToken = token;
	}
	
	/**
	 * Fügt SoundType zur Liste des aktuellen Ticks hinzu
	 */
	public void appendSound(SoundEvent sound) {
		this.soundList.add(sound);
	}
	
	/**
	 * Erhöhen des gesammelten Edelsteine um eins
	 */
	public void incrementGems() {
		this.soundList.add(DIAMOND);
		this.gems++;
	}
	
	/**
	 * Erhöhen der Anzahl an im Spielverlauf getöteten Gegner
	 */
	public void incrementEnemiesKilled() {
		this.enemiesKilled++;
	}
	
	/**
	 * Erhöhen der Anzahl an im Spielverlauf aufgetretenen Explosionen
	 */
	public void incrementExplosions() {
		this.explosionsOccurred++;
	}
	
	/**
	 * Erhöhen der Anzahl an im Spielverlauf verschobenen Steine
	 */
	public void incrementPushes() {
		this.stonesPushed++;
	}
	
}