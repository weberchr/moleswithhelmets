package model;

import java.util.Map;

import static model.Token.*;

/**
 * Abstrakte Klasse für Spielfelder & Regelblöcke
 */
public abstract class FeldBlock {
	
	/**
	 * An das Feld oder den Block übergebene Model
	 */
	protected Model					model;
	/**
	 * Werte der Feldeigenschaften
	 */
	protected Map<Value, Integer>	values;
	/**
	 * Token, falls er ein numerischer Wert ist (-1 falls nicht)
	 */
	protected int					number;
	
	/**
	 * @return Werte der Feldeigenschaften
	 */
	public Map<Value, Integer> getValueMap() {
		return values;
	}
	
	/**
	 * @param key
	 *            Name des gefragten Wertes
	 * @return Aktueller Wert einer globalen Bedingung oder einer lokalen Bedingung des Feldes
	 */
	public int getValue(Value key) {
		return this.values.get(key);
	}
	
	/**
	 * @return Token, falls er ein numerischer Wert ist
	 */
	public int getNumber() {
		return number;
	}
	
	/**
	 * @param key
	 *            Name des zu hinzufügenden oder ändernden Wertes
	 * @param value
	 *            Wert des zu hinzufügenden oder ändernden Wertes
	 */
	public void setValue(Value key, int value) {
		this.values.put(key, value);
	}
	
	/**
	 * Updaten von mehreren Values
	 */
	public void setValues(Map<Value, Integer> valueMap) {
		for (Value fieldVal : valueMap.keySet()) {
			this.values.put(fieldVal, valueMap.get(fieldVal));
		}
	}
	
	/**
	 * Addieren von mehreren Values
	 */
	public void addValues(Map<Value, Integer> valueMap) {
		for (Value fieldValue : valueMap.keySet()) {
			Integer fieldVal = valueMap.get(fieldValue);
			Integer thisVal = this.values.get(fieldValue);
			if (thisVal == null) thisVal = 0;
			
			if (fieldVal != null) {
				if (fieldVal != 0) {
					int newval = fieldVal + thisVal;
					this.values.put(fieldValue, newval);
				} else this.values.put(fieldValue, 0);
				if (thisVal < 0) this.values.put(fieldValue, 0);
			}
		}
	}
	
	/**
	 * Zurücksetzen aller Values auf 0
	 */
	public void clearValues() {
		for (Value value : this.values.keySet()) {
			if (this.values.get(value) != null) {
				this.values.put(value, 0);
			}
		}
	}
	
	/**
	 * Wandelt String in entsprechenden Token um
	 */
	protected Token convertToken(String token) {
		try {
			this.number = Integer.parseInt(token);
			return NUMBER;
		} catch (NumberFormatException e) {
			this.number = -1;
			switch (token) {
				case "me":
					return ME;
				case "mud":
					return MUD;
				case "stone":
					return STONE;
				case "gem":
					return GEM;
				case "exit":
					return EXIT;
				case "wall":
					return WALL;
				case "bricks":
					return BRICKS;
				case "path":
					return PATH;
				case "explosion":
					return EXPLOSION;
				case "slime":
					return SLIME;
				case "swapling":
					return SWAPLING;
				case "blockling":
					return BLOCKLING;
				case "xling":
					return XLING;
				case "ghostling":
					return GHOSTLING;
				case "fire":
					return FIRE;
				case "norththing":
					return NORTHTHING;
				case "eastthing":
					return EASTTHING;
				case "souththing":
					return SOUTHTHING;
				case "westthing":
					return WESTTHING;
				case "pot":
					return POT;
				case "sieve":
					return SIEVE;
				case "sand":
					return SAND;
				case "turbotime":
					return TURBOTIME;
				case "hourglass":
					return HOURGLASS;
				case "kame":
					return KAME;
				case "*":
					return ANY;
				default:
					throw new IllegalArgumentException("In den Leveldaten befindet sich ein unbekannter Token!");
			}
		}
	}
	
	/**
	 * Wandelt Token in entsprechenden String um
	 */
	protected String convertToken(Token token) {
		if (token == NUMBER) return Integer.toString(this.number);
		else return token.toString();
	}
	
}
