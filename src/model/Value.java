package model;

/**
 * Zusatzwerte für Spielfelder und Regelblöcke
 */
public enum Value {
	
	/**
	 * Hat sich im aktuellen Tick bewegt
	 */
	MOVED("moved"),
	/**
	 * Fällt gerade hinunter
	 */
	FALLING("falling"),
	/**
	 * Ist locker und kann hinunterfallen
	 */
	LOOSE("loose"),
	/**
	 * Rutschig, leitet fallende Gegenstände um
	 */
	SLIPPERY("slippery"),
	/**
	 * Lässt sich wegschieben
	 */
	PUSHABLE("pushable"),
	/**
	 * Wird zu {@link Token#EXPLOSION} explodieren
	 */
	BAM("bam"),
	/**
	 * Wird zu {@link Token#GEM} explodieren
	 */
	BAMRICH("bamrich"),
	/**
	 * Bewegungsrichtung
	 */
	DIRECTION("direction"),
	/**
	 * Levelabhängiger Wert A
	 */
	A("A"),
	/**
	 * Levelabhängiger Wert B
	 */
	B("B"),
	/**
	 * Levelabhängiger Wert C
	 */
	C("C"),
	/**
	 * Levelabhängiger Wert D
	 */
	D("D"),
	/**
	 * Referenz auf {@link Model#getGems()}
	 */
	GEMS("gems"),
	/**
	 * Referenz auf {@link Model#getTicks()}
	 */
	TICKS("ticks"),
	/**
	 * Referenz auf {@link Model#getX()}
	 */
	X("X"),
	/**
	 * Referenz auf {@link Model#getY()}
	 */
	Y("Y"),
	/**
	 * Referenz auf {@link Model#getZ()}
	 */
	Z("Z");
	
	/**
	 * Stringrepräsentation des Wertes
	 */
	private String value;
	
	Value(final String v) {
		this.value = v;
	}
	
	/**
	 * @return Stringrepräsentation des Wertes
	 */
	@Override
	public String toString() {
		return this.value;
	}
	
}