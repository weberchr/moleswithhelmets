package model;

/**
 * Objekte, die sich auf Spielfeldern oder Regelblöcken befinden können
 */
public enum Token {
	
	/**
	 * Spielfigur
	 */
	ME("me"),
	/**
	 * Begehbarer Pfad
	 */
	PATH("path"),
	/**
	 * Abzugrabende Erde
	 */
	MUD("mud"),
	/**
	 * Stein
	 */
	STONE("stone"),
	/**
	 * Ziegelstein
	 */
	BRICKS("bricks"),
	/**
	 * Mauer
	 */
	WALL("wall"),
	/**
	 * Sand mit levelabhängigem Verhalten
	 */
	SAND("sand"),
	/**
	 * Edelstein
	 */
	GEM("gem"),
	/**
	 * Levelausgang
	 */
	EXIT("exit"),
	/**
	 * Schleim
	 */
	SLIME("slime"),
	/**
	 * Gegnertyp 1
	 */
	SWAPLING("swapling"),
	/**
	 * Gegnertyp 2
	 */
	BLOCKLING("blockling"),
	/**
	 * Gegnertyp 3
	 */
	XLING("xling"),
	/**
	 * Levelspezifischer Gegnertyp
	 */
	GHOSTLING("ghostling"),
	/**
	 * Feuer mit levelabhängigem Verhalten
	 */
	FIRE("fire"),
	/**
	 * Explodiert gerade
	 */
	EXPLOSION("explosion"),
	/**
	 * Nach Norden zeigender Gegenstand mit levelabhängigem Verhalten
	 */
	NORTHTHING("norththing"),
	/**
	 * Nach Osten zeigender Gegenstand mit levelabhängigem Verhalten
	 */
	EASTTHING("eastthing"),
	/**
	 * Nach Süden zeigender Gegenstand mit levelabhängigem Verhalten
	 */
	SOUTHTHING("souththing"),
	/**
	 * Nach Westen zeigender Gegenstand mit levelabhängigem Verhalten
	 */
	WESTTHING("norththing"),
	/**
	 * Kessel mit levelabhängigem Verhalten
	 */
	POT("pot"),
	/**
	 * Sieb mit levelabhängigem Verhalten
	 */
	SIEVE("sieve"),
	/**
	 * Erhöht temporär Tempo des Spielverlaufs
	 */
	TURBOTIME("turbotime"),
	/**
	 * Gibt mehr Zeit
	 */
	HOURGLASS("hourglass"),
	/**
	 * Munition mit levelabhängigem Verhalten
	 */
	KAME("kame"),
	/**
	 * Beliebiger Wert (in Regelblöcken)
	 */
	ANY("*"),
	/**
	 * Referenz auf Token aus original (in Regelblöcken)
	 */
	NUMBER(null);
	
	/**
	 * Stringrepräsentation des Tokens
	 */
	private String token;
	
	Token(final String t) {
		this.token = t;
	}
	
	/**
	 * @return Stringrepräsentation des Tokens
	 */
	@Override
	public String toString() {
		return this.token;
	}
	
	/**
	 * @return Token, der in der Auflistung hinter dem aktuellem liegt; außer es wäre {@link Token#ME},
	 *         {@link Token#EXPLOSION}, {@link Token#ANY} oder {@link Token#NUMBER}
	 */
	public Token next() {
		Token toReturn = values()[(this.ordinal() + 1) % values().length];
		while (toReturn == ME || toReturn == EXPLOSION || toReturn == ANY || toReturn == NUMBER) {
			toReturn = toReturn.next();
		}
		return toReturn;
	}
	
}