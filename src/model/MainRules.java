package model;

import javafx.scene.input.KeyCode;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import static javafx.scene.input.KeyCode.*;
import static model.SoundEvent.*;
import static model.Token.*;
import static model.Value.*;

/**
 * Berechnen der Hauptregeln
 */
public final class MainRules {
	
	private MainRules() {
	}
	
	/**
	 * Überprüft, ob man sich noch im Spielbereich befindet
	 */
	private static boolean boundaryControl(int x, int y, Level currentlevel) {
		return (x >= 0 && y >= 0 && x < currentlevel.getWidth() && y < currentlevel.getHeight());
	}
	
	/**
	 * Überprüfen, ob das Spiel zu Ende ist
	 */
	public static void gameOver(Model model) {
		Level currentlevel = model.getCurrentlevel();
		Boolean meExists = false;
		
		for (int x = 0; x < currentlevel.getWidth(); x++) {
			for (int y = 0; y < currentlevel.getHeight(); y++) {
				if (currentlevel.getField(x, y).getToken() == ME) {
					meExists = true;
				}
			}
		}
		if (!meExists || model.getTicks() >= model.getCurrentlevel().getTicks().get(0)) model.setFinishedNeg(true);
	}
	
	/**
	 * Spielerbewegung
	 */
	public static void move(Model model) {
		Level currentlevel = model.getCurrentlevel();
		
		KeyCode key = model.getPressedKey();
		int nextX = 0;
		int nextY = 0;
		int nextXPush = 0;
		Value value = null;
		
		if (key == UP) {
			nextY--;
			model.setPlayerDirection("up");
			value = Value.A;
		} else if (key == DOWN) {
			nextY++;
			model.setPlayerDirection("down");
			value = Value.B;
		} else if (key == LEFT) {
			nextX--;
			nextXPush -= 2;
			model.setPlayerDirection("left");
			value = Value.C;
		} else if (key == RIGHT) {
			nextX++;
			nextXPush += 2;
			model.setPlayerDirection("right");
			value = Value.D;
		}
		
		Spielfeld meField = null;
		Spielfeld nextField = null;
		Spielfeld nextFieldPush = null;
		
		for (int x = 0; x < currentlevel.getWidth(); x++) {
			for (int y = 0; y < currentlevel.getHeight(); y++) {
				Spielfeld currentField = currentlevel.getField(x, y);
				if (currentField.getToken() == ME) {
					meField = currentField;
					nextX += x;
					nextY += y;
					nextXPush += x;
					if (boundaryControl(nextX, nextY, currentlevel)) {
						nextField = currentlevel.getField(nextX, nextY);
					}
					if (boundaryControl(nextXPush, nextY, currentlevel)) {
						nextFieldPush = currentlevel.getField(nextXPush, nextY);
					}
				}
			}
		}
		
		if (nextField != null) {
			Token nextToken = nextField.getToken();
			
			// normale Bewegung
			if (!model.getPressedShift() && (nextToken == PATH || nextToken == MUD || nextToken == GEM
					|| nextToken == TURBOTIME || nextToken == HOURGLASS)) {
				
				model.appendSound(STEP);
				
				if (nextToken == GEM) model.incrementGems();
				if (nextToken == TURBOTIME) model.activateTurboMode();
				if (nextToken == HOURGLASS) model.getMoreTime();
				
				meField.setToken(PATH);
				nextField.setToken(ME);
				meField.setValue(MOVED, 1);
				nextField.setValue(MOVED, 1);
			}
			
			// Bewegung auf Exit
			else if (!model.getPressedShift() && nextToken == EXIT) {
				int levelindex = model.getLevelindex();
				int myGems = model.getGems();
				int myTicks = model.getTicks();
				List<Integer> levelGems = model.getCurrentlevel().getGems();
				List<Integer> levelTicks = model.getCurrentlevel().getTicks();
				
				if (myGems >= levelGems.get(2) && myTicks <= levelTicks.get(2)) {
					model.setPoints(levelindex, Math.max(3, model.getPoints().get(levelindex)));
					model.setFinishedPos(3);
				} else if (myGems >= levelGems.get(1) && myTicks <= levelTicks.get(1)) {
					model.setPoints(levelindex, Math.max(2, model.getPoints().get(levelindex)));
					model.setFinishedPos(2);
				} else if (myGems >= levelGems.get(0) && myTicks <= levelTicks.get(0)) {
					model.setPoints(levelindex, Math.max(1, model.getPoints().get(levelindex)));
					model.setFinishedPos(1);
				}
			}
			
			// Verschieben von Pushables
			else if (!model.getPressedShift() && nextFieldPush != null && nextField.getValue(PUSHABLE) > 0
					&& nextFieldPush.getToken() == PATH) {
				model.incrementPushes();
				model.appendSound(STEP);
				
				meField.setToken(PATH);
				nextFieldPush.setToken(nextToken);
				nextField.setToken(ME);
				meField.setValue(MOVED, 1);
				nextFieldPush.setValue(MOVED, 1);
				nextField.setValue(MOVED, 1);
			}
			
			// Abgraben ohne Spielerbewegung
			else if (model.getPressedShift()) {
				if (nextToken == GEM && nextField.getValue(FALLING) == 0) {
					model.incrementGems();
					nextField.setToken(PATH);
				}
				if (nextToken == TURBOTIME && nextField.getValue(FALLING) == 0) {
					model.activateTurboMode();
					nextField.setToken(PATH);
				}
				if (nextToken == HOURGLASS && nextField.getValue(FALLING) == 0) {
					model.getMoreTime();
					nextField.setToken(PATH);
				}
				if (nextToken == MUD) {
					nextField.setToken(PATH);
				}
				if (model.getTurboActive() && nextToken != WALL) {
					nextField.setToken(KAME);
					nextField.setValue(value, 1);
					nextField.setValue(MOVED, 1);
					model.appendSound(KAMEHAME);
					if (nextToken == SWAPLING || nextToken == BLOCKLING || nextToken == XLING || nextToken == GHOSTLING)
						model.incrementEnemiesKilled();
				}
			}
		}
	}
	
	/**
	 * Gravitation
	 */
	public static void gravitation(Model model) {
		Level currentlevel = model.getCurrentlevel();
		
		for (int x = 0; x < currentlevel.getWidth(); x++) {
			for (int y = 0; y < currentlevel.getHeight(); y++) {
				Spielfeld currentField = currentlevel.getField(x, y);
				
				if (currentField.getValue(LOOSE) > 0 && currentField.getValue(MOVED) == 0
						&& boundaryControl(x, y + 1, currentlevel)) {
					Spielfeld nextField = currentlevel.getField(x, y + 1);
					int nextx = x;
					int nexty = y + 1;
					
					Spielfeld bottomleft = null;
					Spielfeld bottomright = null;
					if (boundaryControl(x - 1, y, currentlevel)) bottomleft = currentlevel.getField(x - 1, y);
					if (boundaryControl(x + 1, y, currentlevel)) bottomright = currentlevel.getField(x + 1, y);
					if (nextField.getValue(SLIPPERY) > 0) {
						if (bottomright != null && boundaryControl(x + 1, y, currentlevel)
								&& boundaryControl(x + 1, y + 1, currentlevel) && bottomright.getToken() == PATH
								&& bottomright.getValue(MOVED) == 0) {
							nextField = currentlevel.getField(x + 1, y + 1);
							nextx = x + 1;
							bottomleft = null;
						} else if (bottomleft != null && boundaryControl(x - 1, y, currentlevel)
								&& boundaryControl(x - 1, y + 1, currentlevel) && bottomleft.getToken() == PATH
								&& bottomleft.getValue(MOVED) == 0) {
							nextField = currentlevel.getField(x - 1, y + 1);
							nextx = x - 1;
							bottomright = null;
						} else {
							bottomleft = null;
							bottomright = null;
						}
					} else {
						bottomleft = null;
						bottomright = null;
					}
					
					if (nextField.getToken() == PATH && nextField.getValue(MOVED) == 0) {
						if (currentField.getToken() == STONE) model.appendSound(STONEFALLING);
						
						nextField.setToken(currentField.getToken());
						nextField.setValue(FALLING, 1);
						nextField.setValue(MOVED, 1);
						
						currentField.setToken(PATH);
						currentField.setValue(MOVED, 1);
						
						if (bottomright != null) bottomright.setValue(MOVED, 1);
						if (bottomleft != null) bottomleft.setValue(MOVED, 1);
						
						if (boundaryControl(nextx, nexty + 1, currentlevel)) {
							Spielfeld below = currentlevel.getField(nextx, nexty + 1);
							switch (below.getToken()) {
								case ME:
								case SWAPLING:
								case XLING:
									below.setValue(BAMRICH, 1);
									break;
								case BLOCKLING:
									below.setValue(BAM, 1);
									break;
								default:
									break;
							}
						}
					}
				}
			}
		}
	}
	
	/**
	 * Gegnerbewegungen
	 */
	public static void moveEnemy(Model model) {
		Level currentlevel = model.getCurrentlevel();
		
		for (int x = 0; x < currentlevel.getWidth(); x++) {
			for (int y = 0; y < currentlevel.getHeight(); y++) {
				Spielfeld currentField = currentlevel.getField(x, y);
				if (currentField.getValue(MOVED) == 0) {
					Token currentToken = currentField.getToken();
					if (currentToken == SWAPLING) moveSwapling(model, currentField, x, y);
					if (currentToken == XLING) moveXling(model, currentField, x, y);
					if (currentToken == BLOCKLING) moveBlockling(model, currentField, x, y);
				}
			}
		}
	}
	
	/**
	 * Bewegung eines Swapling
	 */
	private static void moveSwapling(Model model, Spielfeld currentField, int x, int y) {
		Level currentlevel = model.getCurrentlevel();
		List<Token> walkable = Arrays.asList(ME, PATH);
		Spielfeld nextField = null;
		
		// nach rechts
		if (currentField.getValue(DIRECTION) == 1 && currentField.getValue(MOVED) == 0) {
			if (boundaryControl(x + 1, y, currentlevel)
					&& walkable.contains(currentlevel.getField(x + 1, y).getToken())) {
				nextField = currentlevel.getField(x + 1, y);
			} else {
				currentField.setValue(DIRECTION, 3);
				if (boundaryControl(x - 1, y, currentlevel)
						&& walkable.contains(currentlevel.getField(x - 1, y).getToken())) {
					nextField = currentlevel.getField(x - 1, y);
				}
			}
			currentField.setValue(MOVED, 1);
		}
		
		// nach oben
		if (currentField.getValue(DIRECTION) == 2 && currentField.getValue(MOVED) == 0) {
			if (boundaryControl(x, y - 1, currentlevel)
					&& walkable.contains(currentlevel.getField(x, y - 1).getToken())) {
				nextField = currentlevel.getField(x, y - 1);
			} else {
				currentField.setValue(DIRECTION, 4);
				if (boundaryControl(x, y + 1, currentlevel)
						&& walkable.contains(currentlevel.getField(x, y + 1).getToken())) {
					nextField = currentlevel.getField(x, y + 1);
				}
			}
			currentField.setValue(MOVED, 1);
		}
		
		// nach links
		if (currentField.getValue(DIRECTION) == 3 && currentField.getValue(MOVED) == 0) {
			if (boundaryControl(x - 1, y, currentlevel)
					&& walkable.contains(currentlevel.getField(x - 1, y).getToken())) {
				nextField = currentlevel.getField(x - 1, y);
			} else {
				currentField.setValue(DIRECTION, 1);
				if (boundaryControl(x + 1, y, currentlevel)
						&& walkable.contains(currentlevel.getField(x + 1, y).getToken())) {
					nextField = currentlevel.getField(x + 1, y);
				}
			}
			currentField.setValue(MOVED, 1);
		}
		
		// nach unten
		if (currentField.getValue(DIRECTION) == 4 && currentField.getValue(MOVED) == 0) {
			if (boundaryControl(x, y + 1, currentlevel)
					&& walkable.contains(currentlevel.getField(x, y + 1).getToken())) {
				nextField = currentlevel.getField(x, y + 1);
			} else {
				currentField.setValue(DIRECTION, 2);
				if (boundaryControl(x, y - 1, currentlevel)
						&& walkable.contains(currentlevel.getField(x, y - 1).getToken())) {
					nextField = currentlevel.getField(x, y - 1);
				}
			}
			currentField.setValue(MOVED, 1);
		}
		
		if (currentField.getValue(DIRECTION) == 0) currentField.setValue(DIRECTION, 1);
		
		if (nextField != null) {
			moveToNextField(currentField, nextField, true);
		}
	}
	
	/**
	 * Bewegung eines Xling, der sich gemäß rechter-Hand-Regel an der Wand entlang bewegt; nach Feststellung der
	 * Richtung werden alle möglichen Richtungen in Uhrzeigersinn gecheckt
	 */
	private static void moveXling(Model model, Spielfeld currentField, int x, int y) {
		Level currentlevel = model.getCurrentlevel();
		List<Token> walkable = Arrays.asList(ME, PATH);
		Spielfeld nextField = null;
		
		// nach rechts
		if (currentField.getValue(DIRECTION) == 1 && currentField.getValue(MOVED) == 0) {
			
			// unten
			if (!walkable.contains(currentlevel.getField(x - 1, y + 1).getToken())
					&& walkable.contains(currentlevel.getField(x, y + 1).getToken())) {
				nextField = currentlevel.getField(x, y + 1);
				currentField.setValue(DIRECTION, 4);
			}
			
			// rechts
			else if (walkable.contains(currentlevel.getField(x + 1, y).getToken())
					&& !walkable.contains(currentlevel.getField(x, y + 1).getToken())) {
				nextField = currentlevel.getField(x + 1, y);
			}
			
			// oben
			else if (!walkable.contains(currentlevel.getField(x + 1, y).getToken())
					&& walkable.contains(currentlevel.getField(x, y - 1).getToken())) {
				nextField = currentlevel.getField(x, y - 1);
				currentField.setValue(DIRECTION, 2);
			}
			
			// links
			else if (!walkable.contains(currentlevel.getField(x, y - 1).getToken())) {
				if (walkable.contains(currentlevel.getField(x - 1, y).getToken())) {
					nextField = currentlevel.getField(x - 1, y);
				}
				currentField.setValue(DIRECTION, 3);
			}
			
			currentField.setValue(MOVED, 1);
		}
		
		// nach oben
		if (currentField.getValue(DIRECTION) == 2 && currentField.getValue(MOVED) == 0) {
			
			// rechts
			if (!walkable.contains(currentlevel.getField(x + 1, y + 1).getToken())
					&& walkable.contains(currentlevel.getField(x + 1, y).getToken())) {
				nextField = currentlevel.getField(x + 1, y);
				currentField.setValue(DIRECTION, 1);
			}
			
			// oben
			else if (walkable.contains(currentlevel.getField(x, y - 1).getToken())
					&& !walkable.contains(currentlevel.getField(x + 1, y).getToken())) {
				nextField = currentlevel.getField(x, y - 1);
			}
			
			// links
			else if (!walkable.contains(currentlevel.getField(x, y - 1).getToken())
					&& walkable.contains(currentlevel.getField(x - 1, y).getToken())) {
				nextField = currentlevel.getField(x - 1, y);
				currentField.setValue(DIRECTION, 3);
			}
			
			// unten
			else if (!walkable.contains(currentlevel.getField(x - 1, y).getToken())) {
				if (walkable.contains(currentlevel.getField(x, y + 1).getToken())) {
					nextField = currentlevel.getField(x, y + 1);
				}
				currentField.setValue(DIRECTION, 4);
			}
			
			currentField.setValue(MOVED, 1);
		}
		
		// nach links
		if (currentField.getValue(DIRECTION) == 3 && currentField.getValue(MOVED) == 0) {
			
			// oben
			if (!walkable.contains(currentlevel.getField(x + 1, y - 1).getToken())
					&& walkable.contains(currentlevel.getField(x, y - 1).getToken())) {
				nextField = currentlevel.getField(x, y - 1);
				currentField.setValue(DIRECTION, 2);
			}
			// links
			else if (walkable.contains(currentlevel.getField(x - 1, y).getToken())
					&& !walkable.contains(currentlevel.getField(x, y - 1).getToken())) {
				nextField = currentlevel.getField(x - 1, y);
			}
			
			// unten
			else if (!walkable.contains(currentlevel.getField(x - 1, y).getToken())
					&& walkable.contains(currentlevel.getField(x, y + 1).getToken())) {
				nextField = currentlevel.getField(x, y + 1);
				currentField.setValue(DIRECTION, 4);
			}
			
			// rechts
			else if (!walkable.contains(currentlevel.getField(x, y + 1).getToken())) {
				if (walkable.contains(currentlevel.getField(x + 1, y).getToken())) {
					nextField = currentlevel.getField(x + 1, y);
				}
				currentField.setValue(DIRECTION, 1);
			}
			
			currentField.setValue(MOVED, 1);
		}
		
		// nach unten
		if (currentField.getValue(DIRECTION) == 4 && currentField.getValue(MOVED) == 0) {
			
			// links
			if (!walkable.contains(currentlevel.getField(x - 1, y - 1).getToken())
					&& walkable.contains(currentlevel.getField(x - 1, y).getToken())) {
				nextField = currentlevel.getField(x - 1, y);
				currentField.setValue(DIRECTION, 3);
			}
			
			// unten
			else if (walkable.contains(currentlevel.getField(x, y + 1).getToken())
					&& !walkable.contains(currentlevel.getField(x - 1, y).getToken())) {
				nextField = currentlevel.getField(x, y + 1);
			}
			
			// rechts
			else if (!walkable.contains(currentlevel.getField(x, y + 1).getToken())
					&& walkable.contains(currentlevel.getField(x + 1, y).getToken())) {
				nextField = currentlevel.getField(x + 1, y);
				currentField.setValue(DIRECTION, 1);
			}
			
			// oben
			else if (!walkable.contains(currentlevel.getField(x + 1, y).getToken())) {
				if (walkable.contains(currentlevel.getField(x, y - 1).getToken())) {
					nextField = currentlevel.getField(x, y - 1);
				}
				currentField.setValue(DIRECTION, 2);
			}
			
			currentField.setValue(MOVED, 1);
		}
		
		if (currentField.getValue(DIRECTION) == 0) currentField.setValue(DIRECTION, 1);
		
		if (nextField != null) {
			moveToNextField(currentField, nextField, true);
		}
	}
	
	/**
	 * Bewegung eines Blockling, der sich gemäß linker-Hand-Regel an der Wand entlang bewegt; nach Feststellung der
	 * Richtung werden alle möglichen Richtungen in Uhrzeigersinn gecheckt
	 */
	private static void moveBlockling(Model model, Spielfeld currentField, int x, int y) {
		Level currentlevel = model.getCurrentlevel();
		List<Token> walkable = Arrays.asList(ME, PATH);
		Spielfeld nextField = null;
		
		// nach rechts
		if (currentField.getValue(DIRECTION) == 1 && currentField.getValue(MOVED) == 0) {
			
			// oben
			if (!walkable.contains(currentlevel.getField(x - 1, y - 1).getToken())
					&& walkable.contains(currentlevel.getField(x, y - 1).getToken())) {
				nextField = currentlevel.getField(x, y - 1);
				currentField.setValue(DIRECTION, 2);
			}
			
			// rechts
			else if (walkable.contains(currentlevel.getField(x + 1, y).getToken())
					&& !walkable.contains(currentlevel.getField(x, y - 1).getToken())) {
				nextField = currentlevel.getField(x + 1, y);
			}
			
			// unten
			else if (!walkable.contains(currentlevel.getField(x + 1, y).getToken())
					&& walkable.contains(currentlevel.getField(x, y + 1).getToken())) {
				nextField = currentlevel.getField(x, y + 1);
				currentField.setValue(DIRECTION, 4);
			}
			
			// links
			else if (!walkable.contains(currentlevel.getField(x, y + 1).getToken())) {
				if (walkable.contains(currentlevel.getField(x - 1, y).getToken())) {
					nextField = currentlevel.getField(x - 1, y);
				}
				currentField.setValue(DIRECTION, 3);
			}
			
			currentField.setValue(MOVED, 1);
		}
		
		// nach oben
		if (currentField.getValue(DIRECTION) == 2 && currentField.getValue(MOVED) == 0) {
			
			// links
			if (!walkable.contains(currentlevel.getField(x - 1, y + 1).getToken())
					&& walkable.contains(currentlevel.getField(x - 1, y).getToken())) {
				nextField = currentlevel.getField(x - 1, y);
				currentField.setValue(DIRECTION, 3);
			}
			
			// oben
			else if (walkable.contains(currentlevel.getField(x, y - 1).getToken())
					&& !walkable.contains(currentlevel.getField(x - 1, y).getToken())) {
				nextField = currentlevel.getField(x, y - 1);
			}
			
			// rechts
			else if (!walkable.contains(currentlevel.getField(x, y - 1).getToken())
					&& walkable.contains(currentlevel.getField(x + 1, y).getToken())) {
				nextField = currentlevel.getField(x + 1, y);
				currentField.setValue(DIRECTION, 1);
			}
			
			// unten
			else if (!walkable.contains(currentlevel.getField(x + 1, y).getToken())) {
				if (walkable.contains(currentlevel.getField(x, y + 1).getToken())) {
					nextField = currentlevel.getField(x, y + 1);
				}
				currentField.setValue(DIRECTION, 4);
			}
			
			currentField.setValue(MOVED, 1);
		}
		
		// nach links
		if (currentField.getValue(DIRECTION) == 3 && currentField.getValue(MOVED) == 0) {
			
			// unten
			if (!walkable.contains(currentlevel.getField(x + 1, y + 1).getToken())
					&& walkable.contains(currentlevel.getField(x, y + 1).getToken())) {
				nextField = currentlevel.getField(x, y + 1);
				currentField.setValue(DIRECTION, 4);
			}
			
			// links
			else if (walkable.contains(currentlevel.getField(x - 1, y).getToken())
					&& !walkable.contains(currentlevel.getField(x, y + 1).getToken())) {
				nextField = currentlevel.getField(x - 1, y);
			}
			
			// oben
			else if (!walkable.contains(currentlevel.getField(x - 1, y).getToken())
					&& walkable.contains(currentlevel.getField(x, y - 1).getToken())) {
				nextField = currentlevel.getField(x, y - 1);
				currentField.setValue(DIRECTION, 2);
			}
			
			// rechts
			else if (!walkable.contains(currentlevel.getField(x, y - 1).getToken())) {
				if (walkable.contains(currentlevel.getField(x + 1, y).getToken())) {
					nextField = currentlevel.getField(x + 1, y);
				}
				currentField.setValue(DIRECTION, 1);
			}
			
			currentField.setValue(MOVED, 1);
		}
		
		// nach unten
		if (currentField.getValue(DIRECTION) == 4 && currentField.getValue(MOVED) == 0) {
			
			// rechts
			if (!walkable.contains(currentlevel.getField(x + 1, y - 1).getToken())
					&& walkable.contains(currentlevel.getField(x + 1, y).getToken())) {
				nextField = currentlevel.getField(x + 1, y);
				currentField.setValue(DIRECTION, 1);
			}
			
			// unten
			else if (walkable.contains(currentlevel.getField(x, y + 1).getToken())
					&& !walkable.contains(currentlevel.getField(x + 1, y).getToken())) {
				nextField = currentlevel.getField(x, y + 1);
			}
			
			// links
			else if (!walkable.contains(currentlevel.getField(x, y + 1).getToken())
					&& walkable.contains(currentlevel.getField(x - 1, y).getToken())) {
				nextField = currentlevel.getField(x - 1, y);
				currentField.setValue(DIRECTION, 3);
			}
			
			// oben
			else if (!walkable.contains(currentlevel.getField(x - 1, y).getToken())) {
				if (walkable.contains(currentlevel.getField(x, y - 1).getToken())) {
					nextField = currentlevel.getField(x, y - 1);
				}
				currentField.setValue(DIRECTION, 2);
			}
			
			currentField.setValue(MOVED, 1);
		}
		
		if (currentField.getValue(DIRECTION) == 0) currentField.setValue(DIRECTION, 1);
		
		if (nextField != null) {
			moveToNextField(currentField, nextField, false);
		}
	}
	
	/**
	 * Bewegen eines Gegners um ein Spielfeld
	 */
	private static void moveToNextField(Spielfeld currentField, Spielfeld nextField, Boolean rich) {
		Token nextToken = nextField.getToken();
		nextField.setToken(currentField.getToken());
		nextField.setValues(currentField.getValueMap());
		currentField.setToken(PATH);
		currentField.clearValues();
		if (nextToken == ME && rich) nextField.setValue(BAMRICH, 1);
		else if (nextToken == ME && !rich) nextField.setValue(BAM, 1);
	}
	
	/**
	 * Schleimausbreitung und -umwandlung
	 */
	public static void slime(Model model) {
		Level currentlevel = model.getCurrentlevel();
		Random r = new Random();
		int slimecount = 0;
		
		for (int x = 0; x < currentlevel.getWidth(); x++) {
			for (int y = 0; y < currentlevel.getHeight(); y++) {
				if (currentlevel.getField(x, y).getToken() == SLIME) {
					slimecount++;
					if (r.nextDouble() <= 0.03 && boundaryControl(x + 1, y, currentlevel))
						slime_intern(currentlevel.getField(x + 1, y));
					if (r.nextDouble() <= 0.03 && boundaryControl(x - 1, y, currentlevel))
						slime_intern(currentlevel.getField(x - 1, y));
					if (r.nextDouble() <= 0.03 && boundaryControl(x, y + 1, currentlevel))
						slime_intern(currentlevel.getField(x, y + 1));
					if (r.nextDouble() <= 0.03 && boundaryControl(x, y - 1, currentlevel))
						slime_intern(currentlevel.getField(x, y - 1));
					List<Spielfeld> neighbours = get_area(model.getCurrentlevel(), x, y);
					if (neighbours != null) {
						for (Spielfeld f : neighbours) {
							f.setToken(GEM);
						}
					}
				}
			}
		}
		
		if (slimecount > currentlevel.getMaxslime()) {
			for (int x = 0; x < currentlevel.getWidth(); x++) {
				for (int y = 0; y < currentlevel.getHeight(); y++) {
					if (currentlevel.getField(x, y).getToken() == SLIME) {
						currentlevel.getField(x, y).setToken(STONE);
					}
				}
			}
		}
	}
	
	/**
	 * Sucht zusammenhängende Schleimfelder zum Umwandeln, ausgehend von übergebenen Koordinaten
	 * 
	 * @return null wenn nicht umgeben von unveränderlichen Feldern, sonst Liste der Schleimfelder
	 */
	private static List<Spielfeld> get_area(Level currentlevel, int x, int y) {
		List<Spielfeld> neighbours = new LinkedList<Spielfeld>();
		boolean finished = false;
		Spielfeld currentfield = currentlevel.getField(x, y);
		if (currentfield.getVisited()) return null;
		currentfield.setVisited(true);
		neighbours.add(currentfield);
		
		for (int dx = -1; dx <= 1; dx++) {
			for (int dy = -1; dy <= 1; dy++) {
				if (boundaryControl(x + dx, y + dy, currentlevel) && dx != dy && dx != -dy) {
					currentfield = currentlevel.getField(x + dx, y + dy);
					if (currentfield.getToken() == PATH || currentfield.getToken() == MUD) finished = true;
					if (currentfield.getToken() == SLIME && !currentfield.getVisited()) {
						List<Spielfeld> newNeighbours = get_area(currentlevel, x + dx, y + dy);
						if (newNeighbours != null) neighbours.addAll(newNeighbours);
						else finished = true;
					}
				}
			}
		}
		
		if (!finished) return neighbours;
		else return null;
	}
	
	/**
	 * Interner Ablauf der Schleimausbreitung
	 */
	private static void slime_intern(Spielfeld currentField) {
		Token currentToken = currentField.getToken();
		if (currentToken == PATH || currentToken == MUD) currentField.setToken(SLIME);
		if (currentToken == SWAPLING || currentToken == XLING) currentField.setValue(BAMRICH, 1);
		if (currentToken == BLOCKLING) currentField.setValue(BAM, 1);
	}
	
	/**
	 * Kamehame-Erzeugung und Bewegung
	 */
	public static void kamehameha(Model model) {
		Level currentlevel = model.getCurrentlevel();
		for (int x = 0; x < currentlevel.getWidth(); x++) {
			for (int y = 0; y < currentlevel.getHeight(); y++) {
				Spielfeld nextfield = null;
				
				Spielfeld currentField = currentlevel.getField(x, y);
				if (currentField.getToken() == KAME && currentField.getValue(MOVED) == 0) {
					if (currentField.getValue(Value.A) == 1 && boundaryControl(x, y - 1, currentlevel))
						nextfield = currentlevel.getField(x, y - 1);
					else if (currentField.getValue(Value.B) == 1 && boundaryControl(x, y + 1, currentlevel))
						nextfield = currentlevel.getField(x, y + 1);
					else if (currentField.getValue(Value.C) == 1 && boundaryControl(x - 1, y, currentlevel))
						nextfield = currentlevel.getField(x - 1, y);
					else if (currentField.getValue(Value.D) == 1 && boundaryControl(x + 1, y, currentlevel))
						nextfield = currentlevel.getField(x + 1, y);
					if (nextfield != null) {
						Token nextToken = nextfield.getToken();
						if (nextToken == SWAPLING || nextToken == BLOCKLING || nextToken == XLING
								|| nextToken == GHOSTLING)
							model.incrementEnemiesKilled();
						if (nextToken != WALL) {
							nextfield.setToken(KAME);
							nextfield.setValues(currentField.getValueMap());
							nextfield.setValue(MOVED, 1);
							currentField.setToken(PATH);
							currentField.clearValues();
						} else currentField.setValue(BAM, 1);
					}
				}
			}
		}
	}
	
	/**
	 * Explosionen
	 */
	public static void explosion(Model model) {
		Level currentlevel = model.getCurrentlevel();
		for (int x = 0; x < currentlevel.getWidth(); x++) {
			for (int y = 0; y < currentlevel.getHeight(); y++) {
				Spielfeld currentField = currentlevel.getField(x, y);
				if (currentField.getToken() == Token.EXPLOSION && currentField.getValue(MOVED) == 0)
					currentField.setToken(PATH);
				if (currentField.getValue(BAMRICH) > 0) explosion_intern(model, x, y, true);
				if (currentField.getValue(BAM) > 0) explosion_intern(model, x, y, false);
			}
		}
	}
	
	/**
	 * Interner Ablauf der eigentlichen Explosion
	 */
	private static void explosion_intern(Model model, int x, int y, boolean rich) {
		model.incrementExplosions();
		Level currentlevel = model.getCurrentlevel();
		for (int dx = -1; dx <= 1; dx++) {
			for (int dy = -1; dy <= 1; dy++) {
				if (boundaryControl(x + dx, y + dy, currentlevel)) {
					Spielfeld currentField = currentlevel.getField(x + dx, y + dy);
					Token currentToken = currentField.getToken();
					if (currentToken != WALL && currentToken != EXIT) {
						if (currentToken == SWAPLING || currentToken == XLING || currentToken == BLOCKLING
								|| currentToken == GHOSTLING) {
							model.incrementEnemiesKilled();
						}
						if (rich) currentField.setToken(GEM);
						else currentField.setToken(Token.EXPLOSION);
						currentField.setValue(MOVED, 1);
						
						model.appendSound(SoundEvent.EXPLOSION);
					}
				}
			}
		}
	}
	
	/**
	 * Bewegung im Craftmode
	 */
	public static void craftmove(Model model) {
		Level currentlevel = model.getCurrentlevel();
		
		KeyCode key = model.getPressedKey();
		int nextX = 0;
		int nextY = 0;
		if (key == UP) {
			nextY--;
			model.setPlayerDirection("up");
		} else if (key == DOWN) {
			nextY++;
			model.setPlayerDirection("down");
		} else if (key == LEFT) {
			nextX--;
			model.setPlayerDirection("left");
		} else if (key == RIGHT) {
			nextX++;
			model.setPlayerDirection("right");
		}
		
		Spielfeld meField = null;
		Spielfeld nextField = null;
		
		for (int x = 0; x < currentlevel.getWidth(); x++) {
			for (int y = 0; y < currentlevel.getHeight(); y++) {
				Spielfeld currentField = currentlevel.getField(x, y);
				if (currentField.getToken() == ME) {
					meField = currentField;
					nextX += x;
					nextY += y;
					if (boundaryControl(nextX, nextY, currentlevel)) {
						nextField = currentlevel.getField(nextX, nextY);
					}
				}
			}
		}
		
		// normale Bewegung
		if (!model.getPressedShift() && nextField != null) {
			if (nextField.getToken() == TURBOTIME) model.activateTurboMode();
			meField.setToken(PATH);
			nextField.setToken(ME);
		}
		
		// Craften
		else if (model.getPressedShift() && nextField != null) {
			Token nextToken = nextField.getToken();
			Token folgeToken = nextToken.next();
			nextField.setToken(folgeToken);
			List<Token> enemies = Arrays.asList(XLING, BLOCKLING, SWAPLING, GHOSTLING);
			if (enemies.contains(folgeToken)) nextField.setValue(DIRECTION, 1);
		}
	}
	
}