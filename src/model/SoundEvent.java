package model;

/**
 * Werte für jedes Event im Spiel, das einen Sound auslösen kann
 */
public enum SoundEvent {
	
	/**
	 * Stein fällt
	 */
	STONEFALLING,
	/**
	 * Spielfigur macht Schritt
	 */
	STEP,
	/**
	 * Diamant wurde eingesammelt
	 */
	DIAMOND,
	/**
	 * Spielfigur oder Gegner explodiert
	 */
	EXPLOSION,
	/**
	 * Spielfigur wird von Wänden eingeschlossen
	 */
	TRAPPED,
	/**
	 * Kamehame-Erzeugung
	 */
	KAMEHAME,
	/**
	 * Spieler gewinnt
	 */
	APPLAUSE,
	/**
	 * Spieler verliert
	 */
	GAMEOVER;
	
}