package model;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static model.SoundEvent.*;

/**
 * Objektklasse für alle Daten einer Levelregel
 */
public class LevelRule {
	
	/**
	 * An die Regel übergebenes Model
	 */
	private Model				model;
	/**
	 * Situation, in der die Regel angewandt wird
	 */
	private String				situation;
	/**
	 * Richtung, in der die Regel angewandt wird
	 */
	private String				direction;
	/**
	 * Liste an Spielfeldern, auf die die Regel angewandt wird
	 */
	private List<Regelblock>	original;
	/**
	 * Spielfelder, die durch Anwendung der Regel entstehen
	 */
	private List<Regelblock>	result;
	/**
	 * Anzahl der Edelsteine, die gesammelt sein müssen, um die Regel anzuwenden
	 */
	private Integer				gems_orig;
	/**
	 * Anzahl der Ticks, die vergangen sein müssen, um die Regel anzuwenden
	 */
	private Integer				ticks_orig;
	/**
	 * Wert, den die globale Bedingung X erreicht haben muss, um die Regel anzuwenden
	 */
	private Integer				x_orig;
	/**
	 * Wert, den die globale Bedingung Y erreicht haben muss, um die Regel anzuwenden
	 */
	private Integer				y_orig;
	/**
	 * Wert, den die globale Bedingung Z erreicht haben muss, um die Regel anzuwenden
	 */
	private Integer				z_orig;
	/**
	 * Wert auf den der Edelsteincounter nach Anwenden der Regel gesetzt wird
	 */
	private Integer				gems_res;
	/**
	 * Wert auf den der Tickcounter nach Anwenden der Regel gesetzt wird
	 */
	private Integer				ticks_res;
	/**
	 * Wert auf den die globale Bedingung X nach Anwenden der Regel gesetzt wird
	 */
	private Integer				x_res;
	/**
	 * Wert auf den die globale Bedingung Y nach Anwenden der Regel gesetzt wird
	 */
	private Integer				y_res;
	/**
	 * Wert auf den die globale Bedingung Z nach Anwenden der Regel gesetzt wird
	 */
	private Integer				z_res;
	/**
	 * Wert, wie oft eine rare-Regel ausgeführt wird
	 */
	private int					sparsity;
	/**
	 * Sound, der bei Anwenden der Regel gespielt wird
	 */
	private SoundEvent			sound;
	
	/**
	 * @param model
	 * @param JSONRule
	 *            ein von org.json erstelltes JSONObject mit den Daten der zu erstellenden Regel
	 */
	public LevelRule(Model model, JSONObject JSONRule) {
		this.model = model;
		this.situation = JSONRule.getString("situation");
		this.direction = JSONRule.getString("direction");
		
		// Globale Variablen werden initialisiert, falls sie für die Regel irrelevant sind
		// Sind sie für die Regel doch relevant, werden sie unten überschrieben
		this.gems_orig = null;
		this.ticks_orig = null;
		this.x_orig = null;
		this.y_orig = null;
		this.z_orig = null;
		this.gems_res = null;
		this.ticks_res = null;
		this.x_res = null;
		this.y_res = null;
		this.z_res = null;
		
		this.original = new ArrayList<Regelblock>();
		JSONArray JSONOriginal = JSONRule.getJSONArray("original");
		for (int i = 0; i < JSONOriginal.length(); i++) {
			JSONObject currentValues;
			if (JSONOriginal.getJSONObject(i).has("values")) {
				currentValues = JSONOriginal.getJSONObject(i).getJSONObject("values");
			} else {
				currentValues = new JSONObject();
			}
			
			if (currentValues.has("GEMS")) {
				this.gems_orig = currentValues.getInt("GEMS");
			} else if (currentValues.has("gems")) {
				this.gems_orig = currentValues.getInt("gems");
			}
			if (currentValues.has("TICKS")) {
				this.ticks_orig = currentValues.getInt("TICKS");
			} else if (currentValues.has("ticks")) {
				this.ticks_orig = currentValues.getInt("ticks");
			}
			if (currentValues.has("X")) this.x_orig = currentValues.getInt("X");
			if (currentValues.has("Y")) this.y_orig = currentValues.getInt("Y");
			if (currentValues.has("Z")) this.z_orig = currentValues.getInt("Z");
			
			this.original.add(new Regelblock(this.model, JSONOriginal.getJSONObject(i)));
		}
		
		this.result = new ArrayList<Regelblock>();
		JSONArray JSONResult = JSONRule.getJSONArray("result");
		for (int i = 0; i < JSONResult.length(); i++) {
			JSONObject currentValues;
			if (JSONResult.getJSONObject(i).has("values")) {
				currentValues = JSONResult.getJSONObject(i).getJSONObject("values");
			} else {
				currentValues = new JSONObject();
			}
			if (currentValues.has("GEMS")) {
				this.gems_res = currentValues.getInt("GEMS");
			} else if (currentValues.has("gems")) {
				this.gems_res = currentValues.getInt("gems");
			}
			if (currentValues.has("TICKS")) {
				this.ticks_res = currentValues.getInt("TICKS");
			} else if (currentValues.has("ticks")) {
				this.ticks_res = currentValues.getInt("ticks");
			}
			if (currentValues.has("X")) this.x_res = currentValues.getInt("X");
			if (currentValues.has("Y")) this.y_res = currentValues.getInt("Y");
			if (currentValues.has("Z")) this.z_res = currentValues.getInt("Z");
			this.result.add(new Regelblock(this.model, JSONResult.getJSONObject(i)));
		}
		
		if (JSONRule.has("sparsity")) this.sparsity = JSONRule.getInt("sparsity");
		else this.sparsity = 1;
		
		if (JSONRule.has("sound")) {
			switch (JSONRule.getString("sound")) {
				case "trapped":
					this.sound = TRAPPED;
					break;
			}
		} else this.sound = null;
	}
	
	// Getter
	/**
	 * @return Situation, in der die Regel angewandt wird
	 */
	public String getSituation() {
		return situation;
	}
	
	/**
	 * @return Richtung, in der die Regel angewandt wird
	 */
	public String getDirection() {
		return direction;
	}
	
	/**
	 * @return Liste an Spielfeldern, auf die die Regel angewandt wird
	 */
	public List<Regelblock> getOriginal() {
		return original;
	}
	
	/**
	 * @return Spielfelder, die durch Anwendung der Regel entstehen
	 */
	public List<Regelblock> getResult() {
		return result;
	}
	
	/**
	 * @return Anzahl der Edelsteine, die gesammelt sein müssen, um die Regel anzuwenden
	 */
	public Integer getGemsOrig() {
		return gems_orig;
	}
	
	/**
	 * @return Anzahl der Ticks, die vergangen sein müssen, um die Regel anzuwenden
	 */
	public Integer getTicksOrig() {
		return ticks_orig;
	}
	
	/**
	 * @return Wert, den die globale Bedingung X erreicht haben muss, um die Regel anzuwenden
	 */
	public Integer getXOrig() {
		return x_orig;
	}
	
	/**
	 * @return Wert, den die globale Bedingung Y erreicht haben muss, um die Regel anzuwenden
	 */
	public Integer getYOrig() {
		return y_orig;
	}
	
	/**
	 * @return Wert, den die globale Bedingung Z erreicht haben muss, um die Regel anzuwenden
	 */
	public Integer getZOrig() {
		return z_orig;
	}
	
	/**
	 * @return Wert auf den der Edelsteincounter nach Anwenden der Regel gesetzt wird
	 */
	public Integer getGemsRes() {
		return gems_res;
	}
	
	/**
	 * @return Wert auf den der Tickcounter nach Anwenden der Regel gesetzt wird
	 */
	public Integer getTicksRes() {
		return ticks_res;
	}
	
	/**
	 * @return Wert auf den die globale Bedingung X nach Anwenden der Regel gesetzt wird
	 */
	public Integer getXRes() {
		return x_res;
	}
	
	/**
	 * @return Wert auf den die globale Bedingung Y nach Anwenden der Regel gesetzt wird
	 */
	public Integer getYRes() {
		return y_res;
	}
	
	/**
	 * @return Wert auf den die globale Bedingung Z nach Anwenden der Regel gesetzt wird
	 */
	public Integer getZRes() {
		return z_res;
	}
	
	/**
	 * @return Wert, wie oft eine rare-Regel ausgeführt wird
	 */
	public int getSparsity() {
		return sparsity;
	}
	
	/**
	 * @return Sound, der bei Anwenden der Regel gespielt wird
	 */
	public SoundEvent getSound() {
		return sound;
	}
	
}
