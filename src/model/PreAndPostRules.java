package model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import static javafx.scene.input.KeyCode.UP;
import static javafx.scene.input.KeyCode.DOWN;
import static javafx.scene.input.KeyCode.LEFT;
import static javafx.scene.input.KeyCode.RIGHT;
import static model.SoundEvent.*;
import static model.Token.*;
import static model.Value.*;

/**
 * Anwenden der Pre oder Post Levelregeln
 */
public final class PreAndPostRules {
	
	private PreAndPostRules() {
	}
	
	/**
	 * checkSituation überprüft ob Situation der Regel zutrifft und leitet dann an checkDirection weiter
	 * 
	 * @param model
	 * @param rule
	 */
	public static void checkSituation(Model model, LevelRule rule) {
		
		if (rule.getSituation().equals("any")) {
			checkDirection(model, rule);
		}
		if (rule.getSituation().equals("rare") && model.getTicks() % rule.getSparsity() == 0) {
			checkDirection(model, rule);
		}
		
		// abgleichen ob Regel mit Key übereinstimmt
		if (model.getPressedKey() == UP) {
			if (rule.getSituation().equals("up") && !model.getPressedShift()) {
				checkDirection(model, rule);
			} else if (rule.getSituation().equals("metaup") && model.getPressedShift()) {
				checkDirection(model, rule);
			}
		}
		if (model.getPressedKey() == DOWN) {
			if (rule.getSituation().equals("down") && !model.getPressedShift()) {
				checkDirection(model, rule);
			} else if (rule.getSituation().equals("metadown") && model.getPressedShift()) {
				checkDirection(model, rule);
			}
		}
		if (model.getPressedKey() == LEFT) {
			if (rule.getSituation().equals("left") && !model.getPressedShift()) {
				checkDirection(model, rule);
			} else if (rule.getSituation().equals("metaleft") && model.getPressedShift()) {
				checkDirection(model, rule);
			}
		}
		if (model.getPressedKey() == RIGHT) {
			if (rule.getSituation().equals("right") && !model.getPressedShift()) {
				checkDirection(model, rule);
			} else if (rule.getSituation().equals("metaright") && model.getPressedShift()) {
				checkDirection(model, rule);
			}
		}
	}
	
	/**
	 * checkDirection überprüft die direction der Regel und leitet an entsprechende Methode weiter
	 * 
	 * @param model
	 * @param rule
	 */
	private static void checkDirection(Model model, LevelRule rule) {
		String direction = rule.getDirection();
		
		if (direction.equals("east")) {
			east(model, rule);
		} else if (direction.equals("west")) {
			west(model, rule);
		} else if (direction.equals("north")) {
			north(model, rule);
		} else if (direction.equals("south")) {
			south(model, rule);
		}
	}
	
	/**
	 * east, west, noth und south schreiben eine Zeile oder Spalte des currentlevel in eine Liste und übergeben diese
	 * weiter an applyRules
	 * 
	 * @param model
	 * @param rule
	 */
	private static void east(Model model, LevelRule rule) {
		Level currentlevel = model.getCurrentlevel();
		ArrayList<Spielfeld> row;
		
		for (int y = 0; y < currentlevel.getHeight(); y++) {
			row = new ArrayList<>();
			for (int x = 0; x < currentlevel.getWidth(); x++) {
				row.add(currentlevel.getField(x, y));
			}
			applyRules(model, rule, row);
		}
	}
	
	private static void west(Model model, LevelRule rule) {
		Level currentlevel = model.getCurrentlevel();
		ArrayList<Spielfeld> row;
		
		for (int y = 0; y < currentlevel.getHeight(); y++) {
			row = new ArrayList<>();
			for (int x = currentlevel.getWidth() - 1; x >= 0; x--) {
				row.add(currentlevel.getField(x, y));
			}
			applyRules(model, rule, row);
		}
	}
	
	private static void north(Model model, LevelRule rule) {
		Level currentlevel = model.getCurrentlevel();
		ArrayList<Spielfeld> column;
		
		for (int x = 0; x < currentlevel.getWidth(); x++) {
			column = new ArrayList<>();
			for (int y = currentlevel.getHeight() - 1; y >= 0; y--) {
				column.add(currentlevel.getField(x, y));
			}
			applyRules(model, rule, column);
		}
	}
	
	private static void south(Model model, LevelRule rule) {
		Level currentlevel = model.getCurrentlevel();
		ArrayList<Spielfeld> column;
		
		for (int x = 0; x < currentlevel.getWidth(); x++) {
			column = new ArrayList<>();
			for (int y = 0; y < currentlevel.getHeight(); y++) {
				column.add(currentlevel.getField(x, y));
			}
			applyRules(model, rule, column);
		}
	}
	
	/**
	 * applyRules matched Spielfelder und Originals und bei einem Match wird das Spielfeld in Result abgeändert
	 * 
	 * @param model
	 * @param rule
	 * @param fieldList
	 *            ist entweder Zeile oder Spalte des currentlevel die mit der Regel verglichen werden soll
	 */
	private static void applyRules(Model model, LevelRule rule, ArrayList<Spielfeld> fieldList) {
		List<Regelblock> original = rule.getOriginal();
		List<Regelblock> result = rule.getResult();
		
		// Liste mit Spielfeldern (Reihe oder Spalte) durchgehen bis Anzahl verbleibender Spielfelder
		// kleiner als die Länge des Originals ist
		for (int i = 0; i <= fieldList.size() - original.size(); i++) {
			
			// Liste an Spielfeldern die später abgeändert werden sollen
			ArrayList<Spielfeld> aenderliste;
			aenderliste = new ArrayList<>();
			
			// Liste an Token die im Original vorhanden sind
			ArrayList<Token> temptokens;
			temptokens = new ArrayList<>();
			
			// Original durchgehen
			for (int o = 0; o < original.size(); o++) {
				
				// Integer der Position von Spielfeld aus fieldList(j) darstellt,
				// welches dem original an Stelle o entsprechen soll
				int f = i + o;
				
				// original beinhaltet token von aktueller Stelle der fieldListe NICHT!
				if (!original.get(o).getTokenlist().contains(fieldList.get(f).getToken())
						&& (original.get(o).getTokenlist().get(0) != ANY)) {
					aenderliste.clear();
					temptokens.clear();
					break;
				}
				
				// original beinhaltet token von aktueller Stelle der fieldListe
				else {
					boolean match = true;
					
					// durchgehen aller Values aus dem Original an der Stelle o
					for (Value origVal : original.get(o).getValueMap().keySet()) {
						
						// Int vom Original Value
						int origValueInt = original.get(o).getValue(origVal);
						
						// Int vom Feld des selben Value
						int fieldValueInt = fieldList.get(f).getValue(origVal);
						
						// Bei übereinstimmung bleibt match auf true
						if (!compareFieldAndOriginal(origValueInt, fieldValueInt, origVal, model, rule)) {
							match = false;
						}
					}
					
					// Da Values GEMS, X-Z, TICKS nicht in den Values der Felder gespeichert werden,
					// müssen diese hier noch mal gesondert überprüft werden
					List<Value> specialVals = Arrays.asList(GEMS, X, Y, Z, TICKS);
					for (Value fieldVal : specialVals) {
						if (!compareFieldAndOriginal(0, 0, fieldVal, model, rule)) {
							match = false;
						}
					}
					
					// Bedingung falls kein Match vorliegt
					if (!match) {
						temptokens.clear();
						aenderliste.clear();
						break;
					}
					
					// Bei match werden temptokens und spiefelder geadded
					else {
						temptokens.add(fieldList.get(f).getToken());
						aenderliste.add(fieldList.get(f));
					}
				}
			}
			
			// wenn aenderliste gleichlang wie originalliste ist
			// werden die Spielfelder der aenderliste mit den Feldern der resultliste überschrieben
			if (aenderliste.size() != 0 && original.size() == aenderliste.size()) {
				
				for (int r = 0; r < original.size(); r++) {
					
					// Int Wert der -1 ist wenn keine Position vorhanden ist, ansonsten die Position (0 bis n) enthält
					int position = result.get(r).getNumber();
					
					// für den Fall das in result als Token eine Position festgelegt ist
					if (position != -1) {
						aenderliste.get(r).setToken(temptokens.get(position));
						aenderliste.get(r).addValues(result.get(r).getValueMap());
						model.setXYZ(rule.getXRes(), rule.getYRes(), rule.getZRes(), rule.getGemsRes(),
								rule.getTicksRes());
					}
					
					// für den Fall das in result ein Token festgelegt ist
					else {
						aenderliste.get(r).setToken(result.get(r).getTokenlist().get(0));
						aenderliste.get(r).addValues(result.get(r).getValueMap());
						model.setXYZ(rule.getXRes(), rule.getYRes(), rule.getZRes(), rule.getGemsRes(),
								rule.getTicksRes());
					}
					
				}
				i = i + result.size() - 1;
				
				// Soundbedingungen
				if (rule.getSound() != null && model.getSoundList().contains(DIAMOND) && rule.getSound() == TRAPPED) {
					model.appendSound(rule.getSound());
				}
			}
		}
	}
	
	/**
	 * compareFieldAndOriginal vergleicht Spielfeld Value und Original Value
	 * 
	 * @param origValueInt
	 *            ist int Wert des Original Values
	 * @param fieldValueInt
	 *            ist int Wert des Feld Values
	 * @param val
	 *            ist der Value dessen Werte verglichen werden
	 * @param model
	 * @param rule
	 * @return ret ist boolean true bei match der Values, sonst false
	 */
	private static boolean compareFieldAndOriginal(int origValueInt, int fieldValueInt, Value val, Model model,
			LevelRule rule) {
		boolean ret = false;
		
		Integer origValue = origValueInt;
		int fieldValue = fieldValueInt;
		
		if (val == X && rule.getXOrig() != null) {
			fieldValue = model.getX();
			origValue = rule.getXOrig();
		} else if (val == Y && rule.getYOrig() != null) {
			fieldValue = model.getY();
			origValue = rule.getYOrig();
		} else if (val == Z && rule.getZOrig() != null) {
			fieldValue = model.getZ();
			origValue = rule.getZOrig();
		} else if (val == GEMS && rule.getGemsOrig() != null) {
			fieldValue = model.getGems();
			origValue = rule.getGemsOrig();
		} else if (val == TICKS && rule.getTicksOrig() != null) {
			fieldValue = model.getTicks();
			origValue = rule.getTicksOrig();
		} else if (val == X || val == Y || val == Z || val == GEMS || val == TICKS) {
			origValue = -1;
		}
		
		// "==0" || ">0" sind Bedingungen der Angabe, "<0" Prüft, ob ein Value -1 und somit nicht vergeben ist
		if (((fieldValue == origValue) && origValue == 0) || ((fieldValue >= origValue) && origValue > 0)
				|| origValue == -1) {
			ret = true;
		}
		
		return ret;
	}
	
}
