import javafx.application.Application;
import javafx.stage.Stage;
import controller.Controller;
import model.Model;
import view.View;

public class Main extends Application {
	
	Model	model;
	View	view;
	
	@Override
	public void start(Stage stage) {
		this.model = new Model();
		this.view = new View(model, stage);
		new Controller(model, view);
	}
	
	@Override
	public void stop() {
		this.model.saveScore("data/score.json");
	}
	
	public static void main(String[] args) {
		launch(args);
	}
	
}