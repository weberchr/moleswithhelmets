package view;

import javafx.geometry.Insets;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import model.Level;
import model.Model;
import model.SoundEvent;
import model.Spielfeld;

import java.util.List;
import java.util.Observable;
import java.util.Observer;

import static model.SoundEvent.*;
import static model.Token.ME;
import static model.Value.DIRECTION;
import static model.Value.SLIPPERY;

/**
 * Ausführen des Games
 */
/**
 * created by Leo and Vadim
 */
public class Game extends Pane implements Observer {
	
	private Model			model;
	private View			view;
	private SoundLoader		soundLoader;
	private ImgLoader		imgLoader;
	private Pane			pane;
	private Canvas			canvas;
	private GridPane		levelGrid;
	public Button			backButton, musikButton, saveAndExitButton;
	private DigitalClock	digital;
	private VBox			vBox;
	private Label			labelPunkt, labelEdelstein;
	private ImageView		backgroundImgView, musicView, backView, saveAndExitView;
	private ImagePattern	meLeftPattern, meRightPattern, meUpPattern, meDownPattern, mudPattern, pathPattern,
			gemPattern, wallPattern, bricksPattern, stonePattern, blocklingLeftPattern, blocklingRightPattern,
			swaplingLeftPattern, swaplingRightPattern, xlingRightPattern, xlingLeftPattern, slimePattern, exitPattern,
			firePattern, sandPattern, blocklingUpPattern, blocklingDownPattern, swaplingUpPattern, swaplingDownPattern,
			xlingUpPattern, xlingDownPattern, ghostlingLeftPattern, ghostlingRightPattern, ghostlingUpPattern,
			ghostlingDownPattern, explosionPattern, sievePattern, potPattern, hourGlassPattern, turboTimePattern,
			gemSlipperyPattern, stoneSlipperyPattern, northThingPattern, eastThingPattern, westThingPattern,
			southThingPattern, exitClosedPattern, kamePattern, turboMeLeftPattern, turboMeRightPattern,
			turboMeUpPattern, turboMeDownPattern;
	private int				xMin;
	private int				yMin;
	private int				xMax;
	private int				yMax;
	
	public Game(Model model, View view) {
		this.model = model;
		this.view = view;
		this.soundLoader = this.view.getSoundLoader();
		this.imgLoader = this.view.getImgLoader();
		this.pane = new Pane();
		this.canvas = new Canvas();
		this.vBox = new VBox(20);
		this.digital = new DigitalClock(this.model);
		
		this.backgroundImgView = new ImageView();
		this.backgroundImgView.setSmooth(true);
		this.backgroundImgView.setPreserveRatio(true);
		this.backgroundImgView.setImage(imgLoader.getGameViewBackground());
		
		// Laden der in Game benötigten Pattern
		meLeftPattern = new ImagePattern(imgLoader.getMeLeftWalkingImage());
		meRightPattern = new ImagePattern(imgLoader.getMeRightWalkingImage());
		meUpPattern = new ImagePattern(imgLoader.getMeUpWalkingImage());
		meDownPattern = new ImagePattern(imgLoader.getMeDownWalkingImage());
		turboMeLeftPattern = new ImagePattern(imgLoader.getTurboMeLeft());
		turboMeRightPattern = new ImagePattern(imgLoader.getTurboMeRight());
		turboMeUpPattern = new ImagePattern(imgLoader.getTurboMeUp());
		turboMeDownPattern = new ImagePattern(imgLoader.getTurboMeDown());
		mudPattern = new ImagePattern(imgLoader.getMudImage());
		pathPattern = new ImagePattern(imgLoader.getPathImage());
		gemPattern = new ImagePattern(imgLoader.getGemImage());
		gemSlipperyPattern = new ImagePattern(imgLoader.getGemSlipperyImage());
		wallPattern = new ImagePattern(imgLoader.getWallImage());
		bricksPattern = new ImagePattern(imgLoader.getBricksImage());
		stonePattern = new ImagePattern(imgLoader.getStoneImage());
		stoneSlipperyPattern = new ImagePattern(imgLoader.getStoneSlipperyImage());
		blocklingLeftPattern = new ImagePattern(imgLoader.getFoxWalkingImage());
		blocklingRightPattern = new ImagePattern(imgLoader.getFoxWalkingRightImage());
		blocklingUpPattern = new ImagePattern(imgLoader.getFoxWalkingUpImage());
		blocklingDownPattern = new ImagePattern(imgLoader.getFoxWalkingDownImage());
		swaplingLeftPattern = new ImagePattern(imgLoader.getRacoonWalkingImage());
		swaplingRightPattern = new ImagePattern(imgLoader.getRacoonWalkingRightImage());
		swaplingUpPattern = new ImagePattern(imgLoader.getRacoonWalkingUpImage());
		swaplingDownPattern = new ImagePattern(imgLoader.getRacoonWalkingDownImage());
		xlingLeftPattern = new ImagePattern(imgLoader.getSkeletonWalkingImage());
		xlingRightPattern = new ImagePattern(imgLoader.getSkeletonWalkingRightImage());
		xlingUpPattern = new ImagePattern(imgLoader.getSkeletonWalkingUpImage());
		xlingDownPattern = new ImagePattern(imgLoader.getSkeletonWalkingDownImage());
		slimePattern = new ImagePattern(imgLoader.getSlimeImage());
		exitPattern = new ImagePattern(imgLoader.getExitImage());
		firePattern = new ImagePattern(imgLoader.getFireImage());
		sandPattern = new ImagePattern(imgLoader.getSandImage());
		ghostlingLeftPattern = new ImagePattern(imgLoader.getGhostWalkingLeftImage());
		ghostlingRightPattern = new ImagePattern(imgLoader.getGhostWalkingRightImage());
		ghostlingUpPattern = new ImagePattern(imgLoader.getGhostWalkingUpImage());
		ghostlingDownPattern = new ImagePattern(imgLoader.getGhostWalkingDownImage());
		explosionPattern = new ImagePattern(imgLoader.getExplosionImage());
		sievePattern = new ImagePattern(imgLoader.getSieveImage());
		potPattern = new ImagePattern(imgLoader.getPotImage());
		turboTimePattern = new ImagePattern(imgLoader.getTurboTimeImage());
		hourGlassPattern = new ImagePattern(imgLoader.getHourGlassImage());
		eastThingPattern = new ImagePattern(imgLoader.getEastThingImage());
		westThingPattern = new ImagePattern(imgLoader.getWestThingImage());
		northThingPattern = new ImagePattern(imgLoader.getNorthThingImage());
		southThingPattern = new ImagePattern(imgLoader.getSouthThingImage());
		exitClosedPattern = new ImagePattern(imgLoader.getExitClosedImage());
		kamePattern = new ImagePattern(imgLoader.getKameImage());
		
		/**
		 * Erstellen benötigter Buttons
		 */
		
		// Back-Button
		this.backButton = new Button();
		this.backView = new ImageView(imgLoader.getBackToLevelsButton());
		this.backView.setPreserveRatio(true);
		this.backView.setSmooth(true);
		this.backButton.setGraphic(backView);
		this.backButton.setBackground(null);
		this.backButton.setBorder(null);
		this.backButton.setPadding(Insets.EMPTY);
		this.backButton.setFocusTraversable(false);
		this.backButton.relocate(30, 547);
		
		// SaveAndExit-Button
		this.saveAndExitButton = new Button();
		this.saveAndExitView = new ImageView(imgLoader.getSaveAndExitButton());
		this.saveAndExitView.setPreserveRatio(true);
		this.saveAndExitView.setSmooth(true);
		this.saveAndExitButton.setGraphic(this.saveAndExitView);
		this.saveAndExitButton.setBackground(null);
		this.saveAndExitButton.setBorder(null);
		this.saveAndExitButton.setPadding(Insets.EMPTY);
		this.saveAndExitButton.setFocusTraversable(false);
		this.saveAndExitButton.relocate(1080, 500);
		
		// Musik-Button
		this.musikButton = new Button();
		this.musicView = new ImageView(imgLoader.getMusicOnButton());
		this.musicView.setPreserveRatio(true);
		this.musicView.setSmooth(true);
		this.musikButton.setGraphic(musicView);
		this.musikButton.setBackground(null);
		this.musikButton.setBorder(null);
		this.musikButton.setPadding(Insets.EMPTY);
		this.musikButton.relocate(1098, 110);
		this.musikButton.setFocusTraversable(false);
		
		// Label zur Anzeige in der VBox
		this.labelPunkt = new Label();
		this.labelPunkt.setFont(Font.font("Papyrus", FontPosture.ITALIC, 30));
		
		this.labelEdelstein = new Label();
		this.labelEdelstein.setFont(Font.font("Papyrus", FontPosture.ITALIC, 30));
		
		// Hinzufügen benötigter Elemente zur VBox
		this.vBox.getChildren().addAll(this.labelPunkt, this.labelEdelstein, this.digital);
		this.vBox.setLayoutX(10);
		this.vBox.setLayoutY(10);
		
		// Digitaluhr platzieren
		this.digital.setLayoutX(10);
		this.digital.setLayoutY(60);
		
		// Hinzufügen benötigter Elemente zur Pane
		this.getChildren().addAll(this.pane, this.backgroundImgView, this.vBox, this.backButton, this.musikButton,
				this.saveAndExitButton);
		
		// GridPane für das Raster
		levelGrid = new GridPane();
		levelGrid.setHgap(-1);
		levelGrid.setVgap(-1);
		
		// GridPane der Pane hinzufügen
		this.getChildren().add(levelGrid);
		
		this.model.addObserver(this);
		
	}
	
	/**
	 * zeichnet das Spielfeld
	 */
	private void drawTime() {
		
		List<SoundEvent> sounds = model.getSoundList();
		if (sounds.contains(APPLAUSE)) this.soundLoader.getApplause().play(0.5);
		else if (sounds.contains(GAMEOVER)) this.soundLoader.getGameOver().play(0.5);
		else if (sounds.contains(KAMEHAME)) this.soundLoader.getKamehame().play(0.4);
		else if (sounds.contains(TRAPPED)) this.soundLoader.getMolyOiOi().play(0.06);
		else if (sounds.contains(DIAMOND)) this.soundLoader.getMolyDiamondFound().play(0.1);
		else if (sounds.contains(EXPLOSION)) this.soundLoader.getExplosion().play(0.06);
		else if (sounds.contains(STONEFALLING)) this.soundLoader.getStonefalling().play(0.05);
		else if (sounds.contains(STEP)) this.soundLoader.getPathStep().play(0.06);
		
		levelGrid.getChildren().clear();
		
		this.labelPunkt.setText("Points: " + model.getPoints().get(model.getLevelindex()));
		this.labelEdelstein.setText("Gemstones: " + model.getGems());
		
		if (model.getRunning()) { // jfi getRunning ist zur zeit noch mit dem falschen Knopf verknüpft
			GraphicsContext gc = canvas.getGraphicsContext2D();
			
			gc.clearRect(0, 0, canvas.widthProperty().get(), canvas.heightProperty().get());
			
			// doppeltes Rectangle-Array für die verschiedenfarbigen Rects
			
			drawGrid(this.model.getCurrentlevel(), xMin, xMax, yMin, yMax, levelGrid, model.getPlayerDirection(),
					model.getGems());
			
			if (model.getFinishedNeg()) {
				view.fromGameToVerlorenPopUp();
				model.setFinishedNeg(false);
			}
			if (model.getFinishedPos() > 0) {
				view.fromGameToGewonnenPopUp();
				model.setFinishedPos(0);
			}
			
		}
		
	}
	
	public void drawGrid(Level currentlevel, int xMin, int xMax, int yMin, int yMax, GridPane grid, String playerdirect,
			int mgems) {
		Rectangle[][] rectangles;
		
		rectangles = new Rectangle[currentlevel.getWidth()][currentlevel.getHeight()];
		Spielfeld[][] mapView = currentlevel.getMap(); // Map aus level auslesen
		
		for (int i = xMin; i <= xMax; i++) {
			for (int j = yMin; j <= yMax; j++) {
				Rectangle rectangle = new Rectangle(60, 60); // Dummy-Rect
				rectangles[i][j] = rectangle; // Rect-Array an der Stelle i,j mit Rect befüllen
				
				// Abfragen welcher token an der Stelle in der Map ist und je nach token Pattern ändern
				switch (mapView[i][j].getToken()) {
					case ME:
						if (model.getTurboActive()) {
							switch (playerdirect) {
								case "left":
									rectangle.setFill(turboMeLeftPattern);
									break;
								case "right":
									rectangle.setFill(turboMeRightPattern);
									break;
								case "up":
									rectangle.setFill(turboMeUpPattern);
									break;
								case "down":
									rectangle.setFill(turboMeDownPattern);
									break;
							}
						} else {
							switch (playerdirect) {
								case "left":
									rectangle.setFill(meLeftPattern);
									break;
								case "right":
									rectangle.setFill(meRightPattern);
									break;
								case "up":
									rectangle.setFill(meUpPattern);
									break;
								case "down":
									rectangle.setFill(meDownPattern);
									break;
							}
						}
						break;
					case MUD:
						rectangle.setFill(mudPattern);
						break;
					case STONE:
						if (mapView[i][j].getValue(SLIPPERY) > 0) {
							rectangle.setFill(stoneSlipperyPattern);
						} else {
							rectangle.setFill(stonePattern);
						}
						break;
					case GEM:
						if (mapView[i][j].getValue(SLIPPERY) > 0) {
							rectangle.setFill(gemSlipperyPattern);
						} else {
							rectangle.setFill(gemPattern);
						}
						break;
					case EXIT:
						if (currentlevel.getGems().get(0) <= mgems) rectangle.setFill(exitPattern);
						else rectangle.setFill(exitClosedPattern);
						break;
					case WALL:
						rectangle.setFill(wallPattern);
						break;
					case BRICKS:
						rectangle.setFill(bricksPattern);
						break;
					case PATH:
						rectangle.setFill(pathPattern);
						break;
					case EXPLOSION:
						rectangle.setFill(explosionPattern);
						break;
					case SLIME:
						rectangle.setFill(slimePattern);
						break;
					case SWAPLING:
						switch (mapView[i][j].getValue(DIRECTION)) {
							case 1:
								rectangle.setFill(swaplingRightPattern);
								break;
							case 2:
								rectangle.setFill(swaplingUpPattern);
								break;
							case 3:
								rectangle.setFill(swaplingLeftPattern);
								break;
							case 4:
								rectangle.setFill(swaplingDownPattern);
								break;
						}
						break;
					case BLOCKLING:
						switch (mapView[i][j].getValue(DIRECTION)) {
							case 1:
								rectangle.setFill(blocklingRightPattern);
								break;
							case 2:
								rectangle.setFill(blocklingUpPattern);
								break;
							case 3:
								rectangle.setFill(blocklingLeftPattern);
								break;
							case 4:
								rectangle.setFill(blocklingDownPattern);
								break;
						}
						break;
					case XLING:
						switch (mapView[i][j].getValue(DIRECTION)) {
							case 1:
								rectangle.setFill(xlingRightPattern);
								break;
							case 2:
								rectangle.setFill(xlingUpPattern);
								break;
							case 3:
								rectangle.setFill(xlingLeftPattern);
								break;
							case 4:
								rectangle.setFill(xlingDownPattern);
								break;
						}
						break;
					case GHOSTLING:
						switch (mapView[i][j].getValue(DIRECTION)) {
							case 1:
								rectangle.setFill(ghostlingLeftPattern);
								break;
							case 2:
								rectangle.setFill(ghostlingUpPattern);
								break;
							case 3:
								rectangle.setFill(ghostlingRightPattern);
								break;
							case 4:
								rectangle.setFill(ghostlingDownPattern);
								break;
							default:
								rectangle.setFill(ghostlingDownPattern);
								break;
						}
						break;
					case FIRE:
						rectangle.setFill(firePattern);
						break;
					case NORTHTHING:
						rectangle.setFill(northThingPattern);
						break;
					case EASTTHING:
						rectangle.setFill(eastThingPattern);
						break;
					case SOUTHTHING:
						rectangle.setFill(southThingPattern);
						break;
					case WESTTHING:
						rectangle.setFill(westThingPattern);
						break;
					case POT:
						rectangle.setFill(potPattern);
						break;
					case SIEVE:
						rectangle.setFill(sievePattern);
						break;
					case SAND:
						rectangle.setFill(sandPattern);
						break;
					case TURBOTIME:
						rectangle.setFill(turboTimePattern);
						break;
					case HOURGLASS:
						rectangle.setFill(hourGlassPattern);
						break;
					case KAME:
						rectangle.setFill(kamePattern);
						break;
					// any & number sind für Regelblöcke, kommen im Spielfeld nicht vor
					case ANY:
					case NUMBER:
						break;
				}
				grid.add(rectangle, i, j); // auch der GridPane das Dummy-Rect hinzufügen
			}
		}
		
		grid.setLayoutX(280);
		grid.setLayoutY(120);
		// this.getChildren().add(grid);
	}
	
	/**
	 * centerGame zentriert das Spielfeld um ME
	 */
	public void centerGame() {
		int xMin = 0;
		int yMin = 0;
		int xMax = 0;
		int yMax = 0;
		
		for (int x = 0; x < model.getCurrentlevel().getWidth(); x++) {
			for (int y = 0; y < model.getCurrentlevel().getHeight(); y++) {
				Spielfeld currentField = model.getCurrentlevel().getField(x, y);
				if (currentField.getToken() == ME) {
					int xLen = 6;
					int yLen = 4;
					
					xMax = x + xLen;
					yMax = y + yLen;
					xMin = x - xLen;
					yMin = y - yLen;
					
					if (xMin < 0) {
						xMax = xMax + (xLen - x);
						xMin = 0;
					}
					if (yMin < 0) {
						yMax = yMax + (yLen - y);
						yMin = 0;
					}
					if (xMax > model.getCurrentlevel().getWidth() - 1) {
						xMin = xMin - (xLen - (model.getCurrentlevel().getWidth() - 1 - x));
						xMax = model.getCurrentlevel().getWidth() - 1;
					}
					if (yMax > model.getCurrentlevel().getHeight() - 1) {
						yMin = yMin - (yLen - (model.getCurrentlevel().getHeight() - 1 - y));
						yMax = model.getCurrentlevel().getHeight() - 1;
					}
					
					this.xMin = xMin;
					this.xMax = xMax;
					this.yMin = yMin;
					this.yMax = yMax;
				}
			}
		}
	}
	
	public Button getBackButton() {
		return this.backButton;
	}
	
	public Button getMusikButton() {
		return this.musikButton;
	}
	
	public Button getSaveAndExitButton() {
		return this.saveAndExitButton;
	}
	
	@Override
	public void update(Observable o, Object arg) {
		if (this.model.getRunning()) {
			centerGame();
			drawTime();
			this.saveAndExitButton.setVisible(this.model.getCrafmode());
		}
	}
	
}
