package view;

import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.stage.Popup;
import model.Model;

import java.util.Observable;
import java.util.Observer;

/**
 * Anzeige des Gewonnen- PopUps
 *
 * created by Vadim and Leo
 */

public class GewonnenPopUp extends Popup implements Observer {
	
	private Model				model;
	private ImgLoader			imgLoader;
	private View				view;
	public Popup				popup;
	private VBox				vBox;
	private Label				stonesPushed, enemiesKilled, explosionsOccured;
	public Button				backToLevAuswahlButton, playAgainButton;
	private LevelAuswahl		levelAuswahl;
	private LevelAuswahlPopUp	levelAuswahlPopUp;
	private ImageView			playAgainButtonImg;
	
	public GewonnenPopUp(Model model, View view) {
		this.model = model;
		this.view = view;
		this.imgLoader = this.view.getImgLoader();
		this.popup = new Popup();
		this.levelAuswahl = new LevelAuswahl(this.model, this.view, 0);
		this.levelAuswahlPopUp = new LevelAuswahlPopUp(this.model, this.view);
		this.vBox = new VBox(20);
		
		// Labeln zur Anzeige der Spiel-Statisiken in der VBox
		this.stonesPushed = new Label();
		this.stonesPushed.setFont(Font.font("Papyrus", FontPosture.ITALIC, 30));
		
		this.enemiesKilled = new Label();
		this.enemiesKilled.setFont(Font.font("Papyrus", FontPosture.ITALIC, 30));
		
		this.explosionsOccured = new Label();
		this.explosionsOccured.setFont(Font.font("Papyrus", FontPosture.ITALIC, 30));
		
		// Hinzufügen der Elemente zur VBox und platzieren dieser
		this.vBox.getChildren().addAll(this.stonesPushed, this.enemiesKilled, this.explosionsOccured);
		this.vBox.setLayoutX(120);
		this.vBox.setLayoutY(200);
		
		// Laden des Hintergrundbildes
		this.levelAuswahl.imgView = new ImageView(this.imgLoader.getGewonnenPopUpBackground());
		this.levelAuswahl.fitImgView();
		
		/**
		 * Erstellen der benötigten Buttons
		 */
		
		// Back-Button
		this.backToLevAuswahlButton = new Button();
		this.backToLevAuswahlButton.setGraphic(this.levelAuswahlPopUp.backButtonImg);
		this.backToLevAuswahlButton.setRotate(4);
		this.backToLevAuswahlButton.setScaleX(1.1);
		this.backToLevAuswahlButton.setScaleY(1.1);
		this.backToLevAuswahlButton.setBackground(null);
		this.backToLevAuswahlButton.setBorder(null);
		this.backToLevAuswahlButton.setPadding(Insets.EMPTY);
		this.backToLevAuswahlButton.relocate(240, 552);
		
		// Play-Again Button
		this.playAgainButton = new Button();
		this.playAgainButtonImg = new ImageView(imgLoader.getPlayAgainButton());
		this.playAgainButtonImg.setSmooth(true);
		this.playAgainButtonImg.setPreserveRatio(true);
		this.playAgainButton.setGraphic(playAgainButtonImg);
		this.playAgainButton.setBackground(null);
		this.playAgainButton.setBorder(null);
		this.playAgainButton.setPadding(Insets.EMPTY);
		this.playAgainButton.relocate(445, 547);
		
		// Hinzufügen der benötigten Elemente zu dem eigentlichen PopUp
		this.popup.getContent().addAll(this.levelAuswahl.imgView, this.vBox, this.backToLevAuswahlButton,
				this.playAgainButton);
		this.popup.show(this.view.stage);
		
		this.model.addObserver(this);
	}
	
	public void drawTime() {
		this.stonesPushed.setText("Stones pushed: " + this.model.getStonesPushed());
		this.stonesPushed.setTextFill(Color.DARKGRAY);
		this.enemiesKilled.setText("Enemies killed: " + this.model.getEnemiesKilled());
		this.enemiesKilled.setTextFill(Color.DARKGRAY);
		this.explosionsOccured.setText("Explosions occured: " + this.model.getExplosionsOccurred());
		this.explosionsOccured.setTextFill(Color.DARKGRAY);
	}
	
	@Override
	public void update(Observable o, Object arg) {
		drawTime();
	}
	
}