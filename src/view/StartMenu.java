package view;

import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import model.Model;

/**
 * Anzeige des Startfensters des Spiels
 *
 * created by Vadim
 */
public class StartMenu extends Pane {
	
	private View		view;
	private ImgLoader	imgLoader;
	private Pane		pane;
	private ImageView	imgViewStart, levelEditorView, startButtonView, exitButtonView;
	public Button		startButton, exitButton, levelEditorButton;
	
	public StartMenu(Model model, View view) {
		this.view = view;
		this.imgLoader = this.view.getImgLoader();
		this.pane = new Pane();
		
		// Laden des Startmenü-Hintergrundbildes
		this.imgViewStart = new ImageView(this.imgLoader.getStartMenuBackground());
		this.imgViewStart.setFitHeight(this.getMaxHeight());
		this.imgViewStart.setFitWidth(this.getMaxWidth());
		this.imgViewStart.setPreserveRatio(true);
		this.imgViewStart.setSmooth(true);
		
		// Erstellen des Level-Editor Buttons
		this.levelEditorButton = new Button();
		this.levelEditorView = new ImageView(this.imgLoader.getLevelEditorButtonImg());
		this.levelEditorView.setSmooth(true);
		this.levelEditorView.setPreserveRatio(true);
		this.levelEditorButton.setGraphic(levelEditorView);
		this.levelEditorButton.setBackground(null);
		this.levelEditorButton.setBorder(null);
		this.levelEditorButton.setPadding(Insets.EMPTY);
		this.levelEditorButton.relocate(655, 390);
		
		// Erstellen Start-Buttons
		this.startButton = new Button();
		this.startButtonView = new ImageView(this.imgLoader.getStartButtonImg());
		this.startButtonView.setSmooth(true);
		this.startButtonView.setPreserveRatio(true);
		this.startButtonView.setScaleX(1.1);
		this.startButtonView.setScaleY(1.1);
		this.startButton.setGraphic(startButtonView);
		this.startButton.setBackground(null);
		this.startButton.setBorder(null);
		this.startButton.setPadding(Insets.EMPTY);
		this.startButton.relocate(921, 420);
		
		// Erstellen des Exit Buttons
		this.exitButton = new Button();
		this.exitButtonView = new ImageView(this.imgLoader.getExitButtonImg());
		this.exitButtonView.setPreserveRatio(true);
		this.exitButtonView.setSmooth(true);
		this.exitButtonView.setScaleX(1.1);
		this.exitButtonView.setScaleY(1.1);
		this.exitButton.setGraphic(exitButtonView);
		this.exitButton.setBackground(null);
		this.exitButton.setBorder(null);
		this.exitButton.setPadding(Insets.EMPTY);
		this.exitButton.relocate(711, 555);
		
		// Zusammenfügen sämtlicher für die Darstellung des Startmenüs benötigter Elemente
		this.getChildren().addAll(this.pane, this.imgViewStart, this.startButton, this.exitButton,
				this.levelEditorButton);
	}
	
}
