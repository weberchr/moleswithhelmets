package view;

import javafx.animation.ParallelTransition;
import javafx.animation.PathTransition;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.scene.shape.CubicCurveTo;
import javafx.scene.shape.MoveTo;
import javafx.scene.shape.Path;
import javafx.util.Duration;
import model.Model;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

/**
 * Erstellen des Levelauswahl-Fensters
 *
 * created by Vadim
 */

public class LevelAuswahl extends Pane implements Observer {
	
	private Model					model;
	private ImgLoader				imgLoader;
	private View					view;
	private Pane					pane;
	private PathTransition			pathTransition;
	private ParallelTransition		parallelTransition;
	private Path					path, path2, path3, path4, path5, path6;
	public Button					backToStartButton, nextButton, previousButton, levelButton, tutorialButton;
	public ImageView				imgView, tunnelView, levelMoly, nextButtonView, mainButton, buttonImage,
			previousButtonView, tutorialButtonView;
	public List<Button>				levelButtons	= new ArrayList<>();
	private List<PathTransition>	pathTransitions;
	
	public LevelAuswahl(Model model, View view, int auswahlindex) {
		this.model = model;
		this.view = view;
		this.imgLoader = this.view.getImgLoader();
		this.pane = new Pane();
		
		/**
		 * Laden der benötigten ImageViews
		 */
		this.imgView = new ImageView(this.imgLoader.getLevelsBackground());
		fitImgView();
		
		this.tunnelView = new ImageView(this.imgLoader.getTunnelFront());
		this.tunnelView.setPreserveRatio(true);
		this.tunnelView.setSmooth(true);
		this.tunnelView.relocate(1104, 244);
		
		this.levelMoly = new ImageView(imgLoader.getLevelAuswahlMoly());
		this.levelMoly.setPreserveRatio(true);
		this.levelMoly.setSmooth(true);
		this.levelMoly.relocate(20, 478);
		
		/**
		 * Erstellen sämtlicher benötigter Buttons
		 */
		// Next-Button
		this.nextButton = new Button();
		this.nextButtonView = new ImageView(this.imgLoader.getNextLevelsButton());
		this.nextButtonView.setSmooth(true);
		this.nextButtonView.setPreserveRatio(true);
		this.nextButton.setGraphic(nextButtonView);
		this.nextButton.setBackground(null);
		this.nextButton.setBorder(null);
		this.nextButton.setPadding(Insets.EMPTY);
		this.nextButton.setScaleX(1.2);
		this.nextButton.setScaleY(1.2);
		this.nextButton.relocate(1080, 464);
		
		// Previous-Button
		this.previousButton = new Button();
		this.previousButtonView = new ImageView(this.imgLoader.getPreviousLevelsButton());
		this.previousButtonView.setSmooth(true);
		this.previousButtonView.setPreserveRatio(true);
		this.previousButton.setGraphic(this.previousButtonView);
		this.previousButton.setBackground(null);
		this.previousButton.setBorder(null);
		this.previousButton.setPadding(Insets.EMPTY);
		this.previousButton.setRotate(-10);
		this.previousButton.setScaleX(1.1);
		this.previousButton.setScaleY(1.1);
		this.previousButton.relocate(1080, 462);
		
		// Tutorial-Button
		this.tutorialButton = new Button();
		this.tutorialButtonView = new ImageView(imgLoader.getTutorialButton());
		this.tutorialButton.setGraphic(tutorialButtonView);
		this.tutorialButton.setBackground(null);
		this.tutorialButton.setBorder(null);
		this.tutorialButton.setPadding(Insets.EMPTY);
		this.tutorialButton.relocate(230, 205);
		
		// Main-Button
		this.backToStartButton = new Button();
		this.mainButton = new ImageView(imgLoader.getMainMenuButton());
		this.backToStartButton.setGraphic(mainButton);
		this.backToStartButton.setBackground(null);
		this.backToStartButton.setBorder(null);
		this.backToStartButton.setPadding(Insets.EMPTY);
		this.backToStartButton.relocate(710, 607);
		
		// Erstellen der Levelbuttons (Wägen) 1-12
		for (int lnr = 0; lnr < Model.NUMBEROFLEVELS; lnr++) {
			this.buttonImage = new ImageView();
			this.levelButton = new Button();
			this.levelButton.setBackground(null);
			this.levelButton.setBorder(null);
			this.levelButton.setPadding(Insets.EMPTY);
			this.levelButton.setGraphic(buttonImage);
			
			if (lnr == 0) {
				this.buttonImage.setImage(this.imgLoader.getButtonLevel1());
				this.buttonImage.setRotate(165);
			} else if (lnr == 1) {
				this.buttonImage.setImage(imgLoader.getButtonLevel2());
				this.buttonImage.setRotate(158);
			} else if (lnr == 2) {
				this.buttonImage.setImage(imgLoader.getButtonLevel3());
				this.buttonImage.setRotate(162);
			} else if (lnr == 3) {
				this.buttonImage.setImage(imgLoader.getButtonLevel4());
				this.buttonImage.setRotate(203);
			} else if (lnr == 4) {
				this.buttonImage.setImage(imgLoader.getButtonLevel5());
				this.buttonImage.setRotate(2);
			} else if (lnr == 5) {
				this.buttonImage.setImage(imgLoader.getButtonLevel6());
				this.buttonImage.setRotate(-6);
			} else if (lnr == 6) {
				this.buttonImage.setImage(imgLoader.getButtonLevel7());
				this.buttonImage.setRotate(165);
			} else if (lnr == 7) {
				this.buttonImage.setImage(imgLoader.getButtonLevel8());
				this.buttonImage.setRotate(158);
			} else if (lnr == 8) {
				this.buttonImage.setImage(imgLoader.getButtonLevel9());
				this.buttonImage.setRotate(162);
			} else if (lnr == 9) {
				this.buttonImage.setImage(imgLoader.getButtonLevel10());
				this.buttonImage.setRotate(203);
			} else if (lnr == 10) {
				this.buttonImage.setImage(imgLoader.getButtonLevel11());
				this.buttonImage.setRotate(2);
			} else {
				this.buttonImage.setImage(imgLoader.getButtonLevel12());
				this.buttonImage.setRotate(-6);
			}
			
			this.levelButtons.add(this.levelButton);
		}
		
		/**
		 * Erstellen von Pathtransitions
		 */
		// Initialisierung 6 verschiedener Paths
		this.path = new Path();
		this.path2 = new Path();
		this.path3 = new Path();
		this.path4 = new Path();
		this.path5 = new Path();
		this.path6 = new Path();
		
		// Initialisierung 6 verschiedener Transitions für die Paths
		// Werden für 12 Levels benutzt
		pathTransitions = new ArrayList<>();
		
		for (int i = auswahlindex * 6; i < (auswahlindex + 1) * 6; i++) {
			this.pathTransition = new PathTransition();
			this.pathTransition.setOrientation(PathTransition.OrientationType.ORTHOGONAL_TO_TANGENT);
			this.pathTransition.setCycleCount(1);
			this.pathTransition.setAutoReverse(false);
			this.pathTransition.setNode(this.levelButtons.get(i));
			
			switch (i % 6) {
				case 0:
					this.pathTransition.setDuration(Duration.seconds(2));
					this.pathTransition.setPath(this.path);
					break;
				case 1:
					this.pathTransition.setDuration(Duration.seconds(2.5));
					this.pathTransition.setPath(this.path2);
					break;
				case 2:
					this.pathTransition.setDuration(Duration.seconds(3));
					this.pathTransition.setPath(this.path3);
					break;
				case 3:
					this.pathTransition.setDuration(Duration.seconds(3.2));
					this.pathTransition.setPath(this.path4);
					break;
				case 4:
					this.pathTransition.setDuration(Duration.seconds(3.5));
					this.pathTransition.setPath(this.path5);
					this.pathTransition.setOrientation(PathTransition.OrientationType.NONE);
					break;
				case 5:
					this.pathTransition.setDuration(Duration.seconds(4));
					this.pathTransition.setPath(this.path6);
					this.pathTransition.setOrientation(PathTransition.OrientationType.NONE);
					break;
			}
			
			pathTransitions.add(pathTransition);
		}
		
		// Bestimmung der Koordinaten für die Paths
		this.path.getElements().add(new MoveTo(1260, 376));
		this.path.getElements().add(new CubicCurveTo(1260, 366, 966, 417, 684, 542));
		this.path.getElements().add(new CubicCurveTo(685, 542, 557, 573, 409, 515));
		this.path.getElements().add(new CubicCurveTo(409, 515, 265, 430, 160, 395));
		
		this.path2.getElements().add(new MoveTo(1260, 376));
		this.path2.getElements().add(new CubicCurveTo(1260, 360, 966, 407, 684, 540));
		this.path2.getElements().add(new CubicCurveTo(684, 540, 557, 585, 319, 450));
		
		this.path3.getElements().add(new MoveTo(1260, 376));
		this.path3.getElements().add(new CubicCurveTo(1260, 355, 966, 417, 684, 530));
		this.path3.getElements().add(new CubicCurveTo(684, 530, 557, 575, 479, 530));
		
		this.path4.getElements().add(new MoveTo(1260, 376));
		this.path4.getElements().add(new CubicCurveTo(1260, 350, 966, 427, 644, 540));
		
		this.path5.getElements().add(new MoveTo(1260, 376));
		this.path5.getElements().add(new CubicCurveTo(1260, 370, 966, 392, 804, 475));
		
		this.path6.getElements().add(new MoveTo(1260, 376));
		this.path6.getElements().add(new CubicCurveTo(1260, 376, 1102, 375, 970, 405));
		
		this.getChildren().addAll(this.pane, this.imgView, this.levelMoly, this.backToStartButton, this.tutorialButton);
		
		// Sperre für Level-Buttons.Die werden erst angezeigt wenn der Level freigeschaltet ist.
		for (int i = auswahlindex * 6; i < (auswahlindex + 1) * 6; i++) {
			if (this.model.calculateUnlocked().get(i)) {
				this.getChildren().add(this.levelButtons.get(i));
			}
		}
		
		this.getChildren().add(tunnelView);
		if (auswahlindex == 0) {
			this.getChildren().add(this.nextButton);
		}
		if (auswahlindex == 1) {
			this.getChildren().add(this.previousButton);
		}
		
		this.parallelTransition = new ParallelTransition();
		this.parallelTransition.getChildren().addAll(this.pathTransitions.get(0), this.pathTransitions.get(1),
				this.pathTransitions.get(2), this.pathTransitions.get(3), this.pathTransitions.get(4),
				this.pathTransitions.get(5));
		
		this.model.addObserver(this);
	}
	
	public void fitImgView() {
		this.imgView.setFitHeight(this.getHeight());
		this.imgView.setFitWidth(this.getWidth());
		this.imgView.setPreserveRatio(true);
		this.imgView.setSmooth(true);
	}
	
	public void drawTime() {
		this.parallelTransition.play();
	}
	
	@Override
	public void update(Observable o, Object arg) {
		drawTime();
	}
	
}