package view;

import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.image.ImageView;
import javafx.stage.Popup;
import model.Model;

/**
 * Erstellen des Allert - PopUps
 *
 * created by Leo
 */
public class AlertPopUp extends Popup {
	
	private Model			model;
	private ImgLoader		imgLoader;
	private View			view;
	public Popup			popup;
	private LevelAuswahl	levelAuswahl;
	
	public Button			okayButton;
	
	public AlertPopUp(Model model, View view) {
		this.model = model;
		this.view = view;
		this.imgLoader = this.view.getImgLoader();
		this.popup = new Popup();
		this.levelAuswahl = new LevelAuswahl(this.model, this.view, 0);
		
		this.levelAuswahl.imgView = new ImageView(this.imgLoader.getAlertImg());
		this.levelAuswahl.fitImgView();
		
		// benötigter Button wird geladen
		this.okayButton = new Button();
		this.okayButton.setGraphic(this.levelAuswahl.imgView);
		this.okayButton.setBackground(null);
		this.okayButton.setBorder(null);
		this.okayButton.setPadding(Insets.EMPTY);
		
		// PopUp platziert und angezeigt
		this.popup.setX(1000);
		this.popup.setY(400);
		this.popup.getContent().addAll(this.okayButton);
		this.popup.show(this.view.stage);
	}
	
}