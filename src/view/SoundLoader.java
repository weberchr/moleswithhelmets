package view;

import javafx.scene.media.AudioClip;

/**
 * Erstellen des Soundloaders für das Spiel
 *
 * created by Vadim
 */

public class SoundLoader {
	
	private AudioClip explosion, pathStep, molyDiamondFound, applause, gameOver, molyOiOi, stonefalling, kamehame;
	
	public SoundLoader() {
		this.pathStep = new AudioClip(SoundLoader.class.getResource("../music/PathStep.mp3").toExternalForm());
		this.explosion = new AudioClip(SoundLoader.class.getResource("../music/Explosion.mp3").toExternalForm());
		this.molyDiamondFound = new AudioClip(
				SoundLoader.class.getResource("../music/MolyDiamondFound.mp3").toExternalForm());
		this.applause = new AudioClip(SoundLoader.class.getResource("../music/Applause.mp3").toExternalForm());
		this.gameOver = new AudioClip(SoundLoader.class.getResource("../music/GameOver.mp3").toExternalForm());
		this.molyOiOi = new AudioClip((SoundLoader.class.getResource("../music/MolyOiOi.mp3").toExternalForm()));
		this.stonefalling = new AudioClip(SoundLoader.class.getResource("../music/FallAndSplat.mp3").toExternalForm());
		this.kamehame = new AudioClip(SoundLoader.class.getResource("../music/KameHameHa.mp3").toExternalForm());
	}
	
	// AudioClip-Getters
	public AudioClip getPathStep() {
		return this.pathStep;
	}
	
	public AudioClip getExplosion() {
		return this.explosion;
	}
	
	public AudioClip getMolyDiamondFound() {
		return molyDiamondFound;
	}
	
	public AudioClip getApplause() {
		return this.applause;
	}
	
	public AudioClip getGameOver() {
		return this.gameOver;
	}
	
	public AudioClip getMolyOiOi() {
		return this.molyOiOi;
	}
	
	public AudioClip getStonefalling() {
		return this.stonefalling;
	}
	
	public AudioClip getKamehame() {
		return this.kamehame;
	}
	
}
