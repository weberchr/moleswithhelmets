package view;

import javafx.scene.control.Label;
import javafx.scene.layout.Pane;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import model.Model;

import java.util.Observable;
import java.util.Observer;

/**
 * Digitaluhr
 */

/**
 * @author Andi & Chris
 */

public class DigitalClock extends Pane implements Observer {
	
	private Model	model;
	private Label	timeLabel;
	
	public DigitalClock(Model model) {
		this.model = model;
		this.model.addObserver(this);
		this.setPrefSize(100, 25);
		this.timeLabel = new Label();
		this.timeLabel.setFont((Font.font("Papyrus", FontPosture.ITALIC, 30)));
		this.getChildren().add(this.timeLabel);
		this.updateText();
	}
	
	private void updateText() {
		timeLabel.setText(String.format("Time left: %02d:%02d", model.getMinutes(), model.getSeconds()));
	}
	
	@Override
	public void update(Observable o, Object arg) {
		updateText();
	}
	
}
