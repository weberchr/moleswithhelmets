package view;

import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Rectangle;
import javafx.scene.transform.Affine;
import model.Model;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import static model.Token.*;

/**
 * Anzeige des LevelEditors
 *
 * created by Leo
 */

// LevelEditorView
public class LevelEditor extends Pane implements Observer {
	
	private Model			model;
	private View			view;
	private ImgLoader		imgLoader;
	private Pane			pane;
	public GridPane			levelGrid;
	public Button[][]		levelGridView;
	public Button			backLEButton, gridButtonSmall, gridButtonMedium, gridButtonBig;
	public Button			saveButton;
	private ImagePattern	meLeftPattern, mudPattern, pathPattern, gemPattern, wallPattern, stonePattern,
			blocklingPattern, swaplingPattern, xlingPattern, slimePattern, exitPattern, hourGlassPattern,
			turboTimePattern;
	private ImageView		backView, chooseSizeBack, chooseTokenBack, saveAndExitImg, smallButtonImg, mediumButtonImg,
			bigButtonImg;
	private VBox			vBox, vBox1, vBox2;
	private HBox			hBox, hBox2;
	private Button			tokenButton;
	private ImageView		buttonImage;
	public List<Button>		tokenButtons, buttons;
	private LevelAuswahl	levelAuswahl;
	private Rectangle[][]	rectangles;
	public Affine			affine;
	
	public LevelEditor(Model model, View view) {
		this.model = model;
		this.view = view;
		this.imgLoader = this.view.getImgLoader();
		this.pane = new Pane();
		this.affine = new Affine();
		this.vBox = new VBox(20);
		this.hBox = new HBox(10);
		this.levelAuswahl = new LevelAuswahl(this.model, this.view, 0);
		
		// Erstellen der ImagePatterns für die Rectangles
		meLeftPattern = new ImagePattern(imgLoader.getMeLeftWalkingImage());
		mudPattern = new ImagePattern(imgLoader.getMudImage());
		pathPattern = new ImagePattern(imgLoader.getPathImage());
		gemPattern = new ImagePattern(imgLoader.getGemImage());
		wallPattern = new ImagePattern(imgLoader.getWallImage());
		stonePattern = new ImagePattern(imgLoader.getStoneImage());
		blocklingPattern = new ImagePattern(imgLoader.getFoxImage());
		swaplingPattern = new ImagePattern(imgLoader.getRacoonImage());
		xlingPattern = new ImagePattern(imgLoader.getSkeletonImage());
		slimePattern = new ImagePattern(imgLoader.getSlimeImage());
		exitPattern = new ImagePattern(imgLoader.getExitImage());
		turboTimePattern = new ImagePattern(imgLoader.getTurboTimeImage());
		hourGlassPattern = new ImagePattern(imgLoader.getHourGlassImage());
		
		// HintergrundBild
		this.levelAuswahl.imgView = new ImageView(this.imgLoader.getLevelEditorBackground());
		this.levelAuswahl.fitImgView();
		
		// Erstellen aller benötigter Buttons
		this.chooseSizeBack = new ImageView(this.imgLoader.getChooseSizeBack());
		this.chooseSizeBack.setScaleX(0.9);
		this.chooseSizeBack.setScaleY(0.9);
		this.chooseSizeBack.setPreserveRatio(true);
		this.chooseSizeBack.setSmooth(true);
		this.chooseSizeBack.relocate(10, 0);
		
		this.chooseTokenBack = new ImageView(this.imgLoader.getChooseTokenBack());
		this.chooseTokenBack.setScaleX(1.1);
		this.chooseTokenBack.setScaleY(1.1);
		this.chooseTokenBack.setPreserveRatio(true);
		this.chooseTokenBack.setSmooth(true);
		this.chooseTokenBack.relocate(900, 0);
		
		this.backLEButton = new Button();
		this.backView = new ImageView(imgLoader.getBackButton());
		this.backView.setPreserveRatio(true);
		this.backView.setSmooth(true);
		this.backLEButton.setGraphic(backView);
		this.backLEButton.setBackground(null);
		this.backLEButton.setBorder(null);
		this.backLEButton.setPadding(Insets.EMPTY);
		this.backLEButton.relocate(30, 560);
		
		this.saveButton = new Button();
		this.saveAndExitImg = new ImageView(this.imgLoader.getSaveAndExitButtonLE());
		this.saveAndExitImg.setPreserveRatio(true);
		this.saveAndExitImg.setSmooth(true);
		this.saveAndExitImg.setScaleX(0.9);
		this.saveAndExitImg.setScaleY(0.9);
		this.saveButton.setGraphic(saveAndExitImg);
		this.saveButton.setBackground(null);
		this.saveButton.setBorder(null);
		this.saveButton.setPadding(Insets.EMPTY);
		this.saveButton.relocate(1050, 530);
		
		this.gridButtonSmall = new Button();
		this.smallButtonImg = new ImageView(this.imgLoader.getSmallButton());
		this.smallButtonImg.setScaleX(0.9);
		this.smallButtonImg.setScaleY(0.9);
		this.smallButtonImg.setPreserveRatio(true);
		this.smallButtonImg.setSmooth(true);
		this.gridButtonSmall.setBackground(null);
		this.gridButtonSmall.setBorder(null);
		this.gridButtonSmall.setPadding(Insets.EMPTY);
		this.gridButtonSmall.setGraphic(smallButtonImg);
		
		this.gridButtonMedium = new Button();
		this.mediumButtonImg = new ImageView(this.imgLoader.getMediumButton());
		this.mediumButtonImg.setScaleX(0.9);
		this.mediumButtonImg.setScaleY(0.9);
		this.mediumButtonImg.setPreserveRatio(true);
		this.mediumButtonImg.setSmooth(true);
		this.gridButtonMedium.setBackground(null);
		this.gridButtonMedium.setBorder(null);
		this.gridButtonMedium.setPadding(Insets.EMPTY);
		this.gridButtonMedium.setGraphic(mediumButtonImg);
		
		this.gridButtonBig = new Button();
		this.bigButtonImg = new ImageView(this.imgLoader.getBigButton());
		this.bigButtonImg.setScaleX(0.9);
		this.bigButtonImg.setScaleY(0.9);
		this.bigButtonImg.setPreserveRatio(true);
		this.bigButtonImg.setSmooth(true);
		this.gridButtonBig.setBackground(null);
		this.gridButtonBig.setBorder(null);
		this.gridButtonBig.setPadding(Insets.EMPTY);
		this.gridButtonBig.setGraphic(bigButtonImg);
		
		// Liste mit allen Buttons, die für die verschiedenen Tokens stehen
		tokenButtons = new ArrayList<>();
		
		for (int tnr = 0; tnr < 13; tnr++) {
			this.buttonImage = new ImageView();
			this.buttonImage.setSmooth(true);
			this.buttonImage.setPreserveRatio(true);
			this.buttonImage.setFitWidth(70);
			this.buttonImage.setFitHeight(70);
			this.tokenButton = new Button();
			this.tokenButton.setBackground(null);
			this.tokenButton.setBorder(null);
			this.tokenButton.setPadding(Insets.EMPTY);
			this.tokenButton.setGraphic(buttonImage);
			switch (tnr) {
				case 0:
					this.buttonImage.setImage(this.imgLoader.getMeLeftImage());
					break;
				case 1:
					this.buttonImage.setImage(this.imgLoader.getMudImage());
					break;
				case 2:
					this.buttonImage.setImage(this.imgLoader.getPathImage());
					break;
				case 3:
					this.buttonImage.setImage(this.imgLoader.getGemImage());
					break;
				case 4:
					this.buttonImage.setImage(this.imgLoader.getStoneImage());
					break;
				case 5:
					this.buttonImage.setImage(this.imgLoader.getWallImage());
					break;
				case 6:
					this.buttonImage.setImage(this.imgLoader.getFoxImage());
					break;
				case 7:
					this.buttonImage.setImage(this.imgLoader.getRacoonImage());
					break;
				case 8:
					this.buttonImage.setImage(this.imgLoader.getSkeletonImage());
					break;
				case 9:
					this.buttonImage.setImage(this.imgLoader.getSlimeImage());
					break;
				case 10:
					this.buttonImage.setImage(this.imgLoader.getTurboTimeImage());
					break;
				case 11:
					this.buttonImage.setImage(this.imgLoader.getHourGlassImage());
					break;
				case 12:
					this.buttonImage.setImage(this.imgLoader.getExitImage());
					break;
			}
			
			this.tokenButtons.add(this.tokenButton);
		}
		
		// Layout-G'schichtn
		this.vBox = new VBox(10);
		this.vBox.getChildren().addAll(tokenButtons.get(0), tokenButtons.get(1), tokenButtons.get(2),
				tokenButtons.get(3));
		this.vBox1 = new VBox(10);
		this.vBox1.getChildren().addAll(tokenButtons.get(4), tokenButtons.get(5), tokenButtons.get(6),
				tokenButtons.get(7));
		this.vBox2 = new VBox(10);
		this.vBox2.getChildren().addAll(tokenButtons.get(8), tokenButtons.get(9), tokenButtons.get(10),
				tokenButtons.get(11), tokenButtons.get(12));
		
		this.hBox2 = new HBox(20);
		this.hBox2.getChildren().addAll(this.vBox, this.vBox1, this.vBox2);
		this.hBox2.relocate(920, 100);
		
		this.hBox.getChildren().addAll(this.gridButtonSmall, this.gridButtonMedium, this.gridButtonBig);
		this.hBox.relocate(47, 60);
		
		this.getChildren().addAll(this.pane, this.levelAuswahl.imgView, this.chooseSizeBack, this.chooseTokenBack,
				this.backLEButton, this.saveButton, this.hBox, this.hBox2);
		
		this.levelGrid = new GridPane();// GridPane für das Raster
		this.levelGrid.setVgap(2);
		this.levelGrid.setHgap(2);
		this.levelGrid.relocate(230, 128);
		
		drawRectangles();
		this.getChildren().add(levelGrid);
		this.model.addObserver(this);
	}
	
	/**
	 * Zeichnen des RectangleGrids, das mit neuen Tokens versehen werden kann
	 */
	public void drawRectangles() {
		levelGrid.getChildren().clear();
		this.rectangles = new Rectangle[this.model.getWidthLE()][this.model.getHeightLE()];
		for (int x = 0; x < this.model.getWidthLE(); x++) {
			for (int y = 0; y < this.model.getHeightLE(); y++) {
				Rectangle rect = new Rectangle(this.model.getRectWidth(), this.model.getRectWidth());
				rectangles[x][y] = rect;
				levelGrid.add(rect, x, y);
			}
		}
	}
	
	public Rectangle[][] getRectangleGrid() {
		return this.rectangles;
	}
	
	/**
	 * Abfrage des aktuellen Tokens im Model, setzen des zugehörigen ImagePatterns
	 */
	public void levelEditorRectangleToken() {
		for (int i = 0; i < model.getWidthLE(); i++) {
			for (int j = 0; j < model.getHeightLE(); j++) {
				if (model.getLevelEditorGrid(i, j).getToken() == PATH) {
					rectangles[i][j].setFill(pathPattern);
				} else if (model.getLevelEditorGrid(i, j).getToken() == WALL) {
					rectangles[i][j].setFill(wallPattern);
				} else if (model.getLevelEditorGrid(i, j).getToken() == MUD) {
					rectangles[i][j].setFill(mudPattern);
				} else if (model.getLevelEditorGrid(i, j).getToken() == STONE) {
					rectangles[i][j].setFill(stonePattern);
				} else if (model.getLevelEditorGrid(i, j).getToken() == ME) {
					rectangles[i][j].setFill(meLeftPattern);
				} else if (model.getLevelEditorGrid(i, j).getToken() == GEM) {
					rectangles[i][j].setFill(gemPattern);
				} else if (model.getLevelEditorGrid(i, j).getToken() == BLOCKLING) {
					rectangles[i][j].setFill(blocklingPattern);
				} else if (model.getLevelEditorGrid(i, j).getToken() == XLING) {
					rectangles[i][j].setFill(xlingPattern);
				} else if (model.getLevelEditorGrid(i, j).getToken() == SWAPLING) {
					rectangles[i][j].setFill(swaplingPattern);
				} else if (model.getLevelEditorGrid(i, j).getToken() == SLIME) {
					rectangles[i][j].setFill(slimePattern);
				} else if (model.getLevelEditorGrid(i, j).getToken() == TURBOTIME) {
					rectangles[i][j].setFill(turboTimePattern);
				} else if (model.getLevelEditorGrid(i, j).getToken() == HOURGLASS) {
					rectangles[i][j].setFill(hourGlassPattern);
				} else if (model.getLevelEditorGrid(i, j).getToken() == EXIT) {
					rectangles[i][j].setFill(exitPattern);
				}
			}
		}
	}
	
	@Override
	public void update(Observable o, Object arg) {
		if (model.getLevelEditorRunning() && !model.getRunning()) {
			// levelEditorGridToken();}
			if (rectangles.length != this.model.getWidthLE() || rectangles[0].length != this.model.getHeightLE()) {
				drawRectangles();
			}
			levelEditorRectangleToken();
		}
	}
	
}
