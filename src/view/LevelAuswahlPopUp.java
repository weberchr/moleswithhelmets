package view;

import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.stage.Popup;
import model.Model;

/**
 * Erstellen des Levelauswahl- PopUps
 *
 * created by Vadim and Leo
 */

public class LevelAuswahlPopUp extends Popup {
	
	private View		view;
	private ImgLoader	imgLoader;
	public ImageView	imgView2, imgView4, backButtonImg, craftButtonImg, gameButtonImg;
	public Popup		popup;
	public Button		gameButton, backToLevelAuswahlButton, craftButton;
	public Pane			mappane;
	
	public LevelAuswahlPopUp(Model model, View view) {
		this.view = view;
		this.imgLoader = this.view.getImgLoader();
		this.popup = new Popup();
		this.popup.setX(70);
		this.popup.setY(20);
		this.mappane = new Pane();
		
		/**
		 * Laden der benötigten ImageViews
		 */
		this.imgView2 = new ImageView();
		this.imgView2.setLayoutX(200);
		this.imgView2.setLayoutY(25);
		this.imgView2.setPreserveRatio(true);
		this.imgView2.setSmooth(true);
		
		this.imgView4 = new ImageView();
		this.imgView4.setSmooth(true);
		this.imgView4.setPreserveRatio(true);
		this.imgView4.relocate(490, 480);
		
		/**
		 * Erstellen der benötigten Buttons
		 */
		// Craft-Button
		this.craftButtonImg = new ImageView(imgLoader.getCraftButton());
		this.craftButtonImg.setScaleX(0.7);
		this.craftButtonImg.setScaleY(0.7);
		this.craftButton = new Button();
		this.craftButton.setGraphic(this.craftButtonImg);
		this.craftButton.setBackground(null);
		this.craftButton.setBorder(null);
		this.craftButton.setPadding(Insets.EMPTY);
		this.craftButton.relocate(200, 6);
		
		// Game-Button
		this.gameButton = new Button();
		this.gameButtonImg = new ImageView();
		this.gameButton.setGraphic(gameButtonImg);
		this.gameButton.setBackground(null);
		this.gameButton.setBorder(null);
		this.gameButton.setPadding(Insets.EMPTY);
		this.gameButton.relocate(630, 560);
		
		// Back-Button
		this.backButtonImg = new ImageView(imgLoader.getBackButton());
		smoothBackButton();
		this.backToLevelAuswahlButton = new Button();
		this.backToLevelAuswahlButton.setGraphic(this.backButtonImg);
		this.backToLevelAuswahlButton.setBackground(null);
		this.backToLevelAuswahlButton.setBorder(null);
		this.backToLevelAuswahlButton.setPadding(Insets.EMPTY);
		this.backToLevelAuswahlButton.relocate(460, 585);
		
		// Hinzufügen der benötigten Elemente zu dem eigentlichen PopUp
		this.popup.getContent().addAll(this.imgView2, this.mappane, this.imgView4, this.gameButton,
				this.backToLevelAuswahlButton, this.craftButton);
		this.popup.show(this.view.stage);
		
	}
	
	public ImageView getImgView() {
		return this.imgView2;
	}
	
	public ImageView getHelmets() {
		return this.imgView4;
	}
	
	public void smoothBackButton() {
		this.backButtonImg.setSmooth(true);
		this.backButtonImg.setPreserveRatio(true);
	}
	
}
