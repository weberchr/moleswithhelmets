package view;

import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.stage.Popup;
import model.Model;

import java.util.List;
import java.util.Observable;
import java.util.Observer;

/**
 * Anzeige des Tutorial-PopUps
 */
/**
 * created by Leo
 */

public class TutorialPopUp extends Popup implements Observer {
	
	private Model			model;
	private ImgLoader		imgLoader;
	private View			view;
	public Popup			popup;
	public Button			nextTipButton, closeButton;
	private ImageView		backgroundImg, closeButtonImg, imageView;
	private Pane			imagesPane;
	private List<ImageView>	tutorialImages;
	
	public TutorialPopUp(Model model, View view) {
		this.model = model;
		this.view = view;
		this.imgLoader = this.view.getImgLoader();
		this.popup = new Popup();
		
		// Laden des Hintergrundbildes
		this.backgroundImg = new ImageView(this.imgLoader.getTutorialBackground());
		this.backgroundImg.setSmooth(true);
		this.backgroundImg.setPreserveRatio(true);
		
		// Erstellen der benötigten Buttons
		this.closeButton = new Button();
		this.closeButtonImg = new ImageView(imgLoader.getCloseButton());
		this.closeButtonImg.setSmooth(true);
		this.closeButtonImg.setPreserveRatio(true);
		this.closeButton.setGraphic(closeButtonImg);
		this.closeButton.setBackground(null);
		this.closeButton.setBorder(null);
		this.closeButton.setPadding(Insets.EMPTY);
		this.closeButton.relocate(700, 10);
		
		this.imageView = new ImageView(this.imgLoader.getGeilstesGifDerWelt());
		this.imageView.setSmooth(true);
		this.imageView.setPreserveRatio(true);
		this.imageView.relocate(100, 100);
		
		// Hinzufügen der benötigten Elemente zu dem eigentlichen PopUp
		this.popup.getContent().addAll(this.backgroundImg, this.closeButton, this.imageView);
		this.popup.show(this.view.stage);
		
		this.model.addObserver(this);
	}
	
	public void drawTutorialImages(int tip) {
		switch (tip) {
			case 0:
				this.imagesPane.getChildren().clear();
				this.imagesPane.getChildren().addAll(tutorialImages.get(3), tutorialImages.get(16));
				break;
			case 1:
				this.imagesPane.getChildren().clear();
				this.imagesPane.getChildren().addAll(tutorialImages.get(4), tutorialImages.get(5),
						tutorialImages.get(17));
				break;
			case 2:
				this.imagesPane.getChildren().clear();
				this.imagesPane.getChildren().addAll(tutorialImages.get(6), tutorialImages.get(7),
						tutorialImages.get(18));
				break;
			case 3:
				this.imagesPane.getChildren().clear();
				this.imagesPane.getChildren().addAll(tutorialImages.get(8), tutorialImages.get(9),
						tutorialImages.get(10), tutorialImages.get(19));
				break;
			case 4:
				this.imagesPane.getChildren().clear();
				this.imagesPane.getChildren().addAll(tutorialImages.get(11), tutorialImages.get(12),
						tutorialImages.get(13), tutorialImages.get(14), tutorialImages.get(20));
				break;
		}
	}
	
	@Override
	public void update(Observable o, Object arg) {
	}
	
}