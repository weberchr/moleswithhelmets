package view;

import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.stage.Popup;
import model.Model;

import java.util.Observable;
import java.util.Observer;

/**
 * Anzeige des Verloren- PopUps
 *
 * created by Vadim and Leo
 */

public class VerlorenPopUp extends Popup implements Observer {
	
	private Model				model;
	private ImgLoader			imgLoader;
	private View				view;
	public Popup				popup;
	private VBox				vBox;
	public Button				tryAgainButton, backToLAuswahlButton;
	private Label				stonesPushed, enemiesKilled, explosionsOccured;
	private ImageView			tryAgainImg;
	private LevelAuswahlPopUp	levelAuswahlPopUp;
	private LevelAuswahl		levelAuswahl;
	
	public VerlorenPopUp(Model model, View view) {
		this.model = model;
		this.view = view;
		this.imgLoader = this.view.getImgLoader();
		this.popup = new Popup();
		
		this.levelAuswahlPopUp = new LevelAuswahlPopUp(this.model, this.view);
		this.levelAuswahl = new LevelAuswahl(this.model, this.view, 0);
		this.vBox = new VBox(20);
		
		// Labeln zur Anzeige in der VBox
		this.stonesPushed = new Label();
		this.stonesPushed.setFont(Font.font("Papyrus", FontPosture.ITALIC, 30));
		
		this.enemiesKilled = new Label();
		this.enemiesKilled.setFont(Font.font("Papyrus", FontPosture.ITALIC, 30));
		
		this.explosionsOccured = new Label();
		this.explosionsOccured.setFont(Font.font("Papyrus", FontPosture.ITALIC, 30));
		
		// Hinzufügen der Elemente zur VBox und platzieren dieser
		this.vBox.getChildren().addAll(this.stonesPushed, this.enemiesKilled, this.explosionsOccured);
		this.vBox.setLayoutX(20);
		this.vBox.setLayoutY(200);
		
		// Laden des Hintergrundbildes
		this.levelAuswahl.imgView = new ImageView(this.imgLoader.getVerlorenPopUpBackground());
		this.levelAuswahl.fitImgView();
		
		this.levelAuswahlPopUp.smoothBackButton();
		
		/**
		 * Erstellen der benötigten Buttons
		 */
		// Back_Button
		this.backToLAuswahlButton = new Button();
		this.backToLAuswahlButton.setGraphic(this.levelAuswahlPopUp.backButtonImg);
		this.backToLAuswahlButton.setRotate(6);
		this.backToLAuswahlButton.setBackground(null);
		this.backToLAuswahlButton.setBorder(null);
		this.backToLAuswahlButton.setPadding(Insets.EMPTY);
		this.backToLAuswahlButton.relocate(260, 535);
		
		// Try-Again Button
		this.tryAgainButton = new Button();
		this.tryAgainImg = new ImageView(imgLoader.getTryAgainButton());
		this.tryAgainImg.setSmooth(true);
		this.tryAgainImg.setPreserveRatio(true);
		this.tryAgainButton.setGraphic(tryAgainImg);
		this.tryAgainButton.setBackground(null);
		this.tryAgainButton.setBorder(null);
		this.tryAgainButton.setPadding(Insets.EMPTY);
		this.tryAgainButton.relocate(445, 532);
		
		// Hinzufügen der benötigten Elemente zu dem eigentlichen PopUp
		this.popup.getContent().addAll(this.levelAuswahl.imgView, this.vBox, this.backToLAuswahlButton,
				this.tryAgainButton);
		this.popup.show(this.view.stage);
		
		this.model.addObserver(this);
	}
	
	private void drawTime() {
		this.stonesPushed.setText("Stones pushed: " + this.model.getStonesPushed());
		this.stonesPushed.setTextFill(Color.DARKGRAY);
		this.enemiesKilled.setText("Enemies killed: " + this.model.getEnemiesKilled());
		this.enemiesKilled.setTextFill(Color.DARKGRAY);
		this.explosionsOccured.setText("Explosions occured: " + this.model.getExplosionsOccurred());
		this.explosionsOccured.setTextFill(Color.DARKGRAY);
	}
	
	@Override
	public void update(Observable o, Object arg) {
		drawTime();
	}
	
}