package view;

import javafx.scene.Group;
import javafx.scene.ImageCursor;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import model.Level;
import model.Model;

import java.util.List;
import java.util.Observable;
import java.util.Observer;

/**
 * Erstellen des Views für das Spiel
 *
 * created by Vadim and Leo
 */

public class View implements Observer {
	
	private Model				model;
	public Scene				scene;
	public Stage				stage;
	private ImgLoader			imgLoader;
	private Pane				pane;
	private StartMenu			startmenu;
	private LevelAuswahl		levelAuswahl;
	private LevelAuswahl		levelAuswahl2;
	private Game				game;
	private LevelAuswahlPopUp	levelAuswahlPopUp;
	private GewonnenPopUp		gewonnenPopUp;
	private VerlorenPopUp		verlorenPopUp;
	private AlertPopUp			alertPopUp;
	private SoundLoader			soundLoader;
	private LevelEditor			levelEditor;
	private TutorialPopUp		tutorialPopUp;
	
	public View(Model model, Stage stage) {
		this.model = model;
		this.stage = stage;
		this.imgLoader = new ImgLoader();
		this.soundLoader = new SoundLoader();
		
		this.startmenu = new StartMenu(this.model, this);
		this.game = new Game(this.model, this);
		this.levelAuswahl = new LevelAuswahl(this.model, this, 0);
		this.levelAuswahl2 = new LevelAuswahl(this.model, this, 1);
		this.levelAuswahlPopUp = new LevelAuswahlPopUp(this.model, this);
		this.gewonnenPopUp = new GewonnenPopUp(this.model, this);
		this.verlorenPopUp = new VerlorenPopUp(this.model, this);
		this.alertPopUp = new AlertPopUp(this.model, this);
		this.levelEditor = new LevelEditor(this.model, this);
		this.tutorialPopUp = new TutorialPopUp(this.model, this);
		
		this.pane = new Pane();
		this.pane.getChildren().addAll(this.startmenu);
		this.stage.initStyle(StageStyle.TRANSPARENT);
		setNewScene();
		drawPfote();
	}
	
	public Scene getScene() {
		return this.scene;
	}
	
	public Stage getStage() {
		return this.stage;
	}
	
	public Game getGame() {
		return this.game;
	}
	
	/**
	 * Sämtliche Getter-Methoden für die jeweiligen Buttons
	 */
	// Alert-PopUp Button
	public Button getAlertOkayButton() {
		return this.alertPopUp.okayButton;
	}
	
	// für das TutorialPopUp
	public TutorialPopUp getTutorialPopUp() {
		return this.tutorialPopUp;
	}
	
	// für LevelAuswahl-PopUp- Buttons
	public Button getGameButton() {
		return this.levelAuswahlPopUp.gameButton;
	}
	
	public Button getCraftButton() {
		return this.levelAuswahlPopUp.craftButton;
	}
	
	public Button getBackToLevelAuswahlButton() {
		return this.levelAuswahlPopUp.backToLevelAuswahlButton;
	}
	
	// für Buttons die in Level-Auswahl erscheinen
	public List<Button> getLevelButtonList() {
		return this.levelAuswahl.levelButtons;
	}
	
	public Button getBackToStartButton() {
		return this.levelAuswahl.backToStartButton;
	}
	
	public Button getNextLevelauswahlButton() {
		return this.levelAuswahl.nextButton;
	}
	
	public Button getTutorialButton() {
		return this.levelAuswahl.tutorialButton;
	}
	
	// Für die Buttons die in Level-Auswahl2 erscheinen
	public List<Button> getLevelButtonList2() {
		return this.levelAuswahl2.levelButtons;
	}
	
	public Button getBackToStartButton2() {
		return this.levelAuswahl2.backToStartButton;
	}
	
	public Button getPreviousLevelauswahlButton() {
		return this.levelAuswahl2.previousButton;
	}
	
	public Button getTutorialButton2() {
		return this.levelAuswahl2.tutorialButton;
	}
	
	// Für die Startmenu-Buttons
	public Button getStartButton() {
		return this.startmenu.startButton;
	}
	
	public Button getLevelEditorButton() {
		return this.startmenu.levelEditorButton;
	}
	
	public Button getExitButton() {
		return this.startmenu.exitButton;
	}
	
	// Für die Buttons die in Game erscheinen
	public Button getBackButton() {
		return this.game.backButton;
	}
	
	public Button getMusikButton() {
		return this.game.musikButton;
	}
	
	public Button getSaveAndExitButton() {
		return this.game.saveAndExitButton;
	}
	
	// Für die Buttons die in GewonnenPopUp erscheinen
	public Button getBackToLevAuswahlButton() {
		return this.gewonnenPopUp.backToLevAuswahlButton;
	}
	
	public Button getPlayAgainButton() {
		return this.gewonnenPopUp.playAgainButton;
	}
	
	// Für die Buttons die in VerlorenPopUp erscheinen
	public Button getTryAgainButton() {
		return this.verlorenPopUp.tryAgainButton;
	}
	
	public Button getBackToLAuswahlButton() {
		return this.verlorenPopUp.backToLAuswahlButton;
	}
	
	// Für die Buttons die im TutorialPopUps erscheinen
	public Button getCloseButton() {
		return this.tutorialPopUp.closeButton;
	}
	
	// Für die Buttons die im Leveleditor erscheinen
	public List<Button> getTokenButtons() {
		return this.levelEditor.tokenButtons;
	}
	
	public Button getLevelEditorBackButton() {
		return this.levelEditor.backLEButton;
	}
	
	public Button getSmallButton() {
		return this.levelEditor.gridButtonSmall;
	}
	
	public Button getMediumButton() {
		return this.levelEditor.gridButtonMedium;
	}
	
	public Button getBigButton() {
		return this.levelEditor.gridButtonBig;
	}
	
	public Button getLESaveButton() {
		return this.levelEditor.saveButton;
	}
	
	public AlertPopUp getAlertPopUp() {
		return this.alertPopUp;
	}
	
	public LevelEditor getLevelEditor() {
		return this.levelEditor;
	}
	
	public LevelAuswahlPopUp getLevelAuswahlPopUp() {
		return this.levelAuswahlPopUp;
	}
	
	public ImgLoader getImgLoader() {
		return this.imgLoader;
	}
	
	public SoundLoader getSoundLoader() {
		return this.soundLoader;
	}
	
	public VerlorenPopUp getVerlorenPopUp() {
		return this.verlorenPopUp;
	}
	
	public GewonnenPopUp getGewonnenPopUp() {
		return this.gewonnenPopUp;
	}
	
	/**
	 * Methoden zum Austauschen von Szenen in Stage
	 */
	// LevelEditor wird aufgerufen
	public void fromStartToLevelEditor() {
		this.pane.getChildren().clear();
		this.pane.getChildren().addAll(this.levelEditor);
		setNewScene();
		drawPfote();
	}
	
	// Level-Auswahl Scene wird aufgerufen
	public void fromStartToLevelAuswahl() {
		this.pane.getChildren().clear();
		this.pane.getChildren().addAll(this.levelAuswahl);
		setNewScene();
		drawPfote();
		this.levelAuswahl.drawTime();
	}
	
	// Level-Auswahl2 Scene wird aufgerufen
	public void fromStartToLevelAuswahl2() {
		this.pane.getChildren().clear();
		this.pane.getChildren().addAll(this.levelAuswahl2);
		setNewScene();
		drawPfote();
		this.levelAuswahl2.drawTime();
	}
	
	// Zurückkehren zur Startmenü-Scene
	public void backToMainMenu() {
		this.pane.getChildren().clear();
		this.pane.getChildren().addAll(this.startmenu);
		setNewScene();
		drawPfote();
	}
	
	// LevelAuswahlPopUp Scenes 1-12 werden aufgerufen
	public void toLevelAuswahlPopUp(int i) {
		clearPaneAndShowPopUp();
		setNewScene();
		this.model.setInPopup(true);
		
		switch (i) {
			case 0:
				this.levelAuswahlPopUp.getImgView().setImage(this.imgLoader.getPopUpBackground1());
				this.levelAuswahlPopUp.gameButtonImg.setImage(this.imgLoader.getGameButton1());
				this.levelAuswahlPopUp.craftButton.setVisible(false);
				this.model.setLevel("data/levels/bewegung.json", i);
				drawHelmets(i);
				break;
			case 1:
				this.levelAuswahlPopUp.getImgView().setImage(this.imgLoader.getPopUpBackground2());
				this.levelAuswahlPopUp.gameButtonImg.setImage(this.imgLoader.getGameButton2());
				this.levelAuswahlPopUp.craftButton.setVisible(false);
				this.model.setLevel("data/levels/labyrinth.json", i);
				drawHelmets(i);
				break;
			case 2:
				this.levelAuswahlPopUp.getImgView().setImage(this.imgLoader.getPopUpBackground3());
				this.levelAuswahlPopUp.gameButtonImg.setImage(this.imgLoader.getGameButton3());
				this.levelAuswahlPopUp.craftButton.setVisible(false);
				this.model.setLevel("data/levels/schleimer.json", i);
				drawHelmets(i);
				break;
			case 3:
				this.levelAuswahlPopUp.getImgView().setImage(this.imgLoader.getPopUpBackground4());
				this.levelAuswahlPopUp.gameButtonImg.setImage(this.imgLoader.getGameButton4());
				this.levelAuswahlPopUp.craftButton.setVisible(false);
				this.model.setLevel("data/levels/spiegelgeist.json", i);
				drawHelmets(i);
				break;
			case 4:
				this.levelAuswahlPopUp.getImgView().setImage(this.imgLoader.getPopUpBackground5());
				this.levelAuswahlPopUp.gameButtonImg.setImage(this.imgLoader.getGameButton5());
				this.levelAuswahlPopUp.craftButton.setVisible(false);
				this.model.setLevel("data/levels/wand.json", i);
				drawHelmets(i);
				break;
			case 5:
				this.levelAuswahlPopUp.getImgView().setImage(this.imgLoader.getPopUpBackground6());
				this.levelAuswahlPopUp.gameButtonImg.setImage(this.imgLoader.getGameButton6());
				this.levelAuswahlPopUp.craftButton.setVisible(false);
				this.model.setLevel("data/levels/feuerstein.json", i);
				drawHelmets(i);
				break;
			case 6:
				this.levelAuswahlPopUp.getImgView().setImage(this.imgLoader.getPopUpBackground7());
				this.levelAuswahlPopUp.gameButtonImg.setImage(this.imgLoader.getGameButton7());
				this.levelAuswahlPopUp.craftButton.setVisible(true);
				this.levelAuswahlPopUp.gameButton.relocate(630, 600);
				this.model.setLevel("data/levels/sandsieb.json", i);
				drawHelmets(i);
				break;
			case 7:
				this.levelAuswahlPopUp.getImgView().setImage(this.imgLoader.getPopUpBackground8());
				this.levelAuswahlPopUp.gameButtonImg.setImage(this.imgLoader.getGameButton8());
				this.levelAuswahlPopUp.gameButton.relocate(630, 600);
				this.model.setLevel("data/levels/snake.json", i);
				drawHelmets(i);
				break;
			case 8:
				this.levelAuswahlPopUp.getImgView().setImage(this.imgLoader.getPopUpBackground9());
				this.levelAuswahlPopUp.gameButtonImg.setImage(this.imgLoader.getGameButton9());
				this.levelAuswahlPopUp.gameButton.relocate(630, 600);
				this.model.setLevel("data/levels/teleport.json", i);
				drawHelmets(i);
				break;
			case 9:
				this.levelAuswahlPopUp.getImgView().setImage(this.imgLoader.getPopUpBackground10());
				this.levelAuswahlPopUp.gameButtonImg.setImage(this.imgLoader.getGameButton10());
				this.levelAuswahlPopUp.gameButton.relocate(630, 600);
				this.model.setLevel("data/levels/turbotime.json", i);
				drawHelmets(i);
				break;
			case 10:
				this.levelAuswahlPopUp.getImgView().setImage(this.imgLoader.getPopUpBackground11());
				this.levelAuswahlPopUp.gameButtonImg.setImage(this.imgLoader.getGameButton11());
				this.levelAuswahlPopUp.gameButton.relocate(630, 600);
				this.model.setLevel("data/levels/timecollector.json", i);
				drawHelmets(i);
				break;
			case 11:
				this.levelAuswahlPopUp.getImgView().setImage(this.imgLoader.getPopUpBackground12());
				this.levelAuswahlPopUp.gameButtonImg.setImage(this.imgLoader.getGameButton12());
				this.model.setLevel("data/levels/customlevel.json", i);
				drawHelmets(i);
				break;
		}
		
		drawGamePreviewImage();
		drawPfote();
	}
	
	// Popup Preview Bild für jeweiliges Level wird erstellt und in Popup gesteckt
	private void drawGamePreviewImage() {
		GridPane grid = new GridPane();
		Level level = this.model.getCurrentlevel();
		
		int xMax = 10;
		int yMax = 10;
		
		getGame().drawGrid(level, 0, xMax, 0, yMax, grid, "down", 0);
		
		this.levelAuswahlPopUp.mappane.getChildren().clear();
		this.levelAuswahlPopUp.mappane.getChildren().add(grid);
		this.levelAuswahlPopUp.mappane.setScaleY(0.35);
		this.levelAuswahlPopUp.mappane.setScaleX(0.35);
		this.levelAuswahlPopUp.mappane.relocate(100, -60);
		
		double cellcount = model.getCurrentlevel().getWidth();
		if (cellcount < model.getCurrentlevel().getHeight()) cellcount = model.getCurrentlevel().getHeight();
	}
	
	// Aufrufen der Game-Scene
	public void fromLevelAuswahlPopUpToGame() {
		this.model.setInPopup(false);
		this.pane.getChildren().clear();
		this.pane.getChildren().add(this.game);
		setNewScene();
		drawPfote();
	}
	
	// Aufrufen des GewonnenPopUps
	public void fromGameToGewonnenPopUp() {
		this.pane.getChildren().clear();
		this.gewonnenPopUp.popup.show(this.stage);
		setNewScene();
		drawPfote();
	}
	
	// Aufrufen des VerlorenPopUps
	public void fromGameToVerlorenPopUp() {
		this.pane.getChildren().clear();
		this.verlorenPopUp.popup.show(this.stage);
		setNewScene();
		drawPfote();
	}
	
	// Aufrufen des TutorialPopUps
	public void showTutorialPopUp() {
		this.pane.getChildren().clear();
		this.tutorialPopUp.popup.show(this.stage);
		setNewScene();
		drawPfote();
	}
	
	// Aufrufen des AlertPopUps
	public void showAlert() {
		this.pane.getChildren().clear();
		this.alertPopUp.popup.show(this.stage);
		setNewScene();
		drawPfote();
	}
	
	public void exit() {
		this.stage.close();
	}
	
	/**
	 * Methoden die mehrmals verwendet werden
	 */
	private void clearPaneAndShowPopUp() {
		this.pane.getChildren().clear();
		this.levelAuswahlPopUp.popup.show(this.stage);
	}
	
	private void setNewScene() {
		this.scene = new Scene(new Group(this.pane));
		this.stage.setScene(this.scene);
	}
	
	// Methode um die erreichten Helme in LevelAuswahlPopUp anzuzeigen
	private void drawHelmets(int j) {
		if (model.getPoints().get(j) == 0) {
			this.levelAuswahlPopUp.getHelmets().setImage(this.imgLoader.getNullAus3());
		} else if (model.getPoints().get(j) == 1) {
			this.levelAuswahlPopUp.getHelmets().setImage(this.imgLoader.getEinsAus3());
		} else if (model.getPoints().get(j) == 2) {
			this.levelAuswahlPopUp.getHelmets().setImage(this.imgLoader.getZweiAus3());
		} else if (model.getPoints().get(j) == 3) {
			this.levelAuswahlPopUp.getHelmets().setImage(this.imgLoader.getDreiAus3());
		}
	}
	
	/**
	 * Methode um den Cursor laufend zu ändern
	 */
	private void drawPfote() {
		ImageCursor imgCursorPfote = new ImageCursor(imgLoader.getPfoteZeigend());
		ImageCursor imageCursor = new ImageCursor(imgLoader.getPfoteOffen());
		ImageCursor imgCursor = new ImageCursor(imgLoader.getSpitzhacke());
		
		this.levelAuswahl.backToStartButton.setCursor(imgCursorPfote);
		this.levelAuswahl.backToStartButton.getCursor();
		
		this.levelAuswahl.nextButton.setCursor(imgCursorPfote);
		this.levelAuswahl.nextButton.getCursor();
		
		for (Button button : this.levelAuswahl.levelButtons) {
			button.setCursor(imgCursor);
			button.getCursor();
		}
		
		for (Button button : this.levelAuswahl2.levelButtons) {
			button.setCursor(imgCursor);
			button.getCursor();
		}
		
		this.levelAuswahl2.backToStartButton.setCursor(imgCursorPfote);
		this.levelAuswahl2.backToStartButton.getCursor();
		
		this.levelAuswahl2.previousButton.setCursor(imgCursorPfote);
		this.levelAuswahl2.previousButton.getCursor();
		
		this.levelAuswahlPopUp.backToLevelAuswahlButton.setCursor(imgCursorPfote);
		this.levelAuswahlPopUp.backToLevelAuswahlButton.getCursor();
		
		this.gewonnenPopUp.backToLevAuswahlButton.setCursor(imgCursorPfote);
		this.gewonnenPopUp.backToLevAuswahlButton.getCursor();
		
		this.gewonnenPopUp.playAgainButton.setCursor(imgCursorPfote);
		this.gewonnenPopUp.playAgainButton.getCursor();
		
		this.levelAuswahlPopUp.gameButton.setCursor(imgCursorPfote);
		this.levelAuswahlPopUp.gameButton.getCursor();
		
		this.levelAuswahlPopUp.craftButton.setCursor(imgCursorPfote);
		this.levelAuswahlPopUp.craftButton.getCursor();
		
		this.startmenu.exitButton.setCursor(imgCursorPfote);
		this.startmenu.exitButton.getCursor();
		
		this.startmenu.startButton.setCursor(imgCursorPfote);
		this.startmenu.startButton.getCursor();
		
		this.startmenu.levelEditorButton.setCursor(imgCursorPfote);
		this.startmenu.levelEditorButton.getCursor();
		
		this.verlorenPopUp.backToLAuswahlButton.setCursor(imgCursorPfote);
		this.verlorenPopUp.backToLAuswahlButton.getCursor();
		
		this.verlorenPopUp.tryAgainButton.setCursor(imgCursorPfote);
		this.verlorenPopUp.tryAgainButton.getCursor();
		
		this.game.musikButton.setCursor(imgCursorPfote);
		this.game.musikButton.getCursor();
		
		this.game.backButton.setCursor(imgCursorPfote);
		this.game.backButton.getCursor();
		
		this.game.saveAndExitButton.setCursor(imgCursorPfote);
		this.game.saveAndExitButton.getCursor();
		
		this.levelEditor.saveButton.setCursor(imgCursorPfote);
		this.levelEditor.saveButton.getCursor();
		
		this.levelEditor.backLEButton.setCursor(imgCursorPfote);
		this.levelEditor.backLEButton.getCursor();
		
		this.levelAuswahl.tutorialButton.setCursor(imgCursorPfote);
		this.levelAuswahl.tutorialButton.getCursor();
		
		this.levelAuswahl2.tutorialButton.setCursor(imgCursorPfote);
		this.levelAuswahl2.tutorialButton.getCursor();
		
		this.tutorialPopUp.closeButton.setCursor(imgCursorPfote);
		this.tutorialPopUp.closeButton.getCursor();
		
		for (Button button : this.levelEditor.tokenButtons) {
			button.setCursor(imgCursor);
			button.getCursor();
		}
		
		this.levelEditor.gridButtonBig.setCursor(imgCursorPfote);
		this.levelEditor.gridButtonBig.getCursor();
		
		this.levelEditor.gridButtonMedium.setCursor(imgCursorPfote);
		this.levelEditor.gridButtonMedium.getCursor();
		
		this.levelEditor.gridButtonSmall.setCursor(imgCursorPfote);
		this.levelEditor.gridButtonSmall.getCursor();
		
		this.alertPopUp.okayButton.setCursor(imgCursorPfote);
		this.alertPopUp.okayButton.getCursor();
		
		this.scene.setCursor(imageCursor);
		this.scene.getCursor();
	}
	
	@Override
	public void update(Observable o, Object arg) {
		drawPfote();
	}
	
}
