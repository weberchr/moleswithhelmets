package view;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

/**
 * Erstellen des ImageLoaders
 *
 * created by Leo
 */

public class ImgLoader {
	
	private Image		gewonnenPopUpBackground, verlorenPopUpBackground, tryAgainButton, playAgainButton,
			startMenuBackground, startButtonImg, alertImg, exitButtonImg, pfoteOffen, pfoteZeigend,
			levelEditorButtonImg, spitzhacke, karteLevel1, karteLevel2, karteLevel3, karteLevel4, karteLevel5,
			karteLevel6, levelsBackground, levelAuswahlMoly, mainMenuButton, buttonLevel1, buttonLevel2, buttonLevel3,
			buttonLevel4, buttonLevel7, buttonLevel8, buttonLevel9, buttonLevel10, buttonLevel11, buttonLevel12,
			buttonLevel5, buttonLevel6, nextLevelsButton, previousLevelsButton, tunnelFront, popUpBackground1,
			popUpBackground2, popUpBackground3, popUpBackground4, popUpBackground5, popUpBackground6, gameButton1,
			gameButton2, gameButton3, gameButton4, gameButton5, gameButton6, backButton, nullAus3, einsAus3, zweiAus3,
			dreiAus3, gameViewBackground, musicOnButton, musicOffButton, backToLevelsButton, meLeftImage,
			meLeftWalkingImage, meRightImage, meUpImage, meDownImage, mudImage, pathImage, bricksImage, gemImage,
			wallImage, stoneImage, exitImage, foxImage, foxWalkingImage, racoonImage, racoonWalkingImage, skeletonImage,
			skeletonWalkingImage, slimeImage, foxWalkingRightImage, racoonWalkingRightImage, skeletonWalkingRightImage,
			meRightWalkingImage, meUpWalkingImage, meDownWalkingImage, fireImage, sandImage, foxWalkingDownImage,
			foxWalkingUpImage, racoonWalkingUpImage, racoonWalkingDownImage, skeletonWalkingDownImage,
			skeletonWalkingUpImage, ghostWalkingLeftImage, ghostWalkingRightImage, ghostWalkingUpImage,
			ghostWalkingDownImage, explosionImage, exitClosedImage, sieveImage, potImage, eastThingImage,
			westThingImage, northThingImage, southThingImage, turboTimeImage, hourGlassImage, popUpBackground7,
			popUpBackground8, popUpBackground9, popUpBackground10, popUpBackground11, gemSlipperyImage,
			stoneSlipperyImage, popUpBackground12, turboMeUp, turboMeDown, turboMeLeft, turboMeRight, kameImage,
			craftButton, gameButton7, gameButton8, gameButton9, gameButton10, gameButton11, gameButton12,
			saveAndExitButton, levelEditorBackground, chooseSizeBack, chooseTokenBack, saveAndExitButtonLE, smallButton,
			mediumButton, bigButton, tutorialBackground, tutorialButton, closeButton, geilstesGifDerWelt;
	
	private ImageView	startBackView, musicOffView, musicOnView;
	
	/**
	 * Laden aller im Spiel enthaltenen Images
	 */
	public ImgLoader() {
		try {
			// Gewonnen und Verloren PopUps
			gewonnenPopUpBackground = new Image("images/GameWonBackground.jpg");
			verlorenPopUpBackground = new Image("images/GameOverBackground.jpg");
			tryAgainButton = new Image("images/TryAgainButton.png");
			playAgainButton = new Image("images/PlayAgainButton.png");
			
			// StartMenu
			startMenuBackground = new Image("images/StartMenuBackground.jpg");
			startBackView = new ImageView(startMenuBackground);
			startBackView.setPreserveRatio(true);
			startBackView.setSmooth(true);
			
			startButtonImg = new Image("images/StartMenu_StartButton.png");
			exitButtonImg = new Image("images/StartMenu_ExitButton.png");
			levelEditorButtonImg = new Image("images/LevelEditorButton.png");
			
			// Cursor-Images
			pfoteOffen = new Image("images/PfoteOffen.png");
			pfoteZeigend = new Image("images/PfoteZeigend.png");
			spitzhacke = new Image("images/hacke.png");
			
			// Images für die Levelauswahl
			levelsBackground = new Image("images/LevelAuswahlBackgroundneu.jpg");
			buttonLevel1 = new Image("images/Level1.png");
			buttonLevel2 = new Image("images/Level2.png");
			buttonLevel3 = new Image("images/Level3.png");
			buttonLevel4 = new Image("images/Level4.png");
			buttonLevel5 = new Image("images/Level5.png");
			buttonLevel6 = new Image("images/Level6.png");
			buttonLevel7 = new Image("images/Level7.png");
			buttonLevel8 = new Image("images/Level8.png");
			buttonLevel9 = new Image("images/Level9.png");
			buttonLevel10 = new Image("images/Level10.png");
			buttonLevel11 = new Image("images/Level11.png");
			buttonLevel12 = new Image("images/Level12.png");
			levelAuswahlMoly = new Image("images/MolyLevelAuswahl.gif");
			mainMenuButton = new Image("images/LevelAuswahl_MainButton.png");
			nextLevelsButton = new Image("images/NextLevelsButton.png");
			previousLevelsButton = new Image("images/PreviousLevelsButton.png");
			tunnelFront = new Image("images/TunnelLA.png");
			alertImg = new Image("images/Alert1.png");
			
			// PopUp Hintergründe
			popUpBackground1 = new Image("images/BackgroungMoveIt.jpg");
			popUpBackground2 = new Image("images/BackgroundLabyrinth.jpg");
			popUpBackground3 = new Image("images/BackgroundSlimy.jpg");
			popUpBackground4 = new Image("images/BackgroundMirror.jpg");
			popUpBackground5 = new Image("images/BackgroundWall.jpg");
			popUpBackground6 = new Image("images/BackgroundFeuerstein.jpg");
			popUpBackground7 = new Image("images/BackgroundSandSieve.jpg");
			popUpBackground8 = new Image("images/BackgroundSnake.jpg");
			popUpBackground9 = new Image("images/BackgroundTeleport.jpg");
			popUpBackground10 = new Image("images/BackgroundTurboTime.jpg");
			popUpBackground11 = new Image("images/BackgroundTimeCollecter.jpg");
			popUpBackground12 = new Image("images/BackgroundLev12.jpg");
			
			// Alle Elemente des Tutorials
			tutorialBackground = new Image("images/TutorialBackground.jpg");
			tutorialButton = new Image("images/TutorialButton.png");
			closeButton = new Image("images/CloseButton.png");
			geilstesGifDerWelt = new Image("images/GeilstesGifDerWelt.gif");
			
			// Buttons im LevelAuswahlPopUp
			backButton = new Image("images/BackButton.png");
			gameButton1 = new Image("images/GoButtonLevel1.png");
			gameButton2 = new Image("images/GoButtonLevel2.png");
			gameButton3 = new Image("images/GoButtonLevel3.png");
			gameButton4 = new Image("images/GoButtonLevel4.png");
			gameButton5 = new Image("images/GoButtonLevel6.png");
			gameButton6 = new Image("images/GoButtonFeuer.png");
			gameButton7 = new Image("images/GoButtonSandSieve.png");
			gameButton8 = new Image("images/GoButtonSnake.png");
			gameButton9 = new Image("images/GoButtonTeleport.png");
			gameButton10 = new Image("images/GoButtonTurboTime.png");
			gameButton11 = new Image("images/GoButtonTimeCollecter.png");
			gameButton12 = new Image("images/GoButtonLevel5.png");
			craftButton = new Image("images/CraftModeButton.png");
			
			// Gesammelte Helme
			nullAus3 = new Image("images/0von3.png");
			einsAus3 = new Image("images/1von3.png");
			zweiAus3 = new Image("images/2von3.png");
			dreiAus3 = new Image("images/3von3.png");
			
			// Game und LevelEditor Backgrounds und Buttons
			gameViewBackground = new Image("images/GameViewBackground.jpg");
			levelEditorBackground = new Image("images/LevelEditorBackground.jpg");
			chooseSizeBack = new Image("images/ChooseSizeBack.png");
			chooseTokenBack = new Image("images/ChooseTokenBack.png");
			saveAndExitButtonLE = new Image("images/SaveAndExit.png");
			smallButton = new Image("images/SmallButton.png");
			mediumButton = new Image("images/MediumButton.png");
			bigButton = new Image("images/BigButton.png");
			new Image("images/RahmenGameView.jpg");
			musicOnButton = new Image("images/MusicOnButton.png");
			musicOffButton = new Image("images/MusicOffButton.png");
			backToLevelsButton = new Image("images/BackToLevelsButton.png");
			saveAndExitButton = new Image("images/SaveAndExit2.png");
			musicOffView = new ImageView(musicOffButton);
			musicOnView = new ImageView(musicOnButton);
			
			// Token für das Game
			meLeftImage = new Image("images/Me.jpg");
			meLeftWalkingImage = new Image("images/MeLeftWalking2.gif");
			meRightImage = new Image("images/MeRight.jpg");
			meRightWalkingImage = new Image("images/MeRightWalking.gif");
			meUpImage = new Image("images/MeUp.jpg");
			meUpWalkingImage = new Image("images/MeUpWalking.gif");
			meDownImage = new Image("images/MeDown.jpg");
			meDownWalkingImage = new Image("images/MeDownWalking.gif");
			turboMeUp = new Image("images/TurboMeUp.gif");
			turboMeDown = new Image("images/TurboMeDown.gif");
			turboMeLeft = new Image("images/TurboMeLeft.gif");
			turboMeRight = new Image("images/TurboMeRight.gif");
			mudImage = new Image("images/Mud.jpg");
			pathImage = new Image("images/Path.jpg");
			gemImage = new Image("images/GemMoving.gif");
			gemSlipperyImage = new Image("images/GemSlippery.gif");
			wallImage = new Image("images/Wall.jpg");
			bricksImage = new Image("images/Bricks.jpg");
			stoneImage = new Image("images/StoneGrey.jpg");
			stoneSlipperyImage = new Image("images/StoneSlippery.jpg");
			foxImage = new Image("images/Fox.jpg");
			foxWalkingImage = new Image("images/Fox_Walking.gif");
			foxWalkingRightImage = new Image("images/Fox_WalkingRight.gif");
			foxWalkingDownImage = new Image("images/FoxDownWalking.gif");
			foxWalkingUpImage = new Image("images/FoxUpWalking.gif");
			racoonImage = new Image("images/Racoon.jpg");
			racoonWalkingImage = new Image("images/RacoonWalking.gif");
			racoonWalkingRightImage = new Image("images/RacoonWalkingRight.gif");
			racoonWalkingUpImage = new Image("images/RacoonUpWalking.gif");
			racoonWalkingDownImage = new Image("images/RacoonDownWalking.gif");
			skeletonImage = new Image("images/SkeletonMoly.jpg");
			skeletonWalkingImage = new Image("images/SkeletonMoly_Walking.gif");
			skeletonWalkingRightImage = new Image("images/SkeletonMolyWalkingRight.gif");
			skeletonWalkingDownImage = new Image("images/SkeletonMolyDownWalking.gif");
			skeletonWalkingUpImage = new Image("images/SkeletonMolyUpWalking.gif");
			ghostWalkingLeftImage = new Image("images/GhostLeftWalking.gif");
			ghostWalkingRightImage = new Image("images/GhostRightWalking.gif");
			ghostWalkingUpImage = new Image("images/GhostUpWalking.gif");
			ghostWalkingDownImage = new Image("images/GhostDownWalking.gif");
			slimeImage = new Image("images/SlimeMoving.gif");
			exitImage = new Image("images/Exit.jpg");
			fireImage = new Image("images/Fire.gif");
			sandImage = new Image("images/Sand.gif");
			explosionImage = new Image("images/Explosion.gif");
			sieveImage = new Image("images/Sieve.jpg");
			potImage = new Image("images/Pot.jpg");
			exitClosedImage = new Image("images/ExitClosed.jpg");
			turboTimeImage = new Image("images/TurboTime.gif");
			hourGlassImage = new Image("images/HourGlass.jpg");
			northThingImage = new Image("images/Norththing.jpg");
			southThingImage = new Image("images/Souththing.jpg");
			eastThingImage = new Image("images/Eastthing.jpg");
			westThingImage = new Image("images/Westthing.jpg");
			kameImage = new Image("images/Kamehameha.jpg");
		} catch (Exception e) {
			System.out.println("Fehler beim Laden der Bildquellen!");
		}
	}
	
	// sämtliche benötigten Getters
	public Image getTutorialBackground() {
		return tutorialBackground;
	}
	
	public Image getTutorialButton() {
		return tutorialButton;
	}
	
	public Image getCloseButton() {
		return closeButton;
	}
	
	public Image getGeilstesGifDerWelt() {
		return geilstesGifDerWelt;
	}
	
	public Image getSmallButton() {
		return smallButton;
	}
	
	public Image getMediumButton() {
		return mediumButton;
	}
	
	public Image getBigButton() {
		return bigButton;
	}
	
	public Image getSaveAndExitButtonLE() {
		return saveAndExitButtonLE;
	}
	
	public Image getChooseTokenBack() {
		return chooseTokenBack;
	}
	
	public Image getChooseSizeBack() {
		return chooseSizeBack;
	}
	
	public Image getLevelEditorBackground() {
		return levelEditorBackground;
	}
	
	public Image getSaveAndExitButton() {
		return saveAndExitButton;
	}
	
	public Image getCraftButton() {
		return craftButton;
	}
	
	public Image getGameButton7() {
		return gameButton7;
	}
	
	public Image getGameButton8() {
		return gameButton8;
	}
	
	public Image getGameButton9() {
		return gameButton9;
	}
	
	public Image getGameButton10() {
		return gameButton10;
	}
	
	public Image getGameButton11() {
		return gameButton11;
	}
	
	public Image getGameButton12() {
		return gameButton12;
	}
	
	public Image getGemSlipperyImage() {
		return gemSlipperyImage;
	}
	
	public Image getStoneSlipperyImage() {
		return stoneSlipperyImage;
	}
	
	public Image getTurboMeUp() {
		return turboMeUp;
	}
	
	public Image getTurboMeDown() {
		return turboMeDown;
	}
	
	public Image getTurboMeLeft() {
		return turboMeLeft;
	}
	
	public Image getTurboMeRight() {
		return turboMeRight;
	}
	
	public Image getPopUpBackground12() {
		return popUpBackground12;
	}
	
	public Image getPopUpBackground11() {
		return popUpBackground11;
	}
	
	public Image getPopUpBackground10() {
		return popUpBackground10;
	}
	
	public Image getPopUpBackground9() {
		return popUpBackground9;
	}
	
	public Image getPopUpBackground8() {
		return popUpBackground8;
	}
	
	public Image getPopUpBackground7() {
		return popUpBackground7;
	}
	
	public Image getHourGlassImage() {
		return hourGlassImage;
	}
	
	public Image getKameImage() {
		return kameImage;
	}
	
	public Image getTurboTimeImage() {
		return turboTimeImage;
	}
	
	public Image getSouthThingImage() {
		return southThingImage;
	}
	
	public Image getNorthThingImage() {
		return northThingImage;
	}
	
	public Image getWestThingImage() {
		return westThingImage;
	}
	
	public Image getSieveImage() {
		return sieveImage;
	}
	
	public Image getEastThingImage() {
		return eastThingImage;
	}
	
	public Image getPotImage() {
		return potImage;
	}
	
	public Image getExplosionImage() {
		return explosionImage;
	}
	
	public Image getExitClosedImage() {
		return exitClosedImage;
	}
	
	public Image getSkeletonWalkingUpImage() {
		return skeletonWalkingUpImage;
	}
	
	public Image getSkeletonWalkingDownImage() {
		return skeletonWalkingDownImage;
	}
	
	public Image getRacoonWalkingUpImage() {
		return racoonWalkingUpImage;
	}
	
	public Image getRacoonWalkingDownImage() {
		return racoonWalkingDownImage;
	}
	
	public Image getGhostWalkingUpImage() {
		return ghostWalkingUpImage;
	}
	
	public Image getGhostWalkingRightImage() {
		return ghostWalkingRightImage;
	}
	
	public Image getGhostWalkingDownImage() {
		return ghostWalkingDownImage;
	}
	
	public Image getGhostWalkingLeftImage() {
		return ghostWalkingLeftImage;
	}
	
	public Image getFoxWalkingUpImage() {
		return foxWalkingUpImage;
	}
	
	public Image getFoxWalkingDownImage() {
		return foxWalkingDownImage;
	}
	
	public Image getSandImage() {
		return sandImage;
	}
	
	public Image getMeUpWalkingImage() {
		return meUpWalkingImage;
	}
	
	public Image getMeDownWalkingImage() {
		return meDownWalkingImage;
	}
	
	public Image getFireImage() {
		return fireImage;
	}
	
	public Image getAlertImg() {
		return alertImg;
	}
	
	public Image getLevelEditorButtonImg() {
		return levelEditorButtonImg;
	}
	
	public Image getMeRightWalkingImage() {
		return meRightWalkingImage;
	}
	
	public Image getSkeletonWalkingRightImage() {
		return skeletonWalkingRightImage;
	}
	
	public Image getRacoonWalkingRightImage() {
		return racoonWalkingRightImage;
	}
	
	public Image getFoxWalkingRightImage() {
		return foxWalkingRightImage;
	}
	
	public Image getRacoonWalkingImage() {
		return racoonWalkingImage;
	}
	
	public Image getMeLeftWalkingImage() {
		return meLeftWalkingImage;
	}
	
	public Image getPopUpBackground6() {
		return popUpBackground6;
	}
	
	public Image getPopUpBackground5() {
		return popUpBackground5;
	}
	
	public Image getPopUpBackground4() {
		return popUpBackground4;
	}
	
	public Image getGameButton6() {
		return gameButton6;
	}
	
	public Image getGameButton5() {
		return gameButton5;
	}
	
	public Image getGameButton4() {
		return gameButton4;
	}
	
	public Image getTryAgainButton() {
		return tryAgainButton;
	}
	
	public Image getPlayAgainButton() {
		return playAgainButton;
	}
	
	public Image getPopUpBackground3() {
		return popUpBackground3;
	}
	
	public Image getPopUpBackground2() {
		return popUpBackground2;
	}
	
	public Image getGameButton3() {
		return gameButton3;
	}
	
	public Image getGameButton2() {
		return gameButton2;
	}
	
	public ImageView getMusicOffView() {
		return musicOffView;
	}
	
	public ImageView getMusicOnView() {
		return musicOnView;
	}
	
	public Image getBackToLevelsButton() {
		return backToLevelsButton;
	}
	
	public Image getMusicOffButton() {
		return musicOffButton;
	}
	
	public Image getMusicOnButton() {
		return musicOnButton;
	}
	
	public Image getTunnelFront() {
		return tunnelFront;
	}
	
	public Image getNextLevelsButton() {
		return nextLevelsButton;
	}
	
	public Image getPreviousLevelsButton() {
		return previousLevelsButton;
	}
	
	public Image getButtonLevel8() {
		return buttonLevel8;
	}
	
	public Image getButtonLevel9() {
		return buttonLevel9;
	}
	
	public Image getButtonLevel10() {
		return buttonLevel10;
	}
	
	public Image getButtonLevel11() {
		return buttonLevel11;
	}
	
	public Image getButtonLevel12() {
		return buttonLevel12;
	}
	
	public Image getButtonLevel7() {
		return buttonLevel7;
	}
	
	public Image getSkeletonWalkingImage() {
		return skeletonWalkingImage;
	}
	
	public Image getFoxWalkingImage() {
		return foxWalkingImage;
	}
	
	public Image getEinsAus3() {
		return einsAus3;
	}
	
	public Image getDreiAus3() {
		return dreiAus3;
	}
	
	public Image getNullAus3() {
		return nullAus3;
	}
	
	public Image getZweiAus3() {
		return zweiAus3;
	}
	
	public Image getBackButton() {
		return backButton;
	}
	
	public Image getGameButton1() {
		return gameButton1;
	}
	
	public Image getSkeletonImage() {
		return skeletonImage;
	}
	
	public Image getExitImage() {
		return exitImage;
	}
	
	public Image getMeUpImage() {
		return meUpImage;
	}
	
	public Image getMeDownImage() {
		return meDownImage;
	}
	
	public Image getMeRightImage() {
		return meRightImage;
	}
	
	public Image getFoxImage() {
		return foxImage;
	}
	
	public Image getRacoonImage() {
		return racoonImage;
	}
	
	public Image getSlimeImage() {
		return slimeImage;
	}
	
	public ImageView getStartBackMenu() {
		return startBackView;
	}
	
	public Image getMainMenuButton() {
		return mainMenuButton;
	}
	
	public Image getLevelAuswahlMoly() {
		return levelAuswahlMoly;
	}
	
	public Image getWallImage() {
		return wallImage;
	}
	
	public Image getStoneImage() {
		return stoneImage;
	}
	
	public Image getGemImage() {
		return gemImage;
	}
	
	public Image getPathImage() {
		return pathImage;
	}
	
	public Image getBricksImage() {
		return bricksImage;
	}
	
	public Image getMudImage() {
		return mudImage;
	}
	
	public Image getMeLeftImage() {
		return meLeftImage;
	}
	
	public Image getStartMenuBackground() {
		return startMenuBackground;
	}
	
	public Image getStartButtonImg() {
		return startButtonImg;
	}
	
	public Image getExitButtonImg() {
		return exitButtonImg;
	}
	
	public Image getPfoteOffen() {
		return pfoteOffen;
	}
	
	public Image getSpitzhacke() {
		return spitzhacke;
	}
	
	public Image getPfoteZeigend() {
		return pfoteZeigend;
	}
	
	public Image getKarteLevel1() {
		return karteLevel1;
	}
	
	public Image getKarteLevel2() {
		return karteLevel2;
	}
	
	public Image getKarteLevel3() {
		return karteLevel3;
	}
	
	public Image getKarteLevel4() {
		return karteLevel4;
	}
	
	public Image getKarteLevel5() {
		return karteLevel5;
	}
	
	public Image getKarteLevel6() {
		return karteLevel6;
	}
	
	public Image getLevelsBackground() {
		return levelsBackground;
	}
	
	public Image getButtonLevel1() {
		return buttonLevel1;
	}
	
	public Image getButtonLevel2() {
		return buttonLevel2;
	}
	
	public Image getButtonLevel3() {
		return buttonLevel3;
	}
	
	public Image getButtonLevel4() {
		return buttonLevel4;
	}
	
	public Image getButtonLevel5() {
		return buttonLevel5;
	}
	
	public Image getButtonLevel6() {
		return buttonLevel6;
	}
	
	public Image getPopUpBackground1() {
		return popUpBackground1;
	}
	
	public Image getGameViewBackground() {
		return gameViewBackground;
	}
	
	public Image getGewonnenPopUpBackground() {
		return gewonnenPopUpBackground;
	}
	
	public Image getVerlorenPopUpBackground() {
		return verlorenPopUpBackground;
	}
	
}
