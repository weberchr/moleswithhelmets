# Moles with Helmets
This game was developed in the software development practical course of the computer science faculty at LMU. It was written in the JavaFX programming language and is a version of the game Boulder Dash.

![moles.gif](moles.gif)




## Members
- Andreas Wassermayr
- Christoph Weber
- Leonie Brill
- Vadim Ott
